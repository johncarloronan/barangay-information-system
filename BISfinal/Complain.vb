﻿Imports FireSharp.Config
Imports FireSharp.Response
Imports FireSharp.Interfaces
'-------------------------------------------

Imports System.Web.Script.Serialization '(Importing System.Web.Script.Serialization) This is used to read and convert JSON strings.
Imports System.ComponentModel '(Importing System.ComponentModel) This is used to sort data on DataGridView.
Imports System.IO

Public Class Complain

    Public Property BLOTTERname As String

    Dim IMG_FileNameInput As String
    Dim clearDGVCol As Boolean = True

    Private dtTableGrd As DataTable


    Private fcon As New FirebaseConfig() With
        {
        .AuthSecret = "8kPwBHDkDdZWGHts9sb98dsJu45thBvgDTUrgqJj",
        .BasePath = "https://bisrealtime-default-rtdb.firebaseio.com/"
        }

    Private client As IFirebaseClient

    Sub DisplayRegSave(Stat As Boolean)
        caseNum.Enabled = Stat
        caseDate.Enabled = Stat
        caseTime.Enabled = Stat
        TBComplainant.Enabled = Stat
        TBincident.Enabled = Stat
        TBdetails.Enabled = Stat
        CBremarks.Enabled = Stat
        TBactiontaken.Enabled = Stat
        Eadmin.Enabled = Stat
        btnSave.Enabled = Stat
    End Sub

    Sub ShowRecord()
        Try
            Dim dtTable As New DataTable
            dtTable.Columns.Add("Case no.")
            dtTable.Columns.Add("Date")
            dtTable.Columns.Add("Time")
            dtTable.Columns.Add("Complainant")
            dtTable.Columns.Add("Address")
            dtTable.Columns.Add("Incident")
            dtTable.Columns.Add("Details")
            dtTable.Columns.Add("Action")
            dtTable.Columns.Add("Remarks")
            dtTable.Columns.Add("Encoder")



            If clearDGVCol = True Then

                clearDGVCol = False
            End If


            Dim SRRecord = client.Get("BlotterDB/")


            Dim myJsonTool As New JavaScriptSerializer
            Dim myDeserializedItems = myJsonTool.Deserialize(Of Dictionary(Of String, ComplainData))(SRRecord.Body)


            For Each dictItem As KeyValuePair(Of String, ComplainData) In myDeserializedItems
                dtTable.Rows.Add(dictItem.Value.eCasenumber, dictItem.Value.eDate, dictItem.Value.eTime, dictItem.Value.eComplainant, dictItem.Value.eAddress, dictItem.Value.eDetails, dictItem.Value.eAction, dictItem.Value.eRemarks, dictItem.Value.eEncoder)
            Next


            dtTableGrd = dtTable



        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ElseIf ex.Message = "Object reference not set to an instance of an object." Then
                Dim dtTable As New DataTable
                dtTable.Columns.Add("Case no.")
                dtTable.Columns.Add("Date")
                dtTable.Columns.Add("Time")
                dtTable.Columns.Add("Complainant")
                dtTable.Columns.Add("Address")
                dtTable.Columns.Add("Incident")
                dtTable.Columns.Add("Details")
                dtTable.Columns.Add("Action")
                dtTable.Columns.Add("Remarks")
                dtTable.Columns.Add("Encoder")

                MessageBox.Show("Database not found or Database is empty.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If



        End Try

    End Sub

    Private Sub RequestList_Click(sender As Object, e As EventArgs)


    End Sub

    Private Sub newBlotter_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Timer1.Enabled = True

        Try
            client = New FireSharp.FirebaseClient(fcon)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Try
            caseNum.Text = "Please wait..."

            Dim r As Random = New Random
            Dim num As Integer
            num = (r.Next(1, 999999999))
            Dim IDresults As String = Strings.Right("00000000" & num.ToString(), 8)

            Dim Check_ID = client.Get("BlotterDB/" + IDresults)


            If Check_ID.Body <> "null" Then
                MessageBox.Show("The same ID is found, create another ID by pressing the Create ID button.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                caseNum.Text = IDresults
            End If
            '-------------------------------------------
        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                caseNum.Text = ""
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                caseNum.Text = ""
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End Try

        TBComplainant.Text = BLOTTERname

    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If TBComplainant.Text = Nothing Then
            MessageBox.Show("COMPLAINANT field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TBaddress.Text = Nothing Then
            MessageBox.Show("ADDRESS field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If


        If TBincident.Text = Nothing Then
            MessageBox.Show("CASE field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TBdetails.Text = Nothing Then
            MessageBox.Show("DETAILS field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If
        If TBactiontaken.Text = Nothing Then
            MessageBox.Show("RESPONDENT field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If CBremarks.Text = Nothing Then
            MessageBox.Show("REMARKS field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If Eadmin.Text = Nothing Then
            MessageBox.Show("REMARKS field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If






        Try
            DisplayRegSave(False)
            Dim NC As New ComplainData() With
                {
            .eCasenumber = caseNum.Text,
            .eDate = caseDate.Text,
            .eTime = caseTime.Text,
            .eComplainant = TBComplainant.Text,
            .eAddress = TBaddress.Text,
            .eIncident = TBincident.Text,
            .eDetails = TBdetails.Text,
            .eAction = TBactiontaken.Text,
            .eRemarks = CBremarks.Text,
            .eEncoder = Eadmin.Text
                }
            Dim save = client.Set("BlotterDB/" + caseNum.Text, NC)
            DisplayRegSave(True)
            Me.Close()




        Catch ex As Exception

        End Try
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        caseDate.Text = Date.Now.ToString("MM-dd-yyyy")
        caseTime.Text = Date.Now.ToString("hh:mm:ss tt")
    End Sub
End Class