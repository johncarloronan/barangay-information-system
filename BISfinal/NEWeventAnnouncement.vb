﻿Imports FireSharp.Config
Imports FireSharp.Response
Imports FireSharp.Interfaces
'-------------------------------------------

Imports System.Web.Script.Serialization '(Importing System.Web.Script.Serialization) This is used to read and convert JSON strings.
Imports System.ComponentModel '(Importing System.ComponentModel) This is used to sort data on DataGridView.
Imports System.IO

Public Class NEWeventAnnouncement

    Dim bmp As Bitmap

    Dim clearDGVCol As Boolean = True

    Private dtTableGrd As DataTable


    Private fcon As New FirebaseConfig() With
        {
        .AuthSecret = "8kPwBHDkDdZWGHts9sb98dsJu45thBvgDTUrgqJj",
        .BasePath = "https://bisrealtime-default-rtdb.firebaseio.com/"
        }

    Private client As IFirebaseClient



    Sub ShowRecord()
        Try
            Dim dtTable As New DataTable
            dtTable.Columns.Add("Posted")
            dtTable.Columns.Add("Theme")
            dtTable.Columns.Add("What")
            dtTable.Columns.Add("Why")
            dtTable.Columns.Add("When")
            dtTable.Columns.Add("Who")
            dtTable.Columns.Add("Where")



            If clearDGVCol = True Then

                clearDGVCol = False
            End If


            Dim SRRecord = client.Get("ResidentDB/")


            Dim myJsonTool As New JavaScriptSerializer
            Dim myDeserializedItems = myJsonTool.Deserialize(Of Dictionary(Of String, AnnouncementDATA))(SRRecord.Body)


            For Each dictItem As KeyValuePair(Of String, AnnouncementDATA) In myDeserializedItems
                dtTable.Rows.Add(dictItem.Value.hPosted, dictItem.Value.hTheme, dictItem.Value.hWhat, dictItem.Value.hWhy, dictItem.Value.hWhen, dictItem.Value.hWho, dictItem.Value.hWhere)
            Next


            dtTableGrd = dtTable



        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ElseIf ex.Message = "Object reference not set to an instance of an object." Then
                Dim dtTable As New DataTable
                dtTable.Columns.Add("Posted")
                dtTable.Columns.Add("Theme")
                dtTable.Columns.Add("What")
                dtTable.Columns.Add("Why")
                dtTable.Columns.Add("When")
                dtTable.Columns.Add("Who")
                dtTable.Columns.Add("Where")
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If



        End Try
    End Sub
    Private Sub NEWeventAnnouncement_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            client = New FireSharp.FirebaseClient(fcon)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Timer1.Enabled = True

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnAnnounce.Click

        If tbTHEME.Text = Nothing Then
            MessageBox.Show("THEME field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If tbWHAT.Text = Nothing Then
            MessageBox.Show("WHAT field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If tbWHY.Text = Nothing Then
            MessageBox.Show("WHY field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If



        If tbWHEN.Text = Nothing Then
            MessageBox.Show("WHEN field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If tbWHO.Text = Nothing Then
            MessageBox.Show("WHO is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If tbWHERE.Text = Nothing Then
            MessageBox.Show("WHERE field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If


        Try

            Dim AD As New AnnouncementDATA() With
                {
            .hPosted = Ldate.Text,
            .hTheme = tbTHEME.Text,
            .hWhat = tbWHAT.Text,
            .hWhy = tbWHY.Text,
            .hWhen = tbWHEN.Text,
            .hWho = tbWHO.Text,
            .hWhere = tbWHERE.Text
                }
            Dim save = client.Set("newAnnouncementDB/" + tbTHEME.Text, AD)
            tbTHEME.Text = ""
            tbWHAT.Text = ""
            tbWHY.Text = ""
            tbWHEN.Text = ""
            tbWHO.Text = ""
            tbWHERE.Text = ""
            Me.Close()




        Catch ex As Exception

        End Try
    End Sub

    Private Sub Label7_Click(sender As Object, e As EventArgs) Handles Ldate.Click

    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Ldate.Text = Date.Now.ToString("MM-dd-yyyy")
    End Sub
End Class