﻿Imports FireSharp.Config
Imports FireSharp.Response
Imports FireSharp.Interfaces



Imports System.Web.Script.Serialization '(Importing System.Web.Script.Serialization) This is used to read and convert JSON strings.
Imports System.ComponentModel '(Importing System.ComponentModel) This is used to sort data on DataGridView.
Imports System.IO

Public Class DASHofficials

    Dim clearDGVCol As Boolean = True

    Private dtTableGrd As DataTable


    Private fcon As New FirebaseConfig() With
        {
        .AuthSecret = "8kPwBHDkDdZWGHts9sb98dsJu45thBvgDTUrgqJj",
        .BasePath = "https://bisrealtime-default-rtdb.firebaseio.com/"
        }

    Private client As IFirebaseClient


    Sub ShowRecord()
        Try
            Dim dtTable As New DataTable
            dtTable.Columns.Add("Name")
            dtTable.Columns.Add("Position")
            dtTable.Columns.Add("Num")




            If clearDGVCol = True Then
                DGVUserData.Columns.Clear()
                clearDGVCol = False
            End If
            '-------------------------------------------

            Dim SRRecord = client.Get("OfficialsDB/")

            Dim myJsonTool As New JavaScriptSerializer
            Dim myDeserializedItems = myJsonTool.Deserialize(Of Dictionary(Of String, OfficialsDATA))(SRRecord.Body)

            For Each dictItem As KeyValuePair(Of String, OfficialsDATA) In myDeserializedItems
                dtTable.Rows.Add(dictItem.Value.iName, dictItem.Value.iPosition, dictItem.Value.iNum)
            Next


            DGVUserData.DataSource = dtTable
            dtTableGrd = dtTable

            DGVUserData.Sort(DGVUserData.Columns(1), ListSortDirection.Ascending)
            DGVUserData.ClearSelection()

        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ElseIf ex.Message = "Object reference not set to an instance of an object." Then
                Dim dtTable As New DataTable
                dtTable.Columns.Add("Name")
                dtTable.Columns.Add("Position")
                dtTable.Columns.Add("Num")
                DGVUserData.DataSource = dtTable
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If



        End Try

    End Sub

    Private Sub DGVUserData_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVUserData.CellContentClick
        zName.Text = DGVUserData.SelectedRows(0).Cells("Name").Value
        zPosition.Text = DGVUserData.SelectedRows(0).Cells("Position").Value
        zNum.Text = DGVUserData.SelectedRows(0).Cells("Num").Value
        pasado_Click(sender, e)

    End Sub

    Private Sub DASHofficials_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            client = New FireSharp.FirebaseClient(fcon)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        ShowRecord()
        Dim clearDGVCol As Boolean = True


        Timer1.Interval = 30000
        Timer1.Enabled = True

    End Sub

    Private Sub btnrefresh_Click(sender As Object, e As EventArgs) Handles btnrefresh.Click
        ShowRecord()

    End Sub

    Private Sub pasado_Click(sender As Object, e As EventArgs) Handles pasado.Click
        Dim TR As New addOfficials

        TR.UPDATEoffname = zName.Text
        TR.updateoffposition = zPosition.Text
        TR.UPDATEoffnum = zNum.Text
        TR.Show()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        btnrefresh_Click(sender, e)
    End Sub
End Class