﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class requestBcedula
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LTime = New System.Windows.Forms.Label()
        Me.LDate = New System.Windows.Forms.Label()
        Me.TBname = New System.Windows.Forms.TextBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.CBrequest = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.btnRequest = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox10 = New System.Windows.Forms.PictureBox()
        Me.tbid = New System.Windows.Forms.Label()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LTime
        '
        Me.LTime.AutoSize = True
        Me.LTime.BackColor = System.Drawing.Color.Transparent
        Me.LTime.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LTime.ForeColor = System.Drawing.Color.Black
        Me.LTime.Location = New System.Drawing.Point(405, 74)
        Me.LTime.Name = "LTime"
        Me.LTime.Size = New System.Drawing.Size(46, 19)
        Me.LTime.TabIndex = 162
        Me.LTime.Text = "TIME"
        '
        'LDate
        '
        Me.LDate.AutoSize = True
        Me.LDate.BackColor = System.Drawing.Color.Transparent
        Me.LDate.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LDate.ForeColor = System.Drawing.Color.Black
        Me.LDate.Location = New System.Drawing.Point(405, 43)
        Me.LDate.Name = "LDate"
        Me.LDate.Size = New System.Drawing.Size(47, 19)
        Me.LDate.TabIndex = 161
        Me.LDate.Text = "DATE"
        '
        'TBname
        '
        Me.TBname.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBname.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBname.ForeColor = System.Drawing.Color.Black
        Me.TBname.Location = New System.Drawing.Point(55, 133)
        Me.TBname.Name = "TBname"
        Me.TBname.Size = New System.Drawing.Size(178, 26)
        Me.TBname.TabIndex = 156
        '
        'Timer1
        '
        '
        'CBrequest
        '
        Me.CBrequest.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CBrequest.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBrequest.FormattingEnabled = True
        Me.CBrequest.Items.AddRange(New Object() {"Blotter", "Clearance", "Cedula"})
        Me.CBrequest.Location = New System.Drawing.Point(55, 165)
        Me.CBrequest.Name = "CBrequest"
        Me.CBrequest.Size = New System.Drawing.Size(138, 27)
        Me.CBrequest.TabIndex = 165
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(123, 39)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(102, 22)
        Me.Label1.TabIndex = 166
        Me.Label1.Text = "REQUEST"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(138, 60)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(91, 22)
        Me.Label2.TabIndex = 167
        Me.Label2.Text = "CENTER"
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.BISfinal.My.Resources.Resources.calendar__1_
        Me.PictureBox2.Location = New System.Drawing.Point(366, 70)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(33, 26)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox2.TabIndex = 164
        Me.PictureBox2.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = Global.BISfinal.My.Resources.Resources.clock
        Me.PictureBox3.Location = New System.Drawing.Point(366, 39)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(33, 26)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox3.TabIndex = 163
        Me.PictureBox3.TabStop = False
        '
        'btnRequest
        '
        Me.btnRequest.BackgroundImage = Global.BISfinal.My.Resources.Resources.iconfinder_2540592_pinterest_plane_paper_airplane_send_icon_512px
        Me.btnRequest.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnRequest.FlatAppearance.BorderSize = 0
        Me.btnRequest.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRequest.Location = New System.Drawing.Point(186, 155)
        Me.btnRequest.Name = "btnRequest"
        Me.btnRequest.Size = New System.Drawing.Size(56, 45)
        Me.btnRequest.TabIndex = 160
        Me.btnRequest.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.BISfinal.My.Resources.Resources.PinClipart_com_fastidious_clipart_3337407
        Me.PictureBox1.Location = New System.Drawing.Point(16, 165)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(33, 26)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 159
        Me.PictureBox1.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = Global.BISfinal.My.Resources.Resources.user
        Me.PictureBox4.Location = New System.Drawing.Point(16, 133)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(33, 26)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox4.TabIndex = 157
        Me.PictureBox4.TabStop = False
        '
        'PictureBox10
        '
        Me.PictureBox10.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox10.BackgroundImage = Global.BISfinal.My.Resources.Resources.PinClipart1
        Me.PictureBox10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox10.Location = New System.Drawing.Point(-6, 12)
        Me.PictureBox10.Name = "PictureBox10"
        Me.PictureBox10.Size = New System.Drawing.Size(148, 101)
        Me.PictureBox10.TabIndex = 155
        Me.PictureBox10.TabStop = False
        '
        'tbid
        '
        Me.tbid.AutoSize = True
        Me.tbid.Location = New System.Drawing.Point(183, 12)
        Me.tbid.Name = "tbid"
        Me.tbid.Size = New System.Drawing.Size(39, 13)
        Me.tbid.TabIndex = 168
        Me.tbid.Text = "Label3"
        Me.tbid.Visible = False
        '
        'requestBcedula
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(256, 205)
        Me.Controls.Add(Me.tbid)
        Me.Controls.Add(Me.CBrequest)
        Me.Controls.Add(Me.TBname)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.LTime)
        Me.Controls.Add(Me.LDate)
        Me.Controls.Add(Me.btnRequest)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(Me.PictureBox10)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "requestBcedula"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents LTime As Label
    Friend WithEvents LDate As Label
    Friend WithEvents btnRequest As Button
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents PictureBox4 As PictureBox
    Friend WithEvents TBname As TextBox
    Friend WithEvents PictureBox10 As PictureBox
    Friend WithEvents Timer1 As Timer
    Friend WithEvents CBrequest As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents tbid As Label
End Class
