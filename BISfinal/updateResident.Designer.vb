﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class updateResident
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.tbSearch = New System.Windows.Forms.TextBox()
        Me.CBsb = New System.Windows.Forms.ComboBox()
        Me.DGVUserData = New System.Windows.Forms.DataGridView()
        Me.bFullname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bLastname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bFname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bMname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bBdate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bAge = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bGender = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bAddress = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bBplace = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bProvince = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bContact = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bBtype = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bCitizenship = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bReligion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bEmail = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OpenFileDialogUserImage = New System.Windows.Forms.OpenFileDialog()
        Me.btnS = New System.Windows.Forms.Button()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.PictureBoxUserReg = New System.Windows.Forms.PictureBox()
        Me.CBvoter = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.TBoccupation = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TBCitizenship = New System.Windows.Forms.TextBox()
        Me.CBStatus = New System.Windows.Forms.ComboBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TBFullname = New System.Windows.Forms.TextBox()
        Me.TBid = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TBReligion = New System.Windows.Forms.TextBox()
        Me.CBBtype = New System.Windows.Forms.ComboBox()
        Me.TBContact = New System.Windows.Forms.TextBox()
        Me.TBProvince = New System.Windows.Forms.TextBox()
        Me.TBBplace = New System.Windows.Forms.TextBox()
        Me.TBAddress = New System.Windows.Forms.TextBox()
        Me.CBGender = New System.Windows.Forms.ComboBox()
        Me.TBAge = New System.Windows.Forms.TextBox()
        Me.dtBday = New System.Windows.Forms.DateTimePicker()
        Me.TBSuffix = New System.Windows.Forms.TextBox()
        Me.TBMname = New System.Windows.Forms.TextBox()
        Me.TBFname = New System.Windows.Forms.TextBox()
        Me.TBLname = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.DGVUserData, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxUserReg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbSearch
        '
        Me.tbSearch.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.tbSearch.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSearch.ForeColor = System.Drawing.Color.White
        Me.tbSearch.Location = New System.Drawing.Point(635, 122)
        Me.tbSearch.Name = "tbSearch"
        Me.tbSearch.Size = New System.Drawing.Size(190, 26)
        Me.tbSearch.TabIndex = 72
        '
        'CBsb
        '
        Me.CBsb.FormattingEnabled = True
        Me.CBsb.Items.AddRange(New Object() {"LastName"})
        Me.CBsb.Location = New System.Drawing.Point(247, 433)
        Me.CBsb.Name = "CBsb"
        Me.CBsb.Size = New System.Drawing.Size(46, 21)
        Me.CBsb.TabIndex = 74
        Me.CBsb.Text = "LastName"
        Me.CBsb.Visible = False
        '
        'DGVUserData
        '
        Me.DGVUserData.AllowUserToAddRows = False
        Me.DGVUserData.AllowUserToDeleteRows = False
        Me.DGVUserData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells
        Me.DGVUserData.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.DGVUserData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.DGVUserData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVUserData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.bFullname, Me.bID, Me.bLastname, Me.bFname, Me.bMname, Me.bBdate, Me.bAge, Me.bGender, Me.bAddress, Me.bBplace, Me.bProvince, Me.bContact, Me.bBtype, Me.bCitizenship, Me.bReligion, Me.bEmail})
        Me.DGVUserData.Location = New System.Drawing.Point(594, 127)
        Me.DGVUserData.Name = "DGVUserData"
        Me.DGVUserData.ReadOnly = True
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        Me.DGVUserData.RowsDefaultCellStyle = DataGridViewCellStyle1
        Me.DGVUserData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGVUserData.Size = New System.Drawing.Size(231, 162)
        Me.DGVUserData.TabIndex = 73
        Me.DGVUserData.Visible = False
        '
        'bFullname
        '
        Me.bFullname.HeaderText = "Fullname"
        Me.bFullname.Name = "bFullname"
        Me.bFullname.ReadOnly = True
        Me.bFullname.Width = 74
        '
        'bID
        '
        Me.bID.HeaderText = "ID"
        Me.bID.Name = "bID"
        Me.bID.ReadOnly = True
        Me.bID.Width = 43
        '
        'bLastname
        '
        Me.bLastname.HeaderText = "Last Name"
        Me.bLastname.Name = "bLastname"
        Me.bLastname.ReadOnly = True
        Me.bLastname.Width = 83
        '
        'bFname
        '
        Me.bFname.HeaderText = "First Name"
        Me.bFname.Name = "bFname"
        Me.bFname.ReadOnly = True
        Me.bFname.Width = 82
        '
        'bMname
        '
        Me.bMname.HeaderText = "Middle Name"
        Me.bMname.Name = "bMname"
        Me.bMname.ReadOnly = True
        Me.bMname.Width = 94
        '
        'bBdate
        '
        Me.bBdate.HeaderText = "Birth Date"
        Me.bBdate.Name = "bBdate"
        Me.bBdate.ReadOnly = True
        Me.bBdate.Width = 79
        '
        'bAge
        '
        Me.bAge.HeaderText = "Age"
        Me.bAge.Name = "bAge"
        Me.bAge.ReadOnly = True
        Me.bAge.Width = 51
        '
        'bGender
        '
        Me.bGender.HeaderText = "Gender"
        Me.bGender.Name = "bGender"
        Me.bGender.ReadOnly = True
        Me.bGender.Width = 67
        '
        'bAddress
        '
        Me.bAddress.HeaderText = "Address"
        Me.bAddress.Name = "bAddress"
        Me.bAddress.ReadOnly = True
        Me.bAddress.Width = 70
        '
        'bBplace
        '
        Me.bBplace.HeaderText = "Birth Place"
        Me.bBplace.Name = "bBplace"
        Me.bBplace.ReadOnly = True
        Me.bBplace.Width = 83
        '
        'bProvince
        '
        Me.bProvince.HeaderText = "Province"
        Me.bProvince.Name = "bProvince"
        Me.bProvince.ReadOnly = True
        Me.bProvince.Width = 74
        '
        'bContact
        '
        Me.bContact.HeaderText = "Contact No."
        Me.bContact.Name = "bContact"
        Me.bContact.ReadOnly = True
        Me.bContact.Width = 89
        '
        'bBtype
        '
        Me.bBtype.HeaderText = "Blood Type"
        Me.bBtype.Name = "bBtype"
        Me.bBtype.ReadOnly = True
        Me.bBtype.Width = 86
        '
        'bCitizenship
        '
        Me.bCitizenship.HeaderText = "Citizenship"
        Me.bCitizenship.Name = "bCitizenship"
        Me.bCitizenship.ReadOnly = True
        Me.bCitizenship.Width = 82
        '
        'bReligion
        '
        Me.bReligion.HeaderText = "Religion"
        Me.bReligion.Name = "bReligion"
        Me.bReligion.ReadOnly = True
        Me.bReligion.Width = 70
        '
        'bEmail
        '
        Me.bEmail.HeaderText = "Email"
        Me.bEmail.Name = "bEmail"
        Me.bEmail.ReadOnly = True
        Me.bEmail.Width = 57
        '
        'OpenFileDialogUserImage
        '
        Me.OpenFileDialogUserImage.FileName = "OpenFileDialog1"
        '
        'btnS
        '
        Me.btnS.BackColor = System.Drawing.Color.Transparent
        Me.btnS.BackgroundImage = Global.BISfinal.My.Resources.Resources.f9bb81e576c1a361c61a8c08945b2c48_search_icon_by_vexels
        Me.btnS.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnS.FlatAppearance.BorderSize = 0
        Me.btnS.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnS.Location = New System.Drawing.Point(795, 125)
        Me.btnS.Name = "btnS"
        Me.btnS.Size = New System.Drawing.Size(25, 21)
        Me.btnS.TabIndex = 80
        Me.btnS.UseVisualStyleBackColor = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Location = New System.Drawing.Point(616, 260)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(224, 29)
        Me.PictureBox3.TabIndex = 79
        Me.PictureBox3.TabStop = False
        Me.PictureBox3.Visible = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Location = New System.Drawing.Point(608, 122)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(27, 167)
        Me.PictureBox2.TabIndex = 78
        Me.PictureBox2.TabStop = False
        '
        'btnUpdate
        '
        Me.btnUpdate.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnUpdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnUpdate.FlatAppearance.BorderSize = 0
        Me.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUpdate.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpdate.ForeColor = System.Drawing.Color.White
        Me.btnUpdate.Location = New System.Drawing.Point(375, 414)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(134, 30)
        Me.btnUpdate.TabIndex = 70
        Me.btnUpdate.Text = "UPDATE"
        Me.btnUpdate.UseVisualStyleBackColor = False
        '
        'PictureBoxUserReg
        '
        Me.PictureBoxUserReg.BackColor = System.Drawing.Color.Transparent
        Me.PictureBoxUserReg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxUserReg.Location = New System.Drawing.Point(321, 12)
        Me.PictureBoxUserReg.Name = "PictureBoxUserReg"
        Me.PictureBoxUserReg.Size = New System.Drawing.Size(176, 125)
        Me.PictureBoxUserReg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBoxUserReg.TabIndex = 81
        Me.PictureBoxUserReg.TabStop = False
        '
        'CBvoter
        '
        Me.CBvoter.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CBvoter.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBvoter.ForeColor = System.Drawing.Color.Black
        Me.CBvoter.FormattingEnabled = True
        Me.CBvoter.Items.AddRange(New Object() {"Yes", "No"})
        Me.CBvoter.Location = New System.Drawing.Point(375, 221)
        Me.CBvoter.Name = "CBvoter"
        Me.CBvoter.Size = New System.Drawing.Size(134, 23)
        Me.CBvoter.TabIndex = 132
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(265, 224)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(104, 15)
        Me.Label13.TabIndex = 131
        Me.Label13.Text = "Registered Voter:"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.Black
        Me.Label21.Location = New System.Drawing.Point(265, 196)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(73, 15)
        Me.Label21.TabIndex = 130
        Me.Label21.Text = "Occupation:"
        '
        'TBoccupation
        '
        Me.TBoccupation.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBoccupation.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBoccupation.ForeColor = System.Drawing.Color.Black
        Me.TBoccupation.Location = New System.Drawing.Point(375, 193)
        Me.TBoccupation.Name = "TBoccupation"
        Me.TBoccupation.Size = New System.Drawing.Size(134, 22)
        Me.TBoccupation.TabIndex = 129
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.Black
        Me.Label18.Location = New System.Drawing.Point(265, 363)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(74, 15)
        Me.Label18.TabIndex = 128
        Me.Label18.Text = "Citizenship:"
        '
        'TBCitizenship
        '
        Me.TBCitizenship.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBCitizenship.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBCitizenship.ForeColor = System.Drawing.Color.Black
        Me.TBCitizenship.Location = New System.Drawing.Point(375, 360)
        Me.TBCitizenship.Name = "TBCitizenship"
        Me.TBCitizenship.Size = New System.Drawing.Size(134, 22)
        Me.TBCitizenship.TabIndex = 127
        '
        'CBStatus
        '
        Me.CBStatus.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CBStatus.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBStatus.ForeColor = System.Drawing.Color.Black
        Me.CBStatus.FormattingEnabled = True
        Me.CBStatus.Items.AddRange(New Object() {"Single", "Married", "Divorse"})
        Me.CBStatus.Location = New System.Drawing.Point(375, 307)
        Me.CBStatus.Name = "CBStatus"
        Me.CBStatus.Size = New System.Drawing.Size(134, 23)
        Me.CBStatus.TabIndex = 126
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Black
        Me.Label15.Location = New System.Drawing.Point(265, 310)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(46, 15)
        Me.Label15.TabIndex = 125
        Me.Label15.Text = "Status:"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.Black
        Me.Label17.Location = New System.Drawing.Point(11, 160)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(67, 15)
        Me.Label17.TabIndex = 124
        Me.Label17.Text = "Full Name:"
        '
        'TBFullname
        '
        Me.TBFullname.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBFullname.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBFullname.ForeColor = System.Drawing.Color.Black
        Me.TBFullname.Location = New System.Drawing.Point(107, 157)
        Me.TBFullname.Name = "TBFullname"
        Me.TBFullname.Size = New System.Drawing.Size(134, 22)
        Me.TBFullname.TabIndex = 123
        '
        'TBid
        '
        Me.TBid.AutoSize = True
        Me.TBid.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBid.ForeColor = System.Drawing.Color.Black
        Me.TBid.Location = New System.Drawing.Point(107, 182)
        Me.TBid.Name = "TBid"
        Me.TBid.Size = New System.Drawing.Size(49, 19)
        Me.TBid.TabIndex = 122
        Me.TBid.Text = "00000"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Black
        Me.Label16.Location = New System.Drawing.Point(11, 185)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(24, 15)
        Me.Label16.TabIndex = 121
        Me.Label16.Text = "ID:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Black
        Me.Label14.Location = New System.Drawing.Point(265, 389)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(58, 15)
        Me.Label14.TabIndex = 120
        Me.Label14.Text = "Religion:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Black
        Me.Label12.Location = New System.Drawing.Point(265, 336)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(70, 15)
        Me.Label12.TabIndex = 119
        Me.Label12.Text = "Blood Type:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Black
        Me.Label11.Location = New System.Drawing.Point(265, 282)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(74, 15)
        Me.Label11.TabIndex = 118
        Me.Label11.Text = "Contact No.:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Black
        Me.Label10.Location = New System.Drawing.Point(265, 255)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(59, 15)
        Me.Label10.TabIndex = 117
        Me.Label10.Text = "Province:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Black
        Me.Label9.Location = New System.Drawing.Point(11, 435)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(74, 15)
        Me.Label9.TabIndex = 116
        Me.Label9.Text = "Birth Place:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(11, 407)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(92, 15)
        Me.Label8.TabIndex = 115
        Me.Label8.Text = "Street Address:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(11, 378)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(51, 15)
        Me.Label7.TabIndex = 114
        Me.Label7.Text = "Gender:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(11, 350)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(33, 15)
        Me.Label6.TabIndex = 113
        Me.Label6.Text = "Age:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(11, 325)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(69, 15)
        Me.Label5.TabIndex = 112
        Me.Label5.Text = "Birth Date:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(11, 294)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(44, 15)
        Me.Label4.TabIndex = 111
        Me.Label4.Text = "Suffix:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(11, 266)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(82, 15)
        Me.Label3.TabIndex = 110
        Me.Label3.Text = "Middle Name:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(11, 238)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 15)
        Me.Label2.TabIndex = 109
        Me.Label2.Text = "First Name:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(11, 210)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 15)
        Me.Label1.TabIndex = 108
        Me.Label1.Text = "Last Name:"
        '
        'TBReligion
        '
        Me.TBReligion.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBReligion.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBReligion.ForeColor = System.Drawing.Color.Black
        Me.TBReligion.Location = New System.Drawing.Point(375, 386)
        Me.TBReligion.Name = "TBReligion"
        Me.TBReligion.Size = New System.Drawing.Size(134, 22)
        Me.TBReligion.TabIndex = 107
        '
        'CBBtype
        '
        Me.CBBtype.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CBBtype.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBBtype.ForeColor = System.Drawing.Color.Black
        Me.CBBtype.FormattingEnabled = True
        Me.CBBtype.Items.AddRange(New Object() {"A", "B", "O", "A+", "B+", "O+", "N/A"})
        Me.CBBtype.Location = New System.Drawing.Point(375, 333)
        Me.CBBtype.Name = "CBBtype"
        Me.CBBtype.Size = New System.Drawing.Size(134, 23)
        Me.CBBtype.TabIndex = 106
        '
        'TBContact
        '
        Me.TBContact.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBContact.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBContact.ForeColor = System.Drawing.Color.Black
        Me.TBContact.Location = New System.Drawing.Point(375, 279)
        Me.TBContact.Name = "TBContact"
        Me.TBContact.Size = New System.Drawing.Size(134, 22)
        Me.TBContact.TabIndex = 105
        '
        'TBProvince
        '
        Me.TBProvince.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBProvince.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBProvince.ForeColor = System.Drawing.Color.Black
        Me.TBProvince.Location = New System.Drawing.Point(375, 252)
        Me.TBProvince.Name = "TBProvince"
        Me.TBProvince.Size = New System.Drawing.Size(134, 22)
        Me.TBProvince.TabIndex = 104
        '
        'TBBplace
        '
        Me.TBBplace.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBBplace.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBBplace.ForeColor = System.Drawing.Color.Black
        Me.TBBplace.Location = New System.Drawing.Point(107, 432)
        Me.TBBplace.Name = "TBBplace"
        Me.TBBplace.Size = New System.Drawing.Size(134, 22)
        Me.TBBplace.TabIndex = 103
        '
        'TBAddress
        '
        Me.TBAddress.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBAddress.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBAddress.ForeColor = System.Drawing.Color.Black
        Me.TBAddress.Location = New System.Drawing.Point(107, 404)
        Me.TBAddress.Name = "TBAddress"
        Me.TBAddress.Size = New System.Drawing.Size(134, 22)
        Me.TBAddress.TabIndex = 102
        '
        'CBGender
        '
        Me.CBGender.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CBGender.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBGender.ForeColor = System.Drawing.Color.Black
        Me.CBGender.FormattingEnabled = True
        Me.CBGender.Items.AddRange(New Object() {"Male", "Female"})
        Me.CBGender.Location = New System.Drawing.Point(107, 375)
        Me.CBGender.Name = "CBGender"
        Me.CBGender.Size = New System.Drawing.Size(134, 23)
        Me.CBGender.TabIndex = 101
        '
        'TBAge
        '
        Me.TBAge.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBAge.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBAge.ForeColor = System.Drawing.Color.Black
        Me.TBAge.Location = New System.Drawing.Point(107, 347)
        Me.TBAge.Name = "TBAge"
        Me.TBAge.Size = New System.Drawing.Size(134, 22)
        Me.TBAge.TabIndex = 100
        '
        'dtBday
        '
        Me.dtBday.CalendarForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.dtBday.CalendarMonthBackground = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dtBday.CalendarTitleBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dtBday.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtBday.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtBday.Location = New System.Drawing.Point(107, 319)
        Me.dtBday.Name = "dtBday"
        Me.dtBday.Size = New System.Drawing.Size(134, 22)
        Me.dtBday.TabIndex = 99
        '
        'TBSuffix
        '
        Me.TBSuffix.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBSuffix.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBSuffix.ForeColor = System.Drawing.Color.Black
        Me.TBSuffix.Location = New System.Drawing.Point(107, 291)
        Me.TBSuffix.Name = "TBSuffix"
        Me.TBSuffix.Size = New System.Drawing.Size(134, 22)
        Me.TBSuffix.TabIndex = 98
        '
        'TBMname
        '
        Me.TBMname.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBMname.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBMname.ForeColor = System.Drawing.Color.Black
        Me.TBMname.Location = New System.Drawing.Point(107, 263)
        Me.TBMname.Name = "TBMname"
        Me.TBMname.Size = New System.Drawing.Size(134, 22)
        Me.TBMname.TabIndex = 97
        '
        'TBFname
        '
        Me.TBFname.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBFname.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBFname.ForeColor = System.Drawing.Color.Black
        Me.TBFname.Location = New System.Drawing.Point(107, 235)
        Me.TBFname.Name = "TBFname"
        Me.TBFname.Size = New System.Drawing.Size(134, 22)
        Me.TBFname.TabIndex = 96
        '
        'TBLname
        '
        Me.TBLname.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBLname.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBLname.ForeColor = System.Drawing.Color.Black
        Me.TBLname.Location = New System.Drawing.Point(107, 207)
        Me.TBLname.Name = "TBLname"
        Me.TBLname.Size = New System.Drawing.Size(134, 22)
        Me.TBLname.TabIndex = 95
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.Label20.Location = New System.Drawing.Point(155, 58)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(109, 22)
        Me.Label20.TabIndex = 135
        Me.Label20.Text = "RESIDENT"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.Label19.Location = New System.Drawing.Point(141, 36)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(110, 22)
        Me.Label19.TabIndex = 134
        Me.Label19.Text = "REGISTER"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = Global.BISfinal.My.Resources.Resources.pngwing_com__1_
        Me.PictureBox1.Location = New System.Drawing.Point(-5, 14)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(161, 106)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 133
        Me.PictureBox1.TabStop = False
        '
        'updateResident
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(529, 467)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.CBvoter)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.TBoccupation)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.TBCitizenship)
        Me.Controls.Add(Me.CBStatus)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.TBFullname)
        Me.Controls.Add(Me.TBid)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TBReligion)
        Me.Controls.Add(Me.CBBtype)
        Me.Controls.Add(Me.TBContact)
        Me.Controls.Add(Me.TBProvince)
        Me.Controls.Add(Me.TBBplace)
        Me.Controls.Add(Me.TBAddress)
        Me.Controls.Add(Me.CBGender)
        Me.Controls.Add(Me.TBAge)
        Me.Controls.Add(Me.dtBday)
        Me.Controls.Add(Me.TBSuffix)
        Me.Controls.Add(Me.TBMname)
        Me.Controls.Add(Me.TBFname)
        Me.Controls.Add(Me.TBLname)
        Me.Controls.Add(Me.btnS)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.tbSearch)
        Me.Controls.Add(Me.CBsb)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.DGVUserData)
        Me.Controls.Add(Me.PictureBoxUserReg)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "updateResident"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "UPDATE"
        CType(Me.DGVUserData, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxUserReg, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnUpdate As Button
    Friend WithEvents tbSearch As TextBox
    Friend WithEvents CBsb As ComboBox
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents btnS As Button
    Friend WithEvents DGVUserData As DataGridView
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents PictureBoxUserReg As PictureBox
    Friend WithEvents OpenFileDialogUserImage As OpenFileDialog
    Friend WithEvents bFullname As DataGridViewTextBoxColumn
    Friend WithEvents bID As DataGridViewTextBoxColumn
    Friend WithEvents bLastname As DataGridViewTextBoxColumn
    Friend WithEvents bFname As DataGridViewTextBoxColumn
    Friend WithEvents bMname As DataGridViewTextBoxColumn
    Friend WithEvents bBdate As DataGridViewTextBoxColumn
    Friend WithEvents bAge As DataGridViewTextBoxColumn
    Friend WithEvents bGender As DataGridViewTextBoxColumn
    Friend WithEvents bAddress As DataGridViewTextBoxColumn
    Friend WithEvents bBplace As DataGridViewTextBoxColumn
    Friend WithEvents bProvince As DataGridViewTextBoxColumn
    Friend WithEvents bContact As DataGridViewTextBoxColumn
    Friend WithEvents bBtype As DataGridViewTextBoxColumn
    Friend WithEvents bCitizenship As DataGridViewTextBoxColumn
    Friend WithEvents bReligion As DataGridViewTextBoxColumn
    Friend WithEvents bEmail As DataGridViewTextBoxColumn
    Friend WithEvents CBvoter As ComboBox
    Friend WithEvents Label13 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents TBoccupation As TextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents TBCitizenship As TextBox
    Friend WithEvents CBStatus As ComboBox
    Friend WithEvents Label15 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents TBFullname As TextBox
    Friend WithEvents TBid As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents TBReligion As TextBox
    Friend WithEvents CBBtype As ComboBox
    Friend WithEvents TBContact As TextBox
    Friend WithEvents TBProvince As TextBox
    Friend WithEvents TBBplace As TextBox
    Friend WithEvents TBAddress As TextBox
    Friend WithEvents CBGender As ComboBox
    Friend WithEvents TBAge As TextBox
    Friend WithEvents dtBday As DateTimePicker
    Friend WithEvents TBSuffix As TextBox
    Friend WithEvents TBMname As TextBox
    Friend WithEvents TBFname As TextBox
    Friend WithEvents TBLname As TextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents PictureBox1 As PictureBox
End Class
