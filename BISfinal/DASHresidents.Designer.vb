﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DASHresidents
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DGVUserData = New System.Windows.Forms.DataGridView()
        Me.bFullname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bLname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bFname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bMname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bSuffix = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bBdate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bAge = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bGender = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bAddress = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bBplace = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bProvince = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bContact = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bStatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bBtype = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bCitizenship = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bReligion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Image = New System.Windows.Forms.DataGridViewImageColumn()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.DGVUserData, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGVUserData
        '
        Me.DGVUserData.AllowUserToAddRows = False
        Me.DGVUserData.AllowUserToDeleteRows = False
        Me.DGVUserData.AllowUserToResizeColumns = False
        Me.DGVUserData.AllowUserToResizeRows = False
        Me.DGVUserData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DGVUserData.BackgroundColor = System.Drawing.Color.AliceBlue
        Me.DGVUserData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.DGVUserData.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.SteelBlue
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVUserData.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DGVUserData.ColumnHeadersHeight = 35
        Me.DGVUserData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.DGVUserData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.bFullname, Me.bID, Me.bLname, Me.bFname, Me.bMname, Me.bSuffix, Me.bBdate, Me.bAge, Me.bGender, Me.bAddress, Me.bBplace, Me.bProvince, Me.bContact, Me.bStatus, Me.bBtype, Me.bCitizenship, Me.bReligion, Me.Image})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightBlue
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGVUserData.DefaultCellStyle = DataGridViewCellStyle2
        Me.DGVUserData.EnableHeadersVisualStyles = False
        Me.DGVUserData.GridColor = System.Drawing.Color.DimGray
        Me.DGVUserData.Location = New System.Drawing.Point(476, 27)
        Me.DGVUserData.MultiSelect = False
        Me.DGVUserData.Name = "DGVUserData"
        Me.DGVUserData.ReadOnly = True
        Me.DGVUserData.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(52, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(52, Byte), Integer))
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVUserData.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DGVUserData.RowHeadersVisible = False
        Me.DGVUserData.RowHeadersWidth = 50
        Me.DGVUserData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGVUserData.Size = New System.Drawing.Size(252, 315)
        Me.DGVUserData.TabIndex = 2
        '
        'bFullname
        '
        Me.bFullname.HeaderText = "Fullname"
        Me.bFullname.Name = "bFullname"
        Me.bFullname.ReadOnly = True
        Me.bFullname.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.bFullname.Width = 94
        '
        'bID
        '
        Me.bID.HeaderText = "ID"
        Me.bID.Name = "bID"
        Me.bID.ReadOnly = True
        Me.bID.Width = 51
        '
        'bLname
        '
        Me.bLname.HeaderText = "LastName"
        Me.bLname.Name = "bLname"
        Me.bLname.ReadOnly = True
        Me.bLname.Width = 103
        '
        'bFname
        '
        Me.bFname.HeaderText = "First Name"
        Me.bFname.Name = "bFname"
        Me.bFname.ReadOnly = True
        Me.bFname.Width = 108
        '
        'bMname
        '
        Me.bMname.HeaderText = "Middle Name"
        Me.bMname.Name = "bMname"
        Me.bMname.ReadOnly = True
        Me.bMname.Width = 125
        '
        'bSuffix
        '
        Me.bSuffix.HeaderText = "Suffix"
        Me.bSuffix.Name = "bSuffix"
        Me.bSuffix.ReadOnly = True
        Me.bSuffix.Width = 72
        '
        'bBdate
        '
        Me.bBdate.HeaderText = "Birth Date"
        Me.bBdate.Name = "bBdate"
        Me.bBdate.ReadOnly = True
        Me.bBdate.Width = 104
        '
        'bAge
        '
        Me.bAge.HeaderText = "Age"
        Me.bAge.Name = "bAge"
        Me.bAge.ReadOnly = True
        Me.bAge.Width = 60
        '
        'bGender
        '
        Me.bGender.HeaderText = "Gender"
        Me.bGender.Name = "bGender"
        Me.bGender.ReadOnly = True
        Me.bGender.Width = 83
        '
        'bAddress
        '
        Me.bAddress.HeaderText = "Address"
        Me.bAddress.Name = "bAddress"
        Me.bAddress.ReadOnly = True
        Me.bAddress.Width = 88
        '
        'bBplace
        '
        Me.bBplace.HeaderText = "Birth Place"
        Me.bBplace.Name = "bBplace"
        Me.bBplace.ReadOnly = True
        Me.bBplace.Width = 107
        '
        'bProvince
        '
        Me.bProvince.HeaderText = "Province"
        Me.bProvince.Name = "bProvince"
        Me.bProvince.ReadOnly = True
        Me.bProvince.Width = 91
        '
        'bContact
        '
        Me.bContact.HeaderText = "Contact No."
        Me.bContact.Name = "bContact"
        Me.bContact.ReadOnly = True
        Me.bContact.Width = 113
        '
        'bStatus
        '
        Me.bStatus.HeaderText = "Status"
        Me.bStatus.Name = "bStatus"
        Me.bStatus.ReadOnly = True
        Me.bStatus.Width = 75
        '
        'bBtype
        '
        Me.bBtype.HeaderText = "Blood Type"
        Me.bBtype.Name = "bBtype"
        Me.bBtype.ReadOnly = True
        Me.bBtype.Width = 109
        '
        'bCitizenship
        '
        Me.bCitizenship.HeaderText = "Citizenship"
        Me.bCitizenship.Name = "bCitizenship"
        Me.bCitizenship.ReadOnly = True
        Me.bCitizenship.Width = 106
        '
        'bReligion
        '
        Me.bReligion.HeaderText = "Religion"
        Me.bReligion.Name = "bReligion"
        Me.bReligion.ReadOnly = True
        Me.bReligion.Width = 89
        '
        'Image
        '
        Me.Image.HeaderText = "Image"
        Me.Image.Name = "Image"
        Me.Image.ReadOnly = True
        Me.Image.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Image.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Image.Width = 75
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.BackColor = System.Drawing.Color.PaleTurquoise
        Me.lblTotal.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.ForeColor = System.Drawing.Color.Black
        Me.lblTotal.Location = New System.Drawing.Point(108, 13)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(28, 31)
        Me.lblTotal.TabIndex = 3
        Me.lblTotal.Text = "0"
        '
        'btnRefresh
        '
        Me.btnRefresh.Location = New System.Drawing.Point(362, 184)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(41, 23)
        Me.btnRefresh.TabIndex = 76
        Me.btnRefresh.Text = "R"
        Me.btnRefresh.UseVisualStyleBackColor = True
        Me.btnRefresh.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(6, 77)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(88, 15)
        Me.Label1.TabIndex = 79
        Me.Label1.Text = "POPULATION"
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.PaleTurquoise
        Me.PictureBox3.Image = Global.BISfinal.My.Resources.Resources.bar_chart
        Me.PictureBox3.Location = New System.Drawing.Point(5, 11)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(82, 50)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox3.TabIndex = 80
        Me.PictureBox3.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.White
        Me.PictureBox2.Location = New System.Drawing.Point(1, 69)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(174, 30)
        Me.PictureBox2.TabIndex = 78
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.PaleTurquoise
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(176, 100)
        Me.PictureBox1.TabIndex = 77
        Me.PictureBox1.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.BackColor = System.Drawing.Color.White
        Me.PictureBox4.Location = New System.Drawing.Point(1, 105)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(174, 30)
        Me.PictureBox4.TabIndex = 81
        Me.PictureBox4.TabStop = False
        '
        'Timer1
        '
        '
        'DASHresidents
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(764, 390)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.DGVUserData)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "DASHresidents"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "DASHresidents"
        CType(Me.DGVUserData, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DGVUserData As DataGridView
    Friend WithEvents bFullname As DataGridViewTextBoxColumn
    Friend WithEvents bID As DataGridViewTextBoxColumn
    Friend WithEvents bLname As DataGridViewTextBoxColumn
    Friend WithEvents bFname As DataGridViewTextBoxColumn
    Friend WithEvents bMname As DataGridViewTextBoxColumn
    Friend WithEvents bSuffix As DataGridViewTextBoxColumn
    Friend WithEvents bBdate As DataGridViewTextBoxColumn
    Friend WithEvents bAge As DataGridViewTextBoxColumn
    Friend WithEvents bGender As DataGridViewTextBoxColumn
    Friend WithEvents bAddress As DataGridViewTextBoxColumn
    Friend WithEvents bBplace As DataGridViewTextBoxColumn
    Friend WithEvents bProvince As DataGridViewTextBoxColumn
    Friend WithEvents bContact As DataGridViewTextBoxColumn
    Friend WithEvents bStatus As DataGridViewTextBoxColumn
    Friend WithEvents bBtype As DataGridViewTextBoxColumn
    Friend WithEvents bCitizenship As DataGridViewTextBoxColumn
    Friend WithEvents bReligion As DataGridViewTextBoxColumn
    Friend WithEvents Image As DataGridViewImageColumn
    Friend WithEvents lblTotal As Label
    Friend WithEvents btnRefresh As Button
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents Label1 As Label
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents PictureBox4 As PictureBox
    Friend WithEvents Timer1 As Timer
End Class
