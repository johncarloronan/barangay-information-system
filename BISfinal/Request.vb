﻿Imports FireSharp.Config
Imports FireSharp.Response
Imports FireSharp.Interfaces
'-------------------------------------------

Imports System.Web.Script.Serialization '(Importing System.Web.Script.Serialization) This is used to read and convert JSON strings.
Imports System.ComponentModel '(Importing System.ComponentModel) This is used to sort data on DataGridView.
Imports System.IO

Public Class requestBcedula
    Dim clearDGVCol As Boolean = True

    Private dtTableGrd As DataTable


    Private fcon As New FirebaseConfig() With
        {
        .AuthSecret = "8kPwBHDkDdZWGHts9sb98dsJu45thBvgDTUrgqJj",
        .BasePath = "https://bisrealtime-default-rtdb.firebaseio.com/"
        }

    Private client As IFirebaseClient



    Sub ShowRecord()
        Try
            Dim dtTable As New DataTable
            dtTable.Columns.Add("Name")
            dtTable.Columns.Add("Requested")
            dtTable.Columns.Add("Time")
            dtTable.Columns.Add("Number")

            If clearDGVCol = True Then

                clearDGVCol = False
            End If


            Dim SRRecord = client.Get("RequestsDB/")


            Dim myJsonTool As New JavaScriptSerializer
            Dim myDeserializedItems = myJsonTool.Deserialize(Of Dictionary(Of String, DATArequests))(SRRecord.Body)


            For Each dictItem As KeyValuePair(Of String, DATArequests) In myDeserializedItems
                dtTable.Rows.Add(dictItem.Value.gName, dictItem.Value.gRequested, dictItem.Value.gTime, dictItem.Value.gNum)
            Next


            dtTableGrd = dtTable



        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ElseIf ex.Message = "Object reference not set to an instance of an object." Then
                Dim dtTable As New DataTable
                dtTable.Columns.Add("Name")
                dtTable.Columns.Add("Requested")
                dtTable.Columns.Add("Time")
                dtTable.Columns.Add("Number")
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If



        End Try
    End Sub
    Private Sub requestBcedula_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            client = New FireSharp.FirebaseClient(fcon)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Try
            TBid.Text = "Please wait..."

            Dim r As Random = New Random
            Dim num As Integer
            num = (r.Next(1, 999999999))
            Dim IDresults As String = Strings.Right("00000000" & num.ToString(), 8)

            Dim Check_ID = client.Get("ResidentDB/" + IDresults)

            '-------------------------------------------Conditions to check whether the ID has been used.
            If Check_ID.Body <> "null" Then
                MessageBox.Show("The same ID is found, create another ID by pressing the Create ID button.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                TBid.Text = IDresults
            End If
            '-------------------------------------------
        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                TBid.Text = ""
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                TBid.Text = ""
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End Try

        Timer1.Enabled = True

        TBname.Text = " Name"
        TBname.ForeColor = Color.Gray
    End Sub

    Private Sub btnRequest_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub TBname_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub TBaddress_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub TBname_ContextMenuStripChanged(sender As Object, e As EventArgs)

    End Sub



    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        LDate.Text = Date.Now.ToString("MM-dd-yyyy")
        LTime.Text = Date.Now.ToString("hh:mm:ss tt")
    End Sub

    Private Sub TBname_TextChanged_1(sender As Object, e As EventArgs) Handles TBname.TextChanged
        TBname.ForeColor = Color.Black
    End Sub

    Private Sub TBname_Click(sender As Object, e As EventArgs) Handles TBname.Click
        If TBname.Text = " Name" Then
            TBname.Text = ""
        End If
    End Sub



    Private Sub btnRequest_Click_1(sender As Object, e As EventArgs) Handles btnRequest.Click
        If TBname.Text = Nothing Then
            MessageBox.Show("NAME field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If CBrequest.Text = Nothing Then
            MessageBox.Show("TYPE OF REQUEST field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If



        Try

            Dim PD As New DATArequests() With
                {
            .gName = TBname.Text,
            .gTime = LTime.Text,
            .gRequested = CBrequest.Text,
            .gNum = tbid.Text
                }
            Dim save = client.Set("RequestsDB/" + tbid.Text, PD)
            tbid.Text = ""
            TBname.Text = ""
            CBrequest.Text = ""
            Me.Close()


        Catch ex As Exception

        End Try
    End Sub
End Class