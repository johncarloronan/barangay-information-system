﻿Imports FireSharp.Config
Imports FireSharp.Response
Imports FireSharp.Interfaces

Imports Newtonsoft.Json

Imports System.Web.Script.Serialization '(Importing System.Web.Script.Serialization) This is used to read and convert JSON strings.
Imports System.ComponentModel '(Importing System.ComponentModel) This is used to sort data on DataGridView.
Imports System.IO

Public Class LISTevents

    Dim clearDGVCol As Boolean = True

    Private dtTableGrd As DataTable


    Private fcon As New FirebaseConfig() With
        {
        .AuthSecret = "8kPwBHDkDdZWGHts9sb98dsJu45thBvgDTUrgqJj",
        .BasePath = "https://bisrealtime-default-rtdb.firebaseio.com/"
        }

    Private client As IFirebaseClient



    Sub ShowRecord()
        Try
            Dim dtTable As New DataTable
            dtTable.Columns.Add("Theme")
            dtTable.Columns.Add("What")
            dtTable.Columns.Add("Why")
            dtTable.Columns.Add("When")
            dtTable.Columns.Add("Who")
            dtTable.Columns.Add("Where")



            If clearDGVCol = True Then

                clearDGVCol = False
            End If


            Dim SRRecord = client.Get("newAnnouncementDB/")


            Dim myJsonTool As New JavaScriptSerializer
            Dim myDeserializedItems = myJsonTool.Deserialize(Of Dictionary(Of String, AnnouncementDATA))(SRRecord.Body)


            For Each dictItem As KeyValuePair(Of String, AnnouncementDATA) In myDeserializedItems
                dtTable.Rows.Add(dictItem.Value.hTheme, dictItem.Value.hWhat, dictItem.Value.hWhy, dictItem.Value.hWhen, dictItem.Value.hWho, dictItem.Value.hWhere)
            Next

            dtTableGrd = dtTable



        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ElseIf ex.Message = "Object reference not set to an instance of an object." Then
                Dim dtTable As New DataTable
                dtTable.Columns.Add("Theme")
                dtTable.Columns.Add("What")
                dtTable.Columns.Add("Why")
                dtTable.Columns.Add("When")
                dtTable.Columns.Add("Who")
                dtTable.Columns.Add("Where")
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If



        End Try
    End Sub

    Private Sub LISTevents_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            client = New FireSharp.FirebaseClient(fcon)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        ShowRecord()

        Dim clearDGVCol As Boolean = True

        RTB_Click(sender, e)

    End Sub

    Private Sub RichTextBox1_TextChanged(sender As Object, e As EventArgs) Handles List.TextChanged

    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click

    End Sub

    Private Sub RTB_Click(sender As Object, e As EventArgs) Handles RTB.Click
        Dim pres As FirebaseResponse = client.Get("newAnnouncementDB")
        Dim dataAnnounce As Dictionary(Of String, AnnouncementDATA) = JsonConvert.DeserializeObject(Of Dictionary(Of String, AnnouncementDATA))(pres.Body.ToString())
        UpdateRTB(dataAnnounce)
    End Sub

    Sub UpdateRTB(ByVal records As Dictionary(Of String, AnnouncementDATA))
        List.Clear()

        For Each item In records
            List.Text += "---------------------------------------------------------------------------------------------" & vbCrLf + "  THEME :  " + item.Value.hTheme.ToString() + vbLf + vbLf
            List.Text += "  WHAT    :  " + item.Value.hWhat.ToString() + vbCrLf
            List.Text += "  WHY      :  " + item.Value.hWhy.ToString() + vbCrLf
            List.Text += "  WHEN    :  " + item.Value.hWhen.ToString() + vbCrLf
            List.Text += "  WHO      :  " + item.Value.hWho.ToString() + vbCrLf
            List.Text += "  WHERE :  " + item.Value.hWhere.ToString() + vbCrLf

        Next
    End Sub


End Class