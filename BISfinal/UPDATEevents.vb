﻿Imports FireSharp.Config
Imports FireSharp.Response
Imports FireSharp.Interfaces
'-------------------------------------------

Imports System.Web.Script.Serialization '(Importing System.Web.Script.Serialization) This is used to read and convert JSON strings.
Imports System.ComponentModel '(Importing System.ComponentModel) This is used to sort data on DataGridView.
Imports System.IO

Public Class UPDATEevents

    Public Property UPDATEEVENTtheme As String
    Public Property UPDATEEVENTwhat As String
    Public Property UPDATEEVENTwhy As String
    Public Property UPDATEEVENTwhen As String
    Public Property UPDATEEVENTwho As String
    Public Property UPDATEEVENTwhere As String


    Dim clearDGVCol As Boolean = True

    Private dtTableGrd As DataTable


    Private fcon As New FirebaseConfig() With
        {
        .AuthSecret = "8kPwBHDkDdZWGHts9sb98dsJu45thBvgDTUrgqJj",
        .BasePath = "https://bisrealtime-default-rtdb.firebaseio.com/"
        }

    Private client As IFirebaseClient



    Sub ShowRecord()
        Try
            Dim dtTable As New DataTable
            dtTable.Columns.Add("Posted")
            dtTable.Columns.Add("Theme")
            dtTable.Columns.Add("What")
            dtTable.Columns.Add("Why")
            dtTable.Columns.Add("When")
            dtTable.Columns.Add("Who")
            dtTable.Columns.Add("Where")



            If clearDGVCol = True Then

                clearDGVCol = False
            End If


            Dim SRRecord = client.Get("ResidentDB/")


            Dim myJsonTool As New JavaScriptSerializer
            Dim myDeserializedItems = myJsonTool.Deserialize(Of Dictionary(Of String, AnnouncementDATA))(SRRecord.Body)


            For Each dictItem As KeyValuePair(Of String, AnnouncementDATA) In myDeserializedItems
                dtTable.Rows.Add(dictItem.Value.hPosted, dictItem.Value.hTheme, dictItem.Value.hWhat, dictItem.Value.hWhy, dictItem.Value.hWhen, dictItem.Value.hWho, dictItem.Value.hWhere)
            Next


            dtTableGrd = dtTable



        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ElseIf ex.Message = "Object reference not set to an instance of an object." Then
                Dim dtTable As New DataTable
                dtTable.Columns.Add("Posted")
                dtTable.Columns.Add("Theme")
                dtTable.Columns.Add("What")
                dtTable.Columns.Add("Why")
                dtTable.Columns.Add("When")
                dtTable.Columns.Add("Who")
                dtTable.Columns.Add("Where")
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If



        End Try
    End Sub
    Private Sub UPDATEevents_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            client = New FireSharp.FirebaseClient(fcon)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Timer1.Enabled = True

        tbTHEME.Text = UPDATEEVENTtheme
        tbWHAT.Text = UPDATEEVENTwhat
        tbWHY.Text = UPDATEEVENTwhy
        tbWHEN.Text = UPDATEEVENTwhen
        tbWHO.Text = UPDATEEVENTwho
        tbWHERE.Text = UPDATEEVENTwhere
    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Try
            Dim QA As New AnnouncementDATA() With
                {
                .hPosted = Ldate.Text,
                .hTheme = tbTHEME.Text,
                .hWhat = tbWHAT.Text,
                .hWhy = tbWHY.Text,
                .hWhen = tbWHEN.Text,
                .hWho = tbWHO.Text,
                .hWhere = tbWHERE.Text
                }

            Dim update = client.Update("newAnnouncementDB/" + tbTHEME.Text, QA)

            MessageBox.Show("Updated successfully.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Ldate.Text = ""
            tbTHEME.Text = ""
            tbWHAT.Text = ""
            tbWHY.Text = ""
            tbWHEN.Text = ""
            tbWHO.Text = ""
            tbWHERE.Text = ""
            Me.Close()

        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If


        End Try

    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Ldate.Text = Date.Now.ToString("MMM dd yyyy")
    End Sub
End Class