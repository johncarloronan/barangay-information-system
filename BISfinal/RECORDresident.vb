﻿Imports FireSharp.Config
Imports FireSharp.Response
Imports FireSharp.Interfaces

Imports Newtonsoft.Json

Imports System.Web.Script.Serialization '(Importing System.Web.Script.Serialization) This is used to read and convert JSON strings.
Imports System.ComponentModel '(Importing System.ComponentModel) This is used to sort data on DataGridView.
Imports System.IO

Public Class RECORDresident



    Dim IMG_FileNameInput As String
    Dim clearDGVCol As Boolean = True

    Private dtTableGrd As DataTable 'This variable is used to search data on DataGridView.

    '-------------------------------------------Configure FireSharp
    Private fcon As New FirebaseConfig() With
        {
        .AuthSecret = "8kPwBHDkDdZWGHts9sb98dsJu45thBvgDTUrgqJj",
        .BasePath = "https://bisrealtime-default-rtdb.firebaseio.com/"
        }

    Private client As IFirebaseClient

    Public Function ImageToBase64(image As Image) As String
        Using ms As New MemoryStream()
            ' Convert Image to byte[]  
            Dim Format As System.Drawing.Imaging.ImageFormat = Imaging.ImageFormat.Jpeg
            image.Save(ms, Format)
            Dim imageBytes As Byte() = ms.ToArray()

            ' Convert byte[] to Base64 String  
            Dim base64String As String = Convert.ToBase64String(imageBytes)
            Return base64String
        End Using
    End Function

    Public Function Base64ToImage(base64String As String) As Image
        ' Convert Base64 String to byte[]  
        Dim imageBytes As Byte() = Convert.FromBase64String(base64String)
        Dim ms As New MemoryStream(imageBytes, 0, imageBytes.Length)

        ' Convert byte[] to Image  
        ms.Write(imageBytes, 0, imageBytes.Length)
        Dim image__1 As Image = System.Drawing.Image.FromStream(ms, True)
        Return image__1
    End Function





    Sub ShowRecord()
        Try
            Dim dtTable As New DataTable
            dtTable.Columns.Add("Fullname")
            dtTable.Columns.Add("ID")
            dtTable.Columns.Add("Lastname")
            dtTable.Columns.Add("First Name")
            dtTable.Columns.Add("Middle Name")
            dtTable.Columns.Add("Suffix")
            dtTable.Columns.Add("Birth Date")
            dtTable.Columns.Add("Age")
            dtTable.Columns.Add("Gender")
            dtTable.Columns.Add("Address")
            dtTable.Columns.Add("Birth Place")
            dtTable.Columns.Add("Occupation")
            dtTable.Columns.Add("Voter")
            dtTable.Columns.Add("Province")
            dtTable.Columns.Add("Contact No.")
            dtTable.Columns.Add("Status")
            dtTable.Columns.Add("Blood Type")
            dtTable.Columns.Add("Citizenship")
            dtTable.Columns.Add("Religion")
            dtTable.Columns.Add("Image", GetType(Image))


            If clearDGVCol = True Then
                DGVUserData.Columns.Clear()
                clearDGVCol = False
            End If
            '-------------------------------------------

            Dim SRRecord = client.Get("ResidentDB/")

            Dim myJsonTool As New JavaScriptSerializer
            Dim myDeserializedItems = myJsonTool.Deserialize(Of Dictionary(Of String, ResidentData))(SRRecord.Body)

            For Each dictItem As KeyValuePair(Of String, ResidentData) In myDeserializedItems
                dtTable.Rows.Add(dictItem.Value.bFullname, dictItem.Value.bID, dictItem.Value.bLname, dictItem.Value.bFname, dictItem.Value.bMname, dictItem.Value.bSuffix, dictItem.Value.bBdate, dictItem.Value.bAge, dictItem.Value.bGender, dictItem.Value.bAddress, dictItem.Value.bBplace, dictItem.Value.bOccupation, dictItem.Value.bVoter, dictItem.Value.bProvince, dictItem.Value.bContact, dictItem.Value.bStatus, dictItem.Value.bBtype, dictItem.Value.bCitizenship, dictItem.Value.bReligion, Base64ToImage(dictItem.Value.Image))
            Next
            '-------------------------------------------

            DGVUserData.DataSource = dtTable
            dtTableGrd = dtTable

            Dim imageColumn = DirectCast(DGVUserData.Columns("Image"), DataGridViewImageColumn)
            imageColumn.ImageLayout = DataGridViewImageCellLayout.Zoom
            '-------------------------------------------
            DGVUserData.Sort(DGVUserData.Columns(2), ListSortDirection.Ascending)

            lblTotal.Text = "" & (DGVUserData.RowCount)



            DGVUserData.ClearSelection()
        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ElseIf ex.Message = "Object reference not set to an instance of an object." Then
                Dim dtTable As New DataTable
                dtTable.Columns.Add("Fullname")
                dtTable.Columns.Add("ID")
                dtTable.Columns.Add("Lastname")
                dtTable.Columns.Add("First Name")
                dtTable.Columns.Add("Middle Name")
                dtTable.Columns.Add("Suffix")
                dtTable.Columns.Add("Birth Date")
                dtTable.Columns.Add("Age")
                dtTable.Columns.Add("Gender")
                dtTable.Columns.Add("Address")
                dtTable.Columns.Add("Birth Place")
                dtTable.Columns.Add("Occupation")
                dtTable.Columns.Add("Voter")
                dtTable.Columns.Add("Province")
                dtTable.Columns.Add("Contact No.")
                dtTable.Columns.Add("Status")
                dtTable.Columns.Add("Blood Type")
                dtTable.Columns.Add("Citizenship")
                dtTable.Columns.Add("Religion")
                dtTable.Columns.Add("Image", GetType(Image))
                DGVUserData.DataSource = dtTable
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If



        End Try


    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        newRes.Show()
        updateResident.Close()


    End Sub

    Private Sub resView_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            client = New FireSharp.FirebaseClient(fcon)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Dim clearDGVCol As Boolean = True
        btnRefresh_Click(sender, e)






    End Sub



    Private Sub DGVUserData_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVUserData.CellContentClick

    End Sub

    Private Sub DGVUserData_Click(sender As Object, e As EventArgs) Handles DGVUserData.Click


    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        updateResident.Show()
        newRes.Close()


    End Sub

    Private Sub tbS_TextChanged(sender As Object, e As EventArgs) Handles tbS.TextChanged
        tbS.ForeColor = Color.Black

        dtTableGrd.DefaultView.RowFilter = CBsb.Text & " Like '" & tbS.Text & "%'" 'To search for data on the DataGridView
        DGVUserData.ClearSelection()

        tbS.Visible = True
        PictureBox1.Visible = True


    End Sub

    Private Sub CBsb_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CBsb.SelectedIndexChanged
        tbS.Text = " Search"
        tbS.ForeColor = Color.Gray

    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub tbS_CursorChanged(sender As Object, e As EventArgs) Handles tbS.CursorChanged

    End Sub

    Private Sub tbS_Click(sender As Object, e As EventArgs) Handles tbS.Click
        tbS.Text = ""
    End Sub



    Private Sub haha_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub List_TextChanged(sender As Object, e As EventArgs)




    End Sub


    Private Sub resVw_MouseMove(sender As Object, e As MouseEventArgs) Handles Me.MouseMove



    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        ShowRecord()

    End Sub

    Private Sub resVw_MouseWheel(sender As Object, e As MouseEventArgs) Handles Me.MouseWheel

    End Sub

    Private Sub DGVUserData_RowsAdded(sender As Object, e As DataGridViewRowsAddedEventArgs) Handles DGVUserData.RowsAdded

    End Sub

    Private Sub btnList_Click(sender As Object, e As EventArgs) Handles btnList.Click
        resList.Show()

    End Sub

    Private Sub DGVUserData_MouseMove(sender As Object, e As MouseEventArgs) Handles DGVUserData.MouseMove

    End Sub

    Private Sub DGVUserData_DataSourceChanged(sender As Object, e As EventArgs) Handles DGVUserData.DataSourceChanged


    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs)

    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If AllCellsSelected(DGVUserData) = True Then
            Try
                If MsgBox("Are you sure you want to delete all data?", MsgBoxStyle.Question + MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Cancel Then Return

                Dim deleteAll = client.Delete("ResidentDB") 'To delete data in the Firebase Database

                MessageBox.Show("Data successfully deleted.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)




                btnRefresh_Click(sender, e)
                Return
            Catch ex As Exception
                If ex.Message = "One or more errors occurred." Then
                    MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Else
                    MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If



            End Try
        End If

        Try
            If DGVUserData.SelectedRows.Count = 0 Then
                MessageBox.Show("Please select a row to be deleted.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If

            If MsgBox("Are you sure you want to delete this data ?", MsgBoxStyle.Question + MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Cancel Then Return




            For Each row As DataGridViewRow In DGVUserData.SelectedRows
                If row.Selected = True Then
                    Dim delete = client.Delete("ResidentDB/" & row.DataBoundItem(1).ToString)
                End If
            Next

            MessageBox.Show("Data successfully deleted.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)




            btnRefresh_Click(sender, e)
        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If



        End Try

    End Sub

    Private Function AllCellsSelected(dgv As DataGridView) As Boolean
        If dgv.RowCount = 0 Then
            AllCellsSelected = False
            Return AllCellsSelected
        End If
        AllCellsSelected = (dgv.SelectedCells.Count = (dgv.RowCount * dgv.Columns.GetColumnCount(DataGridViewElementStates.Visible)))
        If dgv.RowCount = 1 Then
            AllCellsSelected = False
        End If
    End Function


    Private Sub btnSend_Click(sender As Object, e As EventArgs) Handles btnSend.Click
        Dim aa As New updateResident

        aa.one = TBFullname.Text
        aa.two = tbID.Text
        aa.three = TBLname.Text
        aa.four = TBFname.Text
        aa.five = TBMname.Text
        aa.six = TBSuffix.Text
        aa.seven = dtBday.Text
        aa.eight = TBAge.Text
        aa.nine = CBGender.Text
        aa.ten = TBAddress.Text
        aa.eleven = TBBplace.Text
        aa.twelve = TBProvince.Text
        aa.thirteen = TBContact.Text
        aa.fourteen = CBStatus.Text
        aa.fifteen = CBBtype.Text
        aa.sixteen = TBCitizenship.Text
        aa.seventeen = TBReligion.Text
        aa.eighteen = PictureBoxUserReg.Image
        aa.nineteen = TBoccupation.Text
        aa.twenty = CBvoter.Text

        aa.Show()
    End Sub

    Private Sub DGVUserData_DoubleClick(sender As Object, e As EventArgs) Handles DGVUserData.DoubleClick
        TBFullname.Text = DGVUserData.SelectedRows(0).Cells("Fullname").Value
        tbID.Text = DGVUserData.SelectedRows(0).Cells("ID").Value
        TBLname.Text = DGVUserData.SelectedRows(0).Cells("Lastname").Value
        TBFname.Text = DGVUserData.SelectedRows(0).Cells("First Name").Value
        TBMname.Text = DGVUserData.SelectedRows(0).Cells("Middle Name").Value
        TBSuffix.Text = DGVUserData.SelectedRows(0).Cells("Suffix").Value
        dtBday.Text = DGVUserData.SelectedRows(0).Cells("Birth Date").Value
        TBAge.Text = DGVUserData.SelectedRows(0).Cells("Age").Value
        CBGender.Text = DGVUserData.SelectedRows(0).Cells("Gender").Value
        TBAddress.Text = DGVUserData.SelectedRows(0).Cells("Address").Value
        TBBplace.Text = DGVUserData.SelectedRows(0).Cells("Birth Place").Value
        TBoccupation.Text = DGVUserData.SelectedRows(0).Cells("Occupation").Value
        CBvoter.Text = DGVUserData.SelectedRows(0).Cells("Voter").Value
        TBProvince.Text = DGVUserData.SelectedRows(0).Cells("Province").Value
        TBContact.Text = DGVUserData.SelectedRows(0).Cells("Contact No.").Value
        CBStatus.Text = DGVUserData.SelectedRows(0).Cells("Status").Value
        CBBtype.Text = DGVUserData.SelectedRows(0).Cells("Blood Type").Value
        TBCitizenship.Text = DGVUserData.SelectedRows(0).Cells("Citizenship").Value
        TBReligion.Text = DGVUserData.SelectedRows(0).Cells("Religion").Value
        PictureBoxUserReg.Image = DGVUserData.Rows(DGVUserData.SelectedRows(0).Index).Cells("Image").Value


        btnSend_Click(sender, e)
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub lblTotal_Click(sender As Object, e As EventArgs) Handles lblTotal.Click

    End Sub

    Private Sub PictureBox1_Click_1(sender As Object, e As EventArgs) Handles PictureBox1.Click

    End Sub

    Private Sub PictureBox5_Click(sender As Object, e As EventArgs) Handles PictureBox5.Click

    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        resList.Show()

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs)

    End Sub
End Class