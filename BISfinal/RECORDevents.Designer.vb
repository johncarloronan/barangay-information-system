﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RECORDevents
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.DGVuserDATA = New System.Windows.Forms.DataGridView()
        Me.pWHERE = New System.Windows.Forms.TextBox()
        Me.pWHO = New System.Windows.Forms.TextBox()
        Me.pWHEN = New System.Windows.Forms.TextBox()
        Me.pWHY = New System.Windows.Forms.TextBox()
        Me.pWHAT = New System.Windows.Forms.TextBox()
        Me.pTHEME = New System.Windows.Forms.TextBox()
        Me.passer = New System.Windows.Forms.Button()
        Me.lbltotal = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.btnrefresh = New System.Windows.Forms.Button()
        CType(Me.DGVuserDATA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DGVuserDATA
        '
        Me.DGVuserDATA.AllowUserToAddRows = False
        Me.DGVuserDATA.AllowUserToDeleteRows = False
        Me.DGVuserDATA.AllowUserToResizeColumns = False
        Me.DGVuserDATA.AllowUserToResizeRows = False
        Me.DGVuserDATA.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DGVuserDATA.BackgroundColor = System.Drawing.Color.White
        Me.DGVuserDATA.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DGVuserDATA.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.DGVuserDATA.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.Teal
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.SteelBlue
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVuserDATA.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.DGVuserDATA.ColumnHeadersHeight = 40
        Me.DGVuserDATA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.DGVuserDATA.Cursor = System.Windows.Forms.Cursors.Hand
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.MediumAquamarine
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGVuserDATA.DefaultCellStyle = DataGridViewCellStyle11
        Me.DGVuserDATA.EnableHeadersVisualStyles = False
        Me.DGVuserDATA.GridColor = System.Drawing.Color.DimGray
        Me.DGVuserDATA.Location = New System.Drawing.Point(0, 46)
        Me.DGVuserDATA.MultiSelect = False
        Me.DGVuserDATA.Name = "DGVuserDATA"
        Me.DGVuserDATA.ReadOnly = True
        Me.DGVuserDATA.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(CType(CType(52, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(52, Byte), Integer))
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVuserDATA.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.DGVuserDATA.RowHeadersVisible = False
        Me.DGVuserDATA.RowHeadersWidth = 50
        Me.DGVuserDATA.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGVuserDATA.Size = New System.Drawing.Size(861, 609)
        Me.DGVuserDATA.TabIndex = 2
        '
        'pWHERE
        '
        Me.pWHERE.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pWHERE.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pWHERE.Location = New System.Drawing.Point(319, 335)
        Me.pWHERE.Multiline = True
        Me.pWHERE.Name = "pWHERE"
        Me.pWHERE.Size = New System.Drawing.Size(160, 34)
        Me.pWHERE.TabIndex = 31
        Me.pWHERE.Visible = False
        '
        'pWHO
        '
        Me.pWHO.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pWHO.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pWHO.Location = New System.Drawing.Point(319, 295)
        Me.pWHO.Multiline = True
        Me.pWHO.Name = "pWHO"
        Me.pWHO.Size = New System.Drawing.Size(160, 34)
        Me.pWHO.TabIndex = 30
        Me.pWHO.Visible = False
        '
        'pWHEN
        '
        Me.pWHEN.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pWHEN.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pWHEN.Location = New System.Drawing.Point(319, 268)
        Me.pWHEN.Multiline = True
        Me.pWHEN.Name = "pWHEN"
        Me.pWHEN.Size = New System.Drawing.Size(160, 21)
        Me.pWHEN.TabIndex = 29
        Me.pWHEN.Visible = False
        '
        'pWHY
        '
        Me.pWHY.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pWHY.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pWHY.Location = New System.Drawing.Point(319, 228)
        Me.pWHY.Multiline = True
        Me.pWHY.Name = "pWHY"
        Me.pWHY.Size = New System.Drawing.Size(160, 34)
        Me.pWHY.TabIndex = 28
        Me.pWHY.Visible = False
        '
        'pWHAT
        '
        Me.pWHAT.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pWHAT.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pWHAT.Location = New System.Drawing.Point(319, 188)
        Me.pWHAT.Multiline = True
        Me.pWHAT.Name = "pWHAT"
        Me.pWHAT.Size = New System.Drawing.Size(160, 34)
        Me.pWHAT.TabIndex = 27
        Me.pWHAT.Visible = False
        '
        'pTHEME
        '
        Me.pTHEME.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pTHEME.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pTHEME.Location = New System.Drawing.Point(319, 148)
        Me.pTHEME.Multiline = True
        Me.pTHEME.Name = "pTHEME"
        Me.pTHEME.Size = New System.Drawing.Size(160, 34)
        Me.pTHEME.TabIndex = 26
        Me.pTHEME.Visible = False
        '
        'passer
        '
        Me.passer.Location = New System.Drawing.Point(319, 375)
        Me.passer.Name = "passer"
        Me.passer.Size = New System.Drawing.Size(160, 23)
        Me.passer.TabIndex = 32
        Me.passer.Text = "Button1"
        Me.passer.UseVisualStyleBackColor = True
        Me.passer.Visible = False
        '
        'lbltotal
        '
        Me.lbltotal.AutoSize = True
        Me.lbltotal.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltotal.Location = New System.Drawing.Point(63, 12)
        Me.lbltotal.Name = "lbltotal"
        Me.lbltotal.Size = New System.Drawing.Size(21, 24)
        Me.lbltotal.TabIndex = 34
        Me.lbltotal.Text = "0"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = Global.BISfinal.My.Resources.Resources.asdasdasdad
        Me.PictureBox1.Location = New System.Drawing.Point(15, 7)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(44, 34)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 35
        Me.PictureBox1.TabStop = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Transparent
        Me.Button1.BackgroundImage = Global.BISfinal.My.Resources.Resources.trash
        Me.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(788, 6)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(30, 33)
        Me.Button1.TabIndex = 33
        Me.Button1.UseVisualStyleBackColor = False
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.Transparent
        Me.btnAdd.BackgroundImage = Global.BISfinal.My.Resources.Resources._30_512_7281
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.White
        Me.btnAdd.Location = New System.Drawing.Point(752, 7)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(30, 33)
        Me.btnAdd.TabIndex = 1
        Me.btnAdd.UseVisualStyleBackColor = False
        '
        'btnrefresh
        '
        Me.btnrefresh.BackgroundImage = Global.BISfinal.My.Resources.Resources.refresh__1_
        Me.btnrefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnrefresh.FlatAppearance.BorderSize = 0
        Me.btnrefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnrefresh.Location = New System.Drawing.Point(825, 7)
        Me.btnrefresh.Name = "btnrefresh"
        Me.btnrefresh.Size = New System.Drawing.Size(30, 33)
        Me.btnrefresh.TabIndex = 36
        Me.btnrefresh.UseVisualStyleBackColor = True
        '
        'RECORDevents
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(861, 655)
        Me.Controls.Add(Me.btnrefresh)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.lbltotal)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.passer)
        Me.Controls.Add(Me.pWHERE)
        Me.Controls.Add(Me.pWHO)
        Me.Controls.Add(Me.pWHEN)
        Me.Controls.Add(Me.pWHY)
        Me.Controls.Add(Me.pWHAT)
        Me.Controls.Add(Me.pTHEME)
        Me.Controls.Add(Me.DGVuserDATA)
        Me.Controls.Add(Me.btnAdd)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "RECORDevents"
        Me.Text = "RECORDannouncement"
        CType(Me.DGVuserDATA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnAdd As Button
    Friend WithEvents DGVuserDATA As DataGridView
    Friend WithEvents pWHERE As TextBox
    Friend WithEvents pWHO As TextBox
    Friend WithEvents pWHEN As TextBox
    Friend WithEvents pWHY As TextBox
    Friend WithEvents pWHAT As TextBox
    Friend WithEvents pTHEME As TextBox
    Friend WithEvents passer As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents lbltotal As Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents btnrefresh As Button
End Class
