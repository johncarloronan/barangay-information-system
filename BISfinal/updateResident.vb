﻿Imports FireSharp.Config
Imports FireSharp.Response
Imports FireSharp.Interfaces
'-------------------------------------------

Imports System.Web.Script.Serialization '(Importing System.Web.Script.Serialization) This is used to read and convert JSON strings.
Imports System.ComponentModel '(Importing System.ComponentModel) This is used to sort data on DataGridView.
Imports System.IO

Public Class updateResident

    Public Property one As String
    Public Property two As String
    Public Property three As String
    Public Property four As String
    Public Property five As String
    Public Property six As String
    Public Property seven As String
    Public Property eight As String
    Public Property nine As String
    Public Property ten As String
    Public Property eleven As String
    Public Property twelve As String
    Public Property thirteen As String
    Public Property fourteen As String
    Public Property fifteen As String
    Public Property sixteen As String
    Public Property seventeen As String
    Public Property eighteen As Image
    Public Property nineteen As String
    Public Property twenty As String


    Dim IMG_FileNameInput As String
    Dim clearDGVCol As Boolean = True

    Private dtTableGrd As DataTable


    Private fcon As New FirebaseConfig() With
        {
        .AuthSecret = "8kPwBHDkDdZWGHts9sb98dsJu45thBvgDTUrgqJj",
        .BasePath = "https://bisrealtime-default-rtdb.firebaseio.com/"
        }

    Private client As IFirebaseClient

    Public Function ImageToBase64(image As Image) As String
        Using ms As New MemoryStream()
            ' Convert Image to byte[]  
            Dim Format As System.Drawing.Imaging.ImageFormat = Imaging.ImageFormat.Jpeg
            image.Save(ms, Format)
            Dim imageBytes As Byte() = ms.ToArray()

            ' Convert byte[] to Base64 String  
            Dim base64String As String = Convert.ToBase64String(imageBytes)
            Return base64String
        End Using
    End Function

    Public Function Base64ToImage(base64String As String) As Image
        ' Convert Base64 String to byte[]  
        Dim imageBytes As Byte() = Convert.FromBase64String(base64String)
        Dim ms As New MemoryStream(imageBytes, 0, imageBytes.Length)

        ' Convert byte[] to Image  
        ms.Write(imageBytes, 0, imageBytes.Length)
        Dim image__1 As Image = System.Drawing.Image.FromStream(ms, True)
        Return image__1
    End Function


    Sub ShowRecord()
        Try
            Dim dtTable As New DataTable
            dtTable.Columns.Add("Fullname")
            dtTable.Columns.Add("ID")
            dtTable.Columns.Add("Lastname")
            dtTable.Columns.Add("First Name")
            dtTable.Columns.Add("Middle Name")
            dtTable.Columns.Add("Suffix")
            dtTable.Columns.Add("Birth Date")
            dtTable.Columns.Add("Age")
            dtTable.Columns.Add("Gender")
            dtTable.Columns.Add("Address")
            dtTable.Columns.Add("Birth Place")
            dtTable.Columns.Add("Occupation")
            dtTable.Columns.Add("Voter")
            dtTable.Columns.Add("Province")
            dtTable.Columns.Add("Contact No.")
            dtTable.Columns.Add("Status")
            dtTable.Columns.Add("Blood Type")
            dtTable.Columns.Add("Citizenship")
            dtTable.Columns.Add("Religion")
            dtTable.Columns.Add("Image", GetType(Image))


            If clearDGVCol = True Then
                DGVUserData.Columns.Clear()
                clearDGVCol = False
            End If


            Dim SRRecord = client.Get("ResidentDB/")


            Dim myJsonTool As New JavaScriptSerializer
            Dim myDeserializedItems = myJsonTool.Deserialize(Of Dictionary(Of String, ResidentData))(SRRecord.Body)


            For Each dictItem As KeyValuePair(Of String, ResidentData) In myDeserializedItems
                dtTable.Rows.Add(dictItem.Value.bFullname, dictItem.Value.bID, dictItem.Value.bLname, dictItem.Value.bFname, dictItem.Value.bMname, dictItem.Value.bSuffix, dictItem.Value.bBdate, dictItem.Value.bAge, dictItem.Value.bGender, dictItem.Value.bAddress, dictItem.Value.bBplace, dictItem.Value.bOccupation, dictItem.Value.bVoter, dictItem.Value.bProvince, dictItem.Value.bContact, dictItem.Value.bStatus, dictItem.Value.bBtype, dictItem.Value.bCitizenship, dictItem.Value.bReligion, Base64ToImage(dictItem.Value.Image))
            Next



            DGVUserData.DataSource = dtTable
            dtTableGrd = dtTable

            Dim imageColumn = DirectCast(DGVUserData.Columns("Image"), DataGridViewImageColumn)
            imageColumn.ImageLayout = DataGridViewImageCellLayout.Zoom

            DGVUserData.Sort(DGVUserData.Columns(0), ListSortDirection.Descending)





            DGVUserData.ClearSelection()


        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ElseIf ex.Message = "Object reference not set to an instance of an object." Then
                Dim dtTable As New DataTable
                dtTable.Columns.Add("Fullname")
                dtTable.Columns.Add("ID")
                dtTable.Columns.Add("Lastname")
                dtTable.Columns.Add("First Name")
                dtTable.Columns.Add("Middle Name")
                dtTable.Columns.Add("Suffix")
                dtTable.Columns.Add("Birth Date")
                dtTable.Columns.Add("Age")
                dtTable.Columns.Add("Gender")
                dtTable.Columns.Add("Address")
                dtTable.Columns.Add("Birth Place")
                dtTable.Columns.Add("Occupation")
                dtTable.Columns.Add("Voter")
                dtTable.Columns.Add("Province")
                dtTable.Columns.Add("Contact No.")
                dtTable.Columns.Add("Status")
                dtTable.Columns.Add("Blood Type")
                dtTable.Columns.Add("Citizenship")
                dtTable.Columns.Add("Religion")
                dtTable.Columns.Add("Image", GetType(Image))
                DGVUserData.DataSource = dtTable

                MessageBox.Show("Database not found or Database is empty.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If


        End Try
    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        If TBFullname.Text = Nothing Then
            MessageBox.Show("FULL NAME field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TBLname.Text = Nothing Then
            MessageBox.Show("LAST NAME field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TBFname.Text = Nothing Then
            MessageBox.Show("FIRST NAME field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TBMname.Text = Nothing Then
            MessageBox.Show("MIDDLE NAME field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If



        If dtBday.Text = Nothing Then
            MessageBox.Show("BIRTH DATE field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TBAge.Text = Nothing Then
            MessageBox.Show("AGE field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If CBGender.Text = Nothing Then
            MessageBox.Show("GENDER field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TBAddress.Text = Nothing Then
            MessageBox.Show("ADDRESS field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TBBplace.Text = Nothing Then
            MessageBox.Show("BIRTH PLACE field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TBProvince.Text = Nothing Then
            MessageBox.Show("PROVINCE field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TBContact.Text = Nothing Then
            MessageBox.Show("CONTACT NO. field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TBContact.Text = Nothing Then
            MessageBox.Show("STATUS field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TBCitizenship.Text = Nothing Then
            MessageBox.Show("CITIZENSHIP field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TBReligion.Text = Nothing Then
            MessageBox.Show("RELIGION field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If


        Try

            Dim ImgData As String = ImageToBase64(PictureBoxUserReg.Image)

            Dim RD As New ResidentData() With
                {
                .bFullname = TBFullname.Text,
                .bID = TBid.Text,
                .bLname = TBLname.Text,
                .bFname = TBFname.Text,
                .bMname = TBMname.Text,
                .bSuffix = TBSuffix.Text,
                .bBdate = dtBday.Text,
                .bAge = TBAge.Text,
                .bGender = CBGender.Text,
                .bAddress = TBAddress.Text,
                .bBplace = TBBplace.Text,
                .bOccupation = TBoccupation.Text,
                .bVoter = CBvoter.Text,
                .bProvince = TBProvince.Text,
                .bContact = TBContact.Text,
                .bStatus = CBStatus.Text,
                .bBtype = CBBtype.Text,
                .bCitizenship = TBCitizenship.Text,
                .bReligion = TBReligion.Text,
                .Image = ImgData
                }

            Dim update = client.Update("ResidentDB/" + tbID.Text, RD) 'To update data to Firebase Database.

            MessageBox.Show("Data updated successfully.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            tbSearch.Clear()
            TBFullname.Clear()
            tbID.Text = ""
            TBLname.Clear()
            TBFname.Clear()
            TBMname.Clear()
            TBSuffix.Clear()
            TBAge.Clear()
            CBGender.Text = ""
            TBAddress.Clear()
            TBBplace.Clear()
            TBoccupation.Clear()
            CBvoter.Text = ""
            TBProvince.Clear()
            TBContact.Clear()
            CBStatus.Text = ""
            CBBtype.Text = ""
            TBCitizenship.Text = ""
            TBReligion.Text = ""
            Me.Close()






        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If


        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs)
        Me.Close()

    End Sub

    Private Sub updateResident_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Try
            client = New FireSharp.FirebaseClient(fcon)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        ShowRecord()

        Dim clearDGVCol As Boolean = True




        TBFullname.Text = one
        tbID.Text = two
        TBLname.Text = three
        TBFname.Text = four
        TBMname.Text = five
        TBSuffix.Text = six
        dtBday.Text = seven
        TBAge.Text = eight
        CBGender.Text = nine
        TBAddress.Text = ten
        TBBplace.Text = eleven
        TBProvince.Text = twelve
        TBContact.Text = thirteen
        CBStatus.Text = fourteen
        CBBtype.Text = fifteen
        TBCitizenship.Text = sixteen
        TBReligion.Text = seventeen
        PictureBoxUserReg.Image = eighteen
        TBoccupation.Text = nineteen
        CBvoter.Text = twenty














    End Sub

    Private Sub CBsb_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CBsb.SelectedIndexChanged
        CBsb.Text = "Lastname"
        tbSearch.Text = " Search Surname"
        tbSearch.ForeColor = Color.Gray
    End Sub

    Private Sub tbSearch_TextChanged(sender As Object, e As EventArgs) Handles tbSearch.TextChanged
        tbSearch.ForeColor = Color.White
    End Sub

    Private Sub tbSearch_Click(sender As Object, e As EventArgs) Handles tbSearch.Click
        tbSearch.Text = ""


    End Sub

    Private Sub updateResident_Click(sender As Object, e As EventArgs) Handles Me.Click
        DGVUserData.Visible = False
        PictureBox3.Visible = False


    End Sub

    Private Sub PictureBox3_Click(sender As Object, e As EventArgs) Handles PictureBox3.Click
        DGVUserData.Visible = False
        PictureBox3.Visible = False

    End Sub

    Private Sub DGVUserData_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVUserData.CellContentClick


    End Sub

    Private Sub DGVUserData_Click(sender As Object, e As EventArgs) Handles DGVUserData.Click
        DGVUserData.Visible = False
        PictureBox3.Visible = False

        TBFullname.Text = DGVUserData.SelectedRows(0).Cells("Fullname").Value
        tbID.Text = DGVUserData.SelectedRows(0).Cells("ID").Value
        TBLname.Text = DGVUserData.SelectedRows(0).Cells("Lastname").Value
        TBFname.Text = DGVUserData.SelectedRows(0).Cells("First Name").Value
        TBMname.Text = DGVUserData.SelectedRows(0).Cells("Middle Name").Value
        TBSuffix.Text = DGVUserData.SelectedRows(0).Cells("Suffix").Value
        dtBday.Text = DGVUserData.SelectedRows(0).Cells("Birth Date").Value
        TBAge.Text = DGVUserData.SelectedRows(0).Cells("Age").Value
        CBGender.Text = DGVUserData.SelectedRows(0).Cells("Gender").Value
        TBAddress.Text = DGVUserData.SelectedRows(0).Cells("Address").Value
        TBBplace.Text = DGVUserData.SelectedRows(0).Cells("Birth Place").Value
        TBoccupation.Text = DGVUserData.SelectedRows(0).Cells("Occupation").Value
        CBvoter.Text = DGVUserData.SelectedRows(0).Cells("Voter").Value
        TBProvince.Text = DGVUserData.SelectedRows(0).Cells("Province").Value
        TBContact.Text = DGVUserData.SelectedRows(0).Cells("Contact No.").Value
        CBStatus.Text = DGVUserData.SelectedRows(0).Cells("Status").Value
        CBBtype.Text = DGVUserData.SelectedRows(0).Cells("Blood Type").Value
        TBCitizenship.Text = DGVUserData.SelectedRows(0).Cells("Citizenship").Value
        TBReligion.Text = DGVUserData.SelectedRows(0).Cells("Religion").Value
        PictureBoxUserReg.Image = DGVUserData.Rows(DGVUserData.SelectedRows(0).Index).Cells("Image").Value






    End Sub

    Private Sub btnS_Click(sender As Object, e As EventArgs) Handles btnS.Click
        dtTableGrd.DefaultView.RowFilter = CBsb.Text & " Like '" & tbSearch.Text & "%'" 'To search for data on the DataGridView
        DGVUserData.ClearSelection()

        DGVUserData.Visible = True
        PictureBox3.Visible = True




    End Sub

    Private Sub PictureBoxUserReg_Click(sender As Object, e As EventArgs) Handles PictureBoxUserReg.Click
        OpenFileDialogUserImage.FileName = ""
        OpenFileDialogUserImage.Filter = "JPEG (*.jpeg;*.jpg)|*.jpeg;*.jpg"

        If (OpenFileDialogUserImage.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
            IMG_FileNameInput = OpenFileDialogUserImage.FileName
            PictureBoxUserReg.ImageLocation = IMG_FileNameInput


        End If
    End Sub
End Class