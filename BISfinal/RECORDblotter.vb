﻿Imports FireSharp.Config
Imports FireSharp.Response
Imports FireSharp.Interfaces
'-------------------------------------------

Imports System.Web.Script.Serialization '(Importing System.Web.Script.Serialization) This is used to read and convert JSON strings.
Imports System.ComponentModel '(Importing System.ComponentModel) This is used to sort data on DataGridView.
Imports System.IO

Public Class RECORDblotter

    Dim clearDGVCol As Boolean = True

    Private dtTableGrd As DataTable 'This variable is used to search data on DataGridView.

    '-------------------------------------------Configure FireSharp
    Private fcon As New FirebaseConfig() With
        {
        .AuthSecret = "8kPwBHDkDdZWGHts9sb98dsJu45thBvgDTUrgqJj",
        .BasePath = "https://bisrealtime-default-rtdb.firebaseio.com/"
        }

    Private client As IFirebaseClient

    Public Function ImageToBase64(image As Image) As String
        Using ms As New MemoryStream()
            ' Convert Image to byte[]  
            Dim Format As System.Drawing.Imaging.ImageFormat = Imaging.ImageFormat.Jpeg
            image.Save(ms, Format)
            Dim imageBytes As Byte() = ms.ToArray()

            ' Convert byte[] to Base64 String  
            Dim base64String As String = Convert.ToBase64String(imageBytes)
            Return base64String
        End Using
    End Function

    Public Function Base64ToImage(base64String As String) As Image
        ' Convert Base64 String to byte[]  
        Dim imageBytes As Byte() = Convert.FromBase64String(base64String)
        Dim ms As New MemoryStream(imageBytes, 0, imageBytes.Length)

        ' Convert byte[] to Image  
        ms.Write(imageBytes, 0, imageBytes.Length)
        Dim image__1 As Image = System.Drawing.Image.FromStream(ms, True)
        Return image__1
    End Function

    Sub DisplayRecLoad(Stat As Boolean)
        DGVUserData.Enabled = Stat


    End Sub



    Sub ShowRecord()
        Try
            Dim dtTable As New DataTable
            dtTable.Columns.Add("Case no.")
            dtTable.Columns.Add("Date")
            dtTable.Columns.Add("Time")
            dtTable.Columns.Add("Complainant")
            dtTable.Columns.Add("Address")
            dtTable.Columns.Add("Incident")
            dtTable.Columns.Add("Details")
            dtTable.Columns.Add("Action")
            dtTable.Columns.Add("Remarks")
            dtTable.Columns.Add("Encoder")



            If clearDGVCol = True Then
                DGVUserData.Columns.Clear()
                clearDGVCol = False
            End If


            Dim SRRecord = client.Get("BlotterDB/")


            Dim myJsonTool As New JavaScriptSerializer
            Dim myDeserializedItems = myJsonTool.Deserialize(Of Dictionary(Of String, ComplainData))(SRRecord.Body)


            For Each dictItem As KeyValuePair(Of String, ComplainData) In myDeserializedItems
                dtTable.Rows.Add(dictItem.Value.eCasenumber, dictItem.Value.eDate, dictItem.Value.eTime, dictItem.Value.eComplainant, dictItem.Value.eAddress, dictItem.Value.eIncident, dictItem.Value.eDetails, dictItem.Value.eAction, dictItem.Value.eRemarks, dictItem.Value.eEncoder)
            Next

            DGVUserData.DataSource = dtTable
            dtTableGrd = dtTable

            DGVUserData.Sort(DGVUserData.Columns(1), ListSortDirection.Descending)

            lbltotal.Text = "" & (DGVUserData.RowCount)

            DisplayRecLoad(True)

            DGVUserData.ClearSelection()


        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ElseIf ex.Message = "Object reference not set to an instance of an object." Then
                Dim dtTable As New DataTable
                dtTable.Columns.Add("Case no.")
                dtTable.Columns.Add("Date")
                dtTable.Columns.Add("Time")
                dtTable.Columns.Add("Complainant")
                dtTable.Columns.Add("Address")
                dtTable.Columns.Add("Incident")
                dtTable.Columns.Add("Details")
                dtTable.Columns.Add("Action")
                dtTable.Columns.Add("Remarks")
                dtTable.Columns.Add("Encoder")
                DGVUserData.DataSource = dtTable
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

            DisplayRecLoad(True)

        End Try

    End Sub

    Private Sub blotterRecords_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            client = New FireSharp.FirebaseClient(fcon)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Dim clearDGVCol As Boolean = True
        ShowRecord()

    End Sub

    Private Sub DGVUserData_CellContentClick(sender As Object, e As DataGridViewCellEventArgs)

    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If AllCellsSelected(DGVUserData) = True Then
            Try
                If MsgBox("Are you sure you want to delete all data?", MsgBoxStyle.Question + MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Cancel Then Return

                Dim deleteAll = client.Delete("BlotterDB") 'To delete data in the Firebase Database

                MessageBox.Show("Data successfully deleted.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)




                btnRefresh_Click(sender, e)
                Return
            Catch ex As Exception
                If ex.Message = "One or more errors occurred." Then
                    MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Else
                    MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If



            End Try
        End If

        Try
            If DGVUserData.SelectedRows.Count = 0 Then
                MessageBox.Show("Please select a row to be deleted.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If

            If MsgBox("Are you sure you want to delete this data ?", MsgBoxStyle.Question + MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Cancel Then Return




            For Each row As DataGridViewRow In DGVUserData.SelectedRows
                If row.Selected = True Then
                    Dim delete = client.Delete("BlotterDB/" & row.DataBoundItem(0).ToString)
                End If
            Next

            MessageBox.Show("Data successfully deleted.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)




            btnRefresh_Click(sender, e)
        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If



        End Try

    End Sub

    Private Function AllCellsSelected(dgv As DataGridView) As Boolean
        If dgv.RowCount = 0 Then
            AllCellsSelected = False
            Return AllCellsSelected
        End If
        AllCellsSelected = (dgv.SelectedCells.Count = (dgv.RowCount * dgv.Columns.GetColumnCount(DataGridViewElementStates.Visible)))
        If dgv.RowCount = 1 Then
            AllCellsSelected = False
        End If
    End Function


    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        ShowRecord()

    End Sub

    Private Sub tbS_TextChanged(sender As Object, e As EventArgs) Handles tbS.TextChanged
        tbS.ForeColor = Color.Black

        dtTableGrd.DefaultView.RowFilter = CBsb.Text & " Like '" & tbS.Text & "%'" 'To search for data on the DataGridView
        DGVUserData.ClearSelection()

        tbS.Visible = True
        PictureBox1.Visible = True
    End Sub

    Private Sub CBsb_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CBsb.SelectedIndexChanged
        tbS.Text = " Search"
        tbS.ForeColor = Color.Gray
    End Sub

    Private Sub DGVUserData_CellContentClick_1(sender As Object, e As DataGridViewCellEventArgs) Handles DGVUserData.CellContentClick

    End Sub

    Private Sub DGVUserData_DoubleClick(sender As Object, e As EventArgs) Handles DGVUserData.DoubleClick
        Tnum.Text = DGVUserData.SelectedRows(0).Cells("Case no.").Value
        Tdate.Text = DGVUserData.SelectedRows(0).Cells("Date").Value
        Ttime.Text = DGVUserData.SelectedRows(0).Cells("Time").Value
        Tcomplainant.Text = DGVUserData.SelectedRows(0).Cells("Complainant").Value
        Taddress.Text = DGVUserData.SelectedRows(0).Cells("Address").Value
        Tincident.Text = DGVUserData.SelectedRows(0).Cells("Incident").Value
        Tdetails.Text = DGVUserData.SelectedRows(0).Cells("Details").Value
        Tactiontaken.Text = DGVUserData.SelectedRows(0).Cells("Action").Value
        Cremarks.Text = DGVUserData.SelectedRows(0).Cells("Remarks").Value
        PassRecord_Click(sender, e)
    End Sub


    Private Sub PassRecord_Click(sender As Object, e As EventArgs) Handles PassRecord.Click
        Dim ccUpdateComplain As New UPDATEcomplain


        ccUpdateComplain.UPDATEBLOTTERnum = Tnum.Text
        ccUpdateComplain.UPDATEBLOTTERdate = Tdate.Text
        ccUpdateComplain.UPDATEBLOTTERtime = Ttime.Text
        ccUpdateComplain.UPDATEBLOTTERcomplainant = Tcomplainant.Text
        ccUpdateComplain.UPDATEBLOTTERaddress = Taddress.Text
        ccUpdateComplain.UPDATEBLOTTERincident = Tincident.Text
        ccUpdateComplain.UPDATEBLOTTERdetails = Tdetails.Text
        ccUpdateComplain.UPDATEBLOTTERaction = Tactiontaken.Text
        ccUpdateComplain.UPDATEBLOTTERremarks = Cremarks.Text
        ccUpdateComplain.Show()



    End Sub

    Private Sub DGVUserData_Click(sender As Object, e As EventArgs) Handles DGVUserData.Click
    End Sub

    Private Sub tbS_Click(sender As Object, e As EventArgs) Handles tbS.Click
        tbS.Text = ""
    End Sub
End Class