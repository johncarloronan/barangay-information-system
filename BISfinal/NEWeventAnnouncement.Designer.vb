﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class NEWeventAnnouncement
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.tbTHEME = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.tbWHAT = New System.Windows.Forms.TextBox()
        Me.tbWHY = New System.Windows.Forms.TextBox()
        Me.tbWHEN = New System.Windows.Forms.TextBox()
        Me.tbWHO = New System.Windows.Forms.TextBox()
        Me.btnAnnounce = New System.Windows.Forms.Button()
        Me.tbWHERE = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Ldate = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbTHEME
        '
        Me.tbTHEME.BackColor = System.Drawing.Color.WhiteSmoke
        Me.tbTHEME.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbTHEME.Location = New System.Drawing.Point(120, 109)
        Me.tbTHEME.Multiline = True
        Me.tbTHEME.Name = "tbTHEME"
        Me.tbTHEME.Size = New System.Drawing.Size(292, 57)
        Me.tbTHEME.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(15, 109)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(91, 22)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "THEME :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(28, 172)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 22)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "WHAT :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(41, 235)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(65, 22)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "WHY :"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(26, 298)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(80, 22)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "WHEN :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(38, 348)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(68, 22)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "WHO :"
        '
        'tbWHAT
        '
        Me.tbWHAT.BackColor = System.Drawing.Color.WhiteSmoke
        Me.tbWHAT.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbWHAT.Location = New System.Drawing.Point(120, 172)
        Me.tbWHAT.Multiline = True
        Me.tbWHAT.Name = "tbWHAT"
        Me.tbWHAT.Size = New System.Drawing.Size(292, 57)
        Me.tbWHAT.TabIndex = 7
        '
        'tbWHY
        '
        Me.tbWHY.BackColor = System.Drawing.Color.WhiteSmoke
        Me.tbWHY.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbWHY.Location = New System.Drawing.Point(120, 235)
        Me.tbWHY.Multiline = True
        Me.tbWHY.Name = "tbWHY"
        Me.tbWHY.Size = New System.Drawing.Size(292, 57)
        Me.tbWHY.TabIndex = 8
        '
        'tbWHEN
        '
        Me.tbWHEN.BackColor = System.Drawing.Color.WhiteSmoke
        Me.tbWHEN.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbWHEN.Location = New System.Drawing.Point(120, 298)
        Me.tbWHEN.Multiline = True
        Me.tbWHEN.Name = "tbWHEN"
        Me.tbWHEN.Size = New System.Drawing.Size(292, 44)
        Me.tbWHEN.TabIndex = 9
        '
        'tbWHO
        '
        Me.tbWHO.BackColor = System.Drawing.Color.WhiteSmoke
        Me.tbWHO.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbWHO.Location = New System.Drawing.Point(120, 348)
        Me.tbWHO.Multiline = True
        Me.tbWHO.Name = "tbWHO"
        Me.tbWHO.Size = New System.Drawing.Size(292, 57)
        Me.tbWHO.TabIndex = 10
        '
        'btnAnnounce
        '
        Me.btnAnnounce.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnAnnounce.FlatAppearance.BorderSize = 0
        Me.btnAnnounce.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAnnounce.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAnnounce.ForeColor = System.Drawing.Color.White
        Me.btnAnnounce.Location = New System.Drawing.Point(120, 474)
        Me.btnAnnounce.Name = "btnAnnounce"
        Me.btnAnnounce.Size = New System.Drawing.Size(292, 38)
        Me.btnAnnounce.TabIndex = 11
        Me.btnAnnounce.Text = "ANNOUNCE"
        Me.btnAnnounce.UseVisualStyleBackColor = False
        '
        'tbWHERE
        '
        Me.tbWHERE.BackColor = System.Drawing.Color.WhiteSmoke
        Me.tbWHERE.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbWHERE.Location = New System.Drawing.Point(120, 411)
        Me.tbWHERE.Multiline = True
        Me.tbWHERE.Name = "tbWHERE"
        Me.tbWHERE.Size = New System.Drawing.Size(292, 57)
        Me.tbWHERE.TabIndex = 12
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(13, 411)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(93, 22)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "WHERE :"
        '
        'Timer1
        '
        '
        'Ldate
        '
        Me.Ldate.AutoSize = True
        Me.Ldate.Location = New System.Drawing.Point(22, 474)
        Me.Ldate.Name = "Ldate"
        Me.Ldate.Size = New System.Drawing.Size(39, 13)
        Me.Ldate.TabIndex = 14
        Me.Ldate.Text = "Label7"
        Me.Ldate.Visible = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Navy
        Me.Label11.Location = New System.Drawing.Point(125, 52)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(248, 22)
        Me.Label11.TabIndex = 32
        Me.Label11.Text = "EVENT ANNOUNCEMENT"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Navy
        Me.Label10.Location = New System.Drawing.Point(125, 30)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(89, 22)
        Me.Label10.TabIndex = 31
        Me.Label10.Text = "CREATE"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = Global.BISfinal.My.Resources.Resources.imgbin_computer_icons_time_youtube_png
        Me.PictureBox1.Location = New System.Drawing.Point(12, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(125, 81)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 30
        Me.PictureBox1.TabStop = False
        '
        'NEWeventAnnouncement
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(423, 524)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Ldate)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.tbWHERE)
        Me.Controls.Add(Me.btnAnnounce)
        Me.Controls.Add(Me.tbWHO)
        Me.Controls.Add(Me.tbWHEN)
        Me.Controls.Add(Me.tbWHY)
        Me.Controls.Add(Me.tbWHAT)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tbTHEME)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "NEWeventAnnouncement"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "NEW ANNOUNCEMENT"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents tbTHEME As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents tbWHAT As TextBox
    Friend WithEvents tbWHY As TextBox
    Friend WithEvents tbWHEN As TextBox
    Friend WithEvents tbWHO As TextBox
    Friend WithEvents btnAnnounce As Button
    Friend WithEvents tbWHERE As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Timer1 As Timer
    Friend WithEvents Ldate As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents PictureBox1 As PictureBox
End Class
