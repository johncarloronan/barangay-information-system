﻿Imports FireSharp.Config
Imports FireSharp.Response
Imports FireSharp.Interfaces
'-------------------------------------------

Imports System.Web.Script.Serialization '(Importing System.Web.Script.Serialization) This is used to read and convert JSON strings.
Imports System.ComponentModel '(Importing System.ComponentModel) This is used to sort data on DataGridView.
Imports System.IO

Imports AForge
Imports AForge.Video
Imports AForge.Video.DirectShow

Public Class newRes

    Dim CAMERA As VideoCaptureDevice
    Dim bmp As Bitmap


    Dim IMG_FileNameInput As String
    Dim clearDGVCol As Boolean = True

    Private dtTableGrd As DataTable


    Private fcon As New FirebaseConfig() With
        {
        .AuthSecret = "8kPwBHDkDdZWGHts9sb98dsJu45thBvgDTUrgqJj",
        .BasePath = "https://bisrealtime-default-rtdb.firebaseio.com/"
        }

    Private client As IFirebaseClient

    Public Function ImageToBase64(image As Image) As String
        Using ms As New MemoryStream()
            ' Convert Image to byte[]  
            Dim Format As System.Drawing.Imaging.ImageFormat = Imaging.ImageFormat.Jpeg
            image.Save(ms, Format)
            Dim imageBytes As Byte() = ms.ToArray()

            ' Convert byte[] to Base64 String  
            Dim base64String As String = Convert.ToBase64String(imageBytes)
            Return base64String
        End Using
    End Function

    Public Function Base64ToImage(base64String As String) As Image
        ' Convert Base64 String to byte[]  
        Dim imageBytes As Byte() = Convert.FromBase64String(base64String)
        Dim ms As New MemoryStream(imageBytes, 0, imageBytes.Length)

        ' Convert byte[] to Image  
        ms.Write(imageBytes, 0, imageBytes.Length)
        Dim image__1 As Image = System.Drawing.Image.FromStream(ms, True)
        Return image__1
    End Function



    Sub ShowRecord()
        Try
            Dim dtTable As New DataTable
            dtTable.Columns.Add("Fullname")
            dtTable.Columns.Add("ID")
            dtTable.Columns.Add("Lastname")
            dtTable.Columns.Add("First Name")
            dtTable.Columns.Add("Middle Name")
            dtTable.Columns.Add("Suffix")
            dtTable.Columns.Add("Birth Date")
            dtTable.Columns.Add("Age")
            dtTable.Columns.Add("Gender")
            dtTable.Columns.Add("Address")
            dtTable.Columns.Add("Birth Place")
            dtTable.Columns.Add("Occupation")
            dtTable.Columns.Add("Voter")
            dtTable.Columns.Add("Province")
            dtTable.Columns.Add("Contact No.")
            dtTable.Columns.Add("Status")
            dtTable.Columns.Add("Blood Type")
            dtTable.Columns.Add("Citizenship")
            dtTable.Columns.Add("Religion")
            dtTable.Columns.Add("Image", GetType(Image))


            If clearDGVCol = True Then

                clearDGVCol = False
            End If


            Dim SRRecord = client.Get("ResidentDB/")


            Dim myJsonTool As New JavaScriptSerializer
            Dim myDeserializedItems = myJsonTool.Deserialize(Of Dictionary(Of String, ResidentData))(SRRecord.Body)


            For Each dictItem As KeyValuePair(Of String, ResidentData) In myDeserializedItems
                dtTable.Rows.Add(dictItem.Value.bFullname, dictItem.Value.bID, dictItem.Value.bLname, dictItem.Value.bFname, dictItem.Value.bMname, dictItem.Value.bSuffix, dictItem.Value.bBdate, dictItem.Value.bAge, dictItem.Value.bGender, dictItem.Value.bAddress, dictItem.Value.bBplace, dictItem.Value.bOccupation, dictItem.Value.bVoter, dictItem.Value.bProvince, dictItem.Value.bContact, dictItem.Value.bStatus, dictItem.Value.bBtype, dictItem.Value.bCitizenship, dictItem.Value.bReligion, dtTable.Columns.Add("Image", GetType(Image)))
            Next


            dtTableGrd = dtTable



        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ElseIf ex.Message = "Object reference not set to an instance of an object." Then
                Dim dtTable As New DataTable
                dtTable.Columns.Add("Fullname")
                dtTable.Columns.Add("ID")
                dtTable.Columns.Add("Lastname")
                dtTable.Columns.Add("First Name")
                dtTable.Columns.Add("Middle Name")
                dtTable.Columns.Add("Suffix")
                dtTable.Columns.Add("Birth Date")
                dtTable.Columns.Add("Age")
                dtTable.Columns.Add("Gender")
                dtTable.Columns.Add("Address")
                dtTable.Columns.Add("Birth Place")
                dtTable.Columns.Add("Occupation")
                dtTable.Columns.Add("Voter")
                dtTable.Columns.Add("Province")
                dtTable.Columns.Add("Contact No.")
                dtTable.Columns.Add("Status")
                dtTable.Columns.Add("Blood Type")
                dtTable.Columns.Add("Citizenship")
                dtTable.Columns.Add("Religion")
                dtTable.Columns.Add("Image", GetType(Image))



                MessageBox.Show("Database not found or Database is empty.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If



        End Try
    End Sub

    Private Sub Label13_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs)
        Me.Close()


    End Sub

    Private Sub newRes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            client = New FireSharp.FirebaseClient(fcon)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Try
            TBid.Text = "Please wait..."

            Dim r As Random = New Random
            Dim num As Integer
            num = (r.Next(1, 999999999))
            Dim IDresults As String = Strings.Right("00000000" & num.ToString(), 8)

            Dim Check_ID = client.Get("ResidentDB/" + IDresults)

            '-------------------------------------------Conditions to check whether the ID has been used.
            If Check_ID.Body <> "null" Then
                MessageBox.Show("The same ID is found, create another ID by pressing the Create ID button.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                TBid.Text = IDresults
            End If
            '-------------------------------------------
        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                TBid.Text = ""
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                TBid.Text = ""
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End Try
    End Sub

    Private Sub BtnSave_Click(sender As Object, e As EventArgs) Handles BtnSave.Click
        If TBFullname.Text = Nothing Then
            MessageBox.Show("FULL NAME field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TBLname.Text = Nothing Then
            MessageBox.Show("LAST NAME field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TBFname.Text = Nothing Then
            MessageBox.Show("FIRST NAME field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TBMname.Text = Nothing Then
            MessageBox.Show("MIDDLE NAME field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If


        If dtBday.Text = Nothing Then
            MessageBox.Show("BIRTH DATE field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TBAge.Text = Nothing Then
            MessageBox.Show("AGE field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If CBGender.Text = Nothing Then
            MessageBox.Show("GENDER field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TBAddress.Text = Nothing Then
            MessageBox.Show("ADDRESS field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TBBplace.Text = Nothing Then
            MessageBox.Show("BIRTH PLACE field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If CBvoter.Text = Nothing Then
            MessageBox.Show("VOTER field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TBProvince.Text = Nothing Then
            MessageBox.Show("PROVINCE field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TBContact.Text = Nothing Then
            MessageBox.Show("CONTACT NO. field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If CBStatus.Text = Nothing Then
            MessageBox.Show("STATUS field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TBCitizenship.Text = Nothing Then
            MessageBox.Show("CITIZENSHIP field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TBReligion.Text = Nothing Then
            MessageBox.Show("RELIGION field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If



        Try


            Dim ImgData As String = ImageToBase64(PictureBox1.Image)

            Dim PD As New ResidentData() With
                {
            .bFullname = TBFullname.Text,
            .bID = TBid.Text,
            .bLname = TBLname.Text,
            .bFname = TBFname.Text,
            .bMname = TBMname.Text,
            .bSuffix = TBSuffix.Text,
            .bBdate = dtBday.Text,
            .bAge = TBAge.Text,
            .bGender = CBGender.Text,
            .bAddress = TBAddress.Text,
            .bBplace = TBBplace.Text,
            .bOccupation = TBoccupation.Text,
            .bVoter = CBvoter.Text,
            .bProvince = TBProvince.Text,
            .bContact = TBContact.Text,
            .bStatus = CBStatus.Text,
            .bBtype = CBBtype.Text,
            .bCitizenship = TBCitizenship.Text,
            .bReligion = TBReligion.Text,
            .Image = ImgData
                }
            Dim save = client.Set("ResidentDB/" + TBid.Text, PD)
            MsgBox("Registered Successfully")
            TBFullname.Text = ""
            TBLname.Text = ""
            TBFname.Text = ""
            TBMname.Text = ""
            TBSuffix.Text = ""
            dtBday.Text = ""
            TBAge.Text = ""
            CBGender.Text = ""
            TBAddress.Text = ""
            TBBplace.Text = ""
            TBoccupation.Text = ""
            CBvoter.Text = ""
            TBProvince.Text = ""
            TBContact.Text = ""
            CBStatus.Text = ""
            CBBtype.Text = ""
            TBCitizenship.Text = ""
            TBReligion.Text = ""




            Me.Close()




        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Label17_Click(sender As Object, e As EventArgs) Handles TBid.Click

    End Sub

    Private Sub PictureBoxUserReg_Click(sender As Object, e As EventArgs) Handles PictureBoxUserReg.Click
        Dim cameras As VideoCaptureDeviceForm = New VideoCaptureDeviceForm
        If cameras.ShowDialog = DialogResult.OK Then
            CAMERA = cameras.VideoDevice
            AddHandler CAMERA.NewFrame, New NewFrameEventHandler(AddressOf Captured)
            CAMERA.Start()


            Button2.Visible = True

        End If

    End Sub
    Private Sub Captured(sender As Object, eventArgs As NewFrameEventArgs)
        bmp = DirectCast(eventArgs.Frame.Clone(), Bitmap)
        PictureBoxUserReg.Image = DirectCast(eventArgs.Frame.Clone(), Bitmap)

    End Sub
    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        OpenFileDialogUserImage.FileName = ""
        OpenFileDialogUserImage.Filter = "JPEG (*.jpeg;*.jpg)|*.jpeg;*.jpg;*png"

        If (OpenFileDialogUserImage.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
            IMG_FileNameInput = OpenFileDialogUserImage.FileName
            PictureBox1.ImageLocation = IMG_FileNameInput

        End If
    End Sub

    Private Sub Button2_Click_1(sender As Object, e As EventArgs) Handles Button2.Click
        PictureBox1.Image = PictureBoxUserReg.Image
        CAMERA.Stop()
        Button2.Visible = False

    End Sub

    Private Sub Label13_Click_1(sender As Object, e As EventArgs)

    End Sub
End Class