﻿Imports FireSharp.Config
Imports FireSharp.Response
Imports FireSharp.Interfaces
'-------------------------------------------

Imports System.Web.Script.Serialization '(Importing System.Web.Script.Serialization) This is used to read and convert JSON strings.
Imports System.ComponentModel '(Importing System.ComponentModel) This is used to sort data on DataGridView.
Imports System.IO '(Importing System.IO) This is used for ImageToBase64 Public Functions and Base64ToImage Public Functions.

Public Class RECORDlog

    Dim clearDGVCol As Boolean = True

    Private dtTableGrd As DataTable 'This variable is used to search data on DataGridView.

    '-------------------------------------------Configure FireSharp
    Private fcon As New FirebaseConfig() With
        {
        .AuthSecret = "8kPwBHDkDdZWGHts9sb98dsJu45thBvgDTUrgqJj",
        .BasePath = "https://bisrealtime-default-rtdb.firebaseio.com/"
        }

    Private client As IFirebaseClient



    Sub DisplayRecLoad(Stat As Boolean)
        BtnRefresh.Enabled = Stat
        DGVUserData.Enabled = Stat
        TBSearch.Enabled = Stat
        CBSearchBy.Enabled = Stat

    End Sub


    '-------------------------------------------

    '-------------------------------------------Sub to load data from database and display in DataGridView.
    Sub ShowRecord()
        Try
            Dim dtTable As New DataTable
            dtTable.Columns.Add("ID")
            dtTable.Columns.Add("Date")
            dtTable.Columns.Add("Time")
            dtTable.Columns.Add("Name")
            dtTable.Columns.Add("Purpose")

            '-------------------------------------------Conditions for deleting columns. This was executed only once.
            If clearDGVCol = True Then
                DGVUserData.Columns.Clear()
                clearDGVCol = False
            End If
            '-------------------------------------------

            Dim SRRecord = client.Get("LogDB/")

            Dim myJsonTool As New JavaScriptSerializer
            Dim myDeserializedItems = myJsonTool.Deserialize(Of Dictionary(Of String, LogData))(SRRecord.Body)
            '-------------------------------------------

            '-------------------------------------------To enter a Database (in JSON file format that has been previously converted into an object form) into a Table.
            For Each dictItem As KeyValuePair(Of String, LogData) In myDeserializedItems
                dtTable.Rows.Add(dictItem.Value.aID, dictItem.Value.aDate, dictItem.Value.aTime, dictItem.Value.aName, dictItem.Value.aPurpose)
            Next
            '-------------------------------------------

            DGVUserData.DataSource = dtTable 'Gets or sets the data source that the DataGridView is displaying data for.
            dtTableGrd = dtTable 'Entering data from dtTable into dtTableGrd, dtTableGrd is used to find data and display it on DataGridView.



            DGVUserData.Sort(DGVUserData.Columns(1), ListSortDirection.Descending)



            LblTotalUser.Text = "" & (DGVUserData.RowCount)


            DisplayRecLoad(True)

            DGVUserData.ClearSelection()
        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ElseIf ex.Message = "Object reference not set to an instance of an object." Then
                Dim dtTable As New DataTable
                dtTable.Columns.Add("ID")
                dtTable.Columns.Add("Date")
                dtTable.Columns.Add("Time")
                dtTable.Columns.Add("Name")
                dtTable.Columns.Add("Purpose")
                DGVUserData.DataSource = dtTable
                MessageBox.Show("Database not found or Database is empty.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

            Me.Text = "VB Net Firebase RealTime Database"

            DisplayRecLoad(True)
        End Try


    End Sub

    Private Sub adminInterface_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            client = New FireSharp.FirebaseClient(fcon)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        ShowRecord()

        Dim clearDGVCol As Boolean = True

        CBSearchBy.Text = "Search by:"
        CBSearchBy.ForeColor = Color.Gray

        Timer1.Interval = 30000
        Timer1.Enabled = True

    End Sub

    Private Sub BtnRefresh_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub TBSearch_TextChanged(sender As Object, e As EventArgs) Handles TBSearch.TextChanged
        dtTableGrd.DefaultView.RowFilter = CBSearchBy.Text & " Like '" & TBSearch.Text & "%'" 'To search for data on the DataGridView
        DGVUserData.ClearSelection()

        TBSearch.ForeColor = Color.Black

    End Sub

    Private Sub CBSearchBy_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CBSearchBy.SelectedIndexChanged
        If CBSearchBy.Text = "Date" Then
            CBSearchBy.ForeColor = Color.Black
            TBSearch.Text = "Enter mm-dd-yyyy"
            TBSearch.ForeColor = Color.Gray

        ElseIf CBSearchBy.Text = "Name" Then
            CBSearchBy.ForeColor = Color.Black
            TBSearch.Text = "Enter Name"
            TBSearch.ForeColor = Color.Gray

        ElseIf CBSearchBy.Text = "Purpose" Then
            CBSearchBy.ForeColor = Color.Black
            TBSearch.Text = "Enter Purpose"
            TBSearch.ForeColor = Color.Gray

        End If

        TBSearch.Visible = True
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub TextBox1_Click(sender As Object, e As EventArgs)


    End Sub

    Private Sub TBSearch_Click(sender As Object, e As EventArgs) Handles TBSearch.Click
        TBSearch.Text = ""
    End Sub

    Private Sub BtnRefresh_Click_1(sender As Object, e As EventArgs) Handles BtnRefresh.Click
        ShowRecord()

    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        BtnRefresh_Click_1(sender, e)
    End Sub

    Private Sub Timer2_Tick(sender As Object, e As EventArgs)
    End Sub
End Class