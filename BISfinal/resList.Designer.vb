﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class resList
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.RTB = New System.Windows.Forms.Button()
        Me.List = New System.Windows.Forms.RichTextBox()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'RTB
        '
        Me.RTB.Location = New System.Drawing.Point(224, 342)
        Me.RTB.Name = "RTB"
        Me.RTB.Size = New System.Drawing.Size(39, 23)
        Me.RTB.TabIndex = 76
        Me.RTB.Text = "RTB"
        Me.RTB.UseVisualStyleBackColor = True
        Me.RTB.Visible = False
        '
        'List
        '
        Me.List.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.List.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.List.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.List.ForeColor = System.Drawing.Color.White
        Me.List.Location = New System.Drawing.Point(0, 0)
        Me.List.Name = "List"
        Me.List.ReadOnly = True
        Me.List.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical
        Me.List.Size = New System.Drawing.Size(405, 541)
        Me.List.TabIndex = 75
        Me.List.Text = ""
        '
        'btnRefresh
        '
        Me.btnRefresh.Location = New System.Drawing.Point(222, 313)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(41, 23)
        Me.btnRefresh.TabIndex = 77
        Me.btnRefresh.Text = "R"
        Me.btnRefresh.UseVisualStyleBackColor = True
        Me.btnRefresh.Visible = False
        '
        'resList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(369, 540)
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.RTB)
        Me.Controls.Add(Me.List)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "resList"
        Me.Text = "Residents List"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents RTB As Button
    Friend WithEvents List As RichTextBox
    Friend WithEvents btnRefresh As Button
End Class
