﻿Imports FireSharp.Config
Imports FireSharp.Response
Imports FireSharp.Interfaces

Imports Newtonsoft.Json

Imports System.Web.Script.Serialization '(Importing System.Web.Script.Serialization) This is used to read and convert JSON strings.
Imports System.ComponentModel '(Importing System.ComponentModel) This is used to sort data on DataGridView.
Imports System.IO

Public Class RECORDevents

    Dim bmp As Bitmap

    Dim clearDGVCol As Boolean = True

    Private dtTableGrd As DataTable


    Private fcon As New FirebaseConfig() With
        {
        .AuthSecret = "8kPwBHDkDdZWGHts9sb98dsJu45thBvgDTUrgqJj",
        .BasePath = "https://bisrealtime-default-rtdb.firebaseio.com/"
        }

    Private client As IFirebaseClient



    Sub ShowRecord()
        Try
            Dim dtTable As New DataTable
            dtTable.Columns.Add("Theme")
            dtTable.Columns.Add("What")
            dtTable.Columns.Add("Why")
            dtTable.Columns.Add("When")
            dtTable.Columns.Add("Who")
            dtTable.Columns.Add("Where")
            dtTable.Columns.Add("Posted")



            If clearDGVCol = True Then

                clearDGVCol = False
            End If


            Dim SRRecord = client.Get("newAnnouncementDB/")


            Dim myJsonTool As New JavaScriptSerializer
            Dim myDeserializedItems = myJsonTool.Deserialize(Of Dictionary(Of String, AnnouncementDATA))(SRRecord.Body)


            For Each dictItem As KeyValuePair(Of String, AnnouncementDATA) In myDeserializedItems
                dtTable.Rows.Add(dictItem.Value.hTheme, dictItem.Value.hWhat, dictItem.Value.hWhy, dictItem.Value.hWhen, dictItem.Value.hWho, dictItem.Value.hWhere, dictItem.Value.hPosted)
            Next



            DGVuserDATA.DataSource = dtTable
            dtTableGrd = dtTable
            '-------------------------------------------
            DGVuserDATA.Sort(DGVuserDATA.Columns(0), ListSortDirection.Descending)

            lbltotal.Text = "" & (DGVuserDATA.RowCount)

            DGVuserDATA.ClearSelection()

        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ElseIf ex.Message = "Object reference not set to an instance of an object." Then
                Dim dtTable As New DataTable
                dtTable.Columns.Add("Theme")
                dtTable.Columns.Add("What")
                dtTable.Columns.Add("Why")
                dtTable.Columns.Add("When")
                dtTable.Columns.Add("Who")
                dtTable.Columns.Add("Where")
                dtTable.Columns.Add("Posted")
                DGVuserDATA.DataSource = dtTable
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If



        End Try
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        NEWeventAnnouncement.Show()


    End Sub

    Private Sub RECORDevents_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            client = New FireSharp.FirebaseClient(fcon)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Dim clearDGVCol As Boolean = True

        ShowRecord()
    End Sub

    Private Sub passer_Click(sender As Object, e As EventArgs) Handles passer.Click
        Dim zzUpdateEvents As New UPDATEevents


        zzUpdateEvents.UPDATEEVENTtheme = pTHEME.Text
        zzUpdateEvents.UPDATEEVENTwhat = pWHAT.Text
        zzUpdateEvents.UPDATEEVENTwhy = pWHY.Text
        zzUpdateEvents.UPDATEEVENTwhen = pWHEN.Text
        zzUpdateEvents.UPDATEEVENTwho = pWHO.Text
        zzUpdateEvents.UPDATEEVENTwhere = pWHERE.Text
        zzUpdateEvents.Show()



    End Sub

    Private Sub DGVUserData_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVuserDATA.CellContentClick

    End Sub

    Private Sub DGVUserData_Click(sender As Object, e As EventArgs) Handles DGVuserDATA.Click

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If AllCellsSelected(DGVuserDATA) = True Then
            Try
                If MsgBox("Are you sure you want to delete all data?", MsgBoxStyle.Question + MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Cancel Then Return

                Dim deleteAll = client.Delete("newAnnouncementDB") 'To delete data in the Firebase Database

                MessageBox.Show("Data successfully deleted.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)




                btnRefresh_Click(sender, e)
                Return
            Catch ex As Exception
                If ex.Message = "One or more errors occurred." Then
                    MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Else
                    MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If



            End Try
        End If

        Try
            If DGVuserDATA.SelectedRows.Count = 0 Then
                MessageBox.Show("Please select a row to be deleted.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If

            If MsgBox("Are you sure you want to delete this data ?", MsgBoxStyle.Question + MsgBoxStyle.OkCancel, "Confirmation") = MsgBoxResult.Cancel Then Return




            For Each row As DataGridViewRow In DGVuserDATA.SelectedRows
                If row.Selected = True Then
                    Dim delete = client.Delete("newAnnouncementDB/" & row.DataBoundItem(0).ToString)
                End If
            Next

            MessageBox.Show("Data successfully deleted.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)




            btnRefresh_Click(sender, e)
        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If



        End Try

    End Sub

    Private Function AllCellsSelected(dgv As DataGridView) As Boolean
        If dgv.RowCount = 0 Then
            AllCellsSelected = False
            Return AllCellsSelected
        End If
        AllCellsSelected = (dgv.SelectedCells.Count = (dgv.RowCount * dgv.Columns.GetColumnCount(DataGridViewElementStates.Visible)))
        If dgv.RowCount = 1 Then
            AllCellsSelected = False
        End If
    End Function

    Private Sub btnrefresh_Click(sender As Object, e As EventArgs) Handles btnrefresh.Click
        ShowRecord()

    End Sub

    Private Sub DGVuserDATA_DoubleClick(sender As Object, e As EventArgs) Handles DGVuserDATA.DoubleClick
        pTHEME.Text = DGVuserDATA.SelectedRows(0).Cells("Theme").Value
        pWHAT.Text = DGVuserDATA.SelectedRows(0).Cells("What").Value
        pWHY.Text = DGVuserDATA.SelectedRows(0).Cells("Why").Value
        pWHEN.Text = DGVuserDATA.SelectedRows(0).Cells("When").Value
        pWHO.Text = DGVuserDATA.SelectedRows(0).Cells("Who").Value
        pWHERE.Text = DGVuserDATA.SelectedRows(0).Cells("Where").Value
        passer_Click(sender, e)
    End Sub
End Class