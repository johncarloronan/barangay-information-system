﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UPDATEcomplain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TBComplainant = New System.Windows.Forms.TextBox()
        Me.TBincident = New System.Windows.Forms.TextBox()
        Me.TBactiontaken = New System.Windows.Forms.TextBox()
        Me.CBremarks = New System.Windows.Forms.ComboBox()
        Me.TBdetails = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.caseNum = New System.Windows.Forms.Label()
        Me.caseTime = New System.Windows.Forms.Label()
        Me.caseDate = New System.Windows.Forms.Label()
        Me.TBaddress = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.Eadmin = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TBComplainant
        '
        Me.TBComplainant.BackColor = System.Drawing.Color.AliceBlue
        Me.TBComplainant.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBComplainant.ForeColor = System.Drawing.Color.Black
        Me.TBComplainant.Location = New System.Drawing.Point(121, 176)
        Me.TBComplainant.Name = "TBComplainant"
        Me.TBComplainant.Size = New System.Drawing.Size(182, 22)
        Me.TBComplainant.TabIndex = 26
        '
        'TBincident
        '
        Me.TBincident.BackColor = System.Drawing.Color.AliceBlue
        Me.TBincident.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBincident.ForeColor = System.Drawing.Color.Black
        Me.TBincident.Location = New System.Drawing.Point(120, 232)
        Me.TBincident.Multiline = True
        Me.TBincident.Name = "TBincident"
        Me.TBincident.Size = New System.Drawing.Size(183, 49)
        Me.TBincident.TabIndex = 27
        '
        'TBactiontaken
        '
        Me.TBactiontaken.BackColor = System.Drawing.Color.AliceBlue
        Me.TBactiontaken.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBactiontaken.ForeColor = System.Drawing.Color.Black
        Me.TBactiontaken.Location = New System.Drawing.Point(119, 351)
        Me.TBactiontaken.Multiline = True
        Me.TBactiontaken.Name = "TBactiontaken"
        Me.TBactiontaken.Size = New System.Drawing.Size(184, 58)
        Me.TBactiontaken.TabIndex = 28
        '
        'CBremarks
        '
        Me.CBremarks.BackColor = System.Drawing.Color.AliceBlue
        Me.CBremarks.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBremarks.ForeColor = System.Drawing.Color.Black
        Me.CBremarks.FormattingEnabled = True
        Me.CBremarks.Items.AddRange(New Object() {"Settled", "Unsettled"})
        Me.CBremarks.Location = New System.Drawing.Point(119, 415)
        Me.CBremarks.Name = "CBremarks"
        Me.CBremarks.Size = New System.Drawing.Size(184, 23)
        Me.CBremarks.TabIndex = 29
        '
        'TBdetails
        '
        Me.TBdetails.BackColor = System.Drawing.Color.AliceBlue
        Me.TBdetails.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBdetails.ForeColor = System.Drawing.Color.Black
        Me.TBdetails.Location = New System.Drawing.Point(119, 287)
        Me.TBdetails.Multiline = True
        Me.TBdetails.Name = "TBdetails"
        Me.TBdetails.Size = New System.Drawing.Size(184, 58)
        Me.TBdetails.TabIndex = 30
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(20, 115)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(53, 15)
        Me.Label5.TabIndex = 31
        Me.Label5.Text = "Case no."
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(20, 235)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 15)
        Me.Label6.TabIndex = 32
        Me.Label6.Text = "Incident:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(6, 200)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(0, 15)
        Me.Label7.TabIndex = 33
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(20, 179)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(80, 15)
        Me.Label8.TabIndex = 34
        Me.Label8.Text = "Complainant:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Black
        Me.Label9.Location = New System.Drawing.Point(20, 135)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(85, 15)
        Me.Label9.TabIndex = 35
        Me.Label9.Text = "Date of Filing:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Black
        Me.Label10.Location = New System.Drawing.Point(20, 155)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(88, 15)
        Me.Label10.TabIndex = 36
        Me.Label10.Text = "Time of Filing:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Black
        Me.Label11.Location = New System.Drawing.Point(20, 418)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(62, 15)
        Me.Label11.TabIndex = 37
        Me.Label11.Text = "Remarks:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Black
        Me.Label12.Location = New System.Drawing.Point(20, 354)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(84, 15)
        Me.Label12.TabIndex = 38
        Me.Label12.Text = "Action Taken:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(20, 290)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(95, 15)
        Me.Label13.TabIndex = 39
        Me.Label13.Text = "Details of Event:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Black
        Me.Label14.Location = New System.Drawing.Point(20, 447)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(55, 15)
        Me.Label14.TabIndex = 40
        Me.Label14.Text = "Encoder:"
        '
        'caseNum
        '
        Me.caseNum.AutoSize = True
        Me.caseNum.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.caseNum.ForeColor = System.Drawing.Color.Black
        Me.caseNum.Location = New System.Drawing.Point(123, 115)
        Me.caseNum.Name = "caseNum"
        Me.caseNum.Size = New System.Drawing.Size(63, 15)
        Me.caseNum.TabIndex = 41
        Me.caseNum.Text = "00000000"
        '
        'caseTime
        '
        Me.caseTime.AutoSize = True
        Me.caseTime.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.caseTime.ForeColor = System.Drawing.Color.Black
        Me.caseTime.Location = New System.Drawing.Point(123, 155)
        Me.caseTime.Name = "caseTime"
        Me.caseTime.Size = New System.Drawing.Size(55, 15)
        Me.caseTime.TabIndex = 42
        Me.caseTime.Text = "00000000"
        '
        'caseDate
        '
        Me.caseDate.AutoSize = True
        Me.caseDate.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.caseDate.ForeColor = System.Drawing.Color.Black
        Me.caseDate.Location = New System.Drawing.Point(123, 135)
        Me.caseDate.Name = "caseDate"
        Me.caseDate.Size = New System.Drawing.Size(55, 15)
        Me.caseDate.TabIndex = 43
        Me.caseDate.Text = "00000000"
        '
        'TBaddress
        '
        Me.TBaddress.BackColor = System.Drawing.Color.AliceBlue
        Me.TBaddress.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBaddress.ForeColor = System.Drawing.Color.Black
        Me.TBaddress.Location = New System.Drawing.Point(121, 204)
        Me.TBaddress.Name = "TBaddress"
        Me.TBaddress.Size = New System.Drawing.Size(182, 22)
        Me.TBaddress.TabIndex = 46
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(20, 207)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 15)
        Me.Label1.TabIndex = 47
        Me.Label1.Text = "Address:"
        '
        'btnUpdate
        '
        Me.btnUpdate.BackColor = System.Drawing.Color.Teal
        Me.btnUpdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnUpdate.FlatAppearance.BorderSize = 0
        Me.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUpdate.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpdate.ForeColor = System.Drawing.Color.White
        Me.btnUpdate.Location = New System.Drawing.Point(119, 472)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(184, 35)
        Me.btnUpdate.TabIndex = 44
        Me.btnUpdate.Text = "Update"
        Me.btnUpdate.UseVisualStyleBackColor = False
        '
        'Eadmin
        '
        Me.Eadmin.BackColor = System.Drawing.Color.AliceBlue
        Me.Eadmin.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Eadmin.ForeColor = System.Drawing.Color.Black
        Me.Eadmin.Location = New System.Drawing.Point(119, 444)
        Me.Eadmin.Name = "Eadmin"
        Me.Eadmin.ReadOnly = True
        Me.Eadmin.Size = New System.Drawing.Size(184, 22)
        Me.Eadmin.TabIndex = 45
        Me.Eadmin.Text = "ADMIN"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.Label20.Location = New System.Drawing.Point(139, 56)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(94, 22)
        Me.Label20.TabIndex = 138
        Me.Label20.Text = "RECORD"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.Label19.Location = New System.Drawing.Point(125, 34)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(87, 22)
        Me.Label19.TabIndex = 137
        Me.Label19.Text = "UPDATE"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = Global.BISfinal.My.Resources.Resources.pngfind_com_notepad_png_986914
        Me.PictureBox1.Location = New System.Drawing.Point(5, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(126, 90)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 136
        Me.PictureBox1.TabStop = False
        '
        'UPDATEcomplain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(323, 536)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Eadmin)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.TBaddress)
        Me.Controls.Add(Me.caseDate)
        Me.Controls.Add(Me.caseTime)
        Me.Controls.Add(Me.caseNum)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.TBdetails)
        Me.Controls.Add(Me.CBremarks)
        Me.Controls.Add(Me.TBactiontaken)
        Me.Controls.Add(Me.TBincident)
        Me.Controls.Add(Me.TBComplainant)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "UPDATEcomplain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TBComplainant As TextBox
    Friend WithEvents TBincident As TextBox
    Friend WithEvents TBactiontaken As TextBox
    Friend WithEvents CBremarks As ComboBox
    Friend WithEvents TBdetails As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents caseNum As Label
    Friend WithEvents caseTime As Label
    Friend WithEvents caseDate As Label
    Friend WithEvents TBaddress As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents btnUpdate As Button
    Friend WithEvents Eadmin As TextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents PictureBox1 As PictureBox
End Class
