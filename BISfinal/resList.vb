﻿Imports FireSharp.Config
Imports FireSharp.Response
Imports FireSharp.Interfaces

Imports Newtonsoft.Json

Imports System.Web.Script.Serialization '(Importing System.Web.Script.Serialization) This is used to read and convert JSON strings.
Imports System.ComponentModel '(Importing System.ComponentModel) This is used to sort data on DataGridView.
Imports System.IO

Public Class resList

    Dim clearDGVCol As Boolean = True

    Private dtTableGrd As DataTable 'This variable is used to search data on DataGridView.

    '-------------------------------------------Configure FireSharp
    Private fcon As New FirebaseConfig() With
        {
        .AuthSecret = "8kPwBHDkDdZWGHts9sb98dsJu45thBvgDTUrgqJj",
        .BasePath = "https://bisrealtime-default-rtdb.firebaseio.com/"
        }

    Private client As IFirebaseClient





    Sub ShowRecord()
        Try
            Dim dtTable As New DataTable
            dtTable.Columns.Add("Fullname")
            dtTable.Columns.Add("ID")
            dtTable.Columns.Add("LastName")
            dtTable.Columns.Add("First Name")
            dtTable.Columns.Add("Middle Name")
            dtTable.Columns.Add("Suffix")
            dtTable.Columns.Add("Birth Date")
            dtTable.Columns.Add("Age")
            dtTable.Columns.Add("Gender")
            dtTable.Columns.Add("Address")
            dtTable.Columns.Add("Birth Place")
            dtTable.Columns.Add("Province")
            dtTable.Columns.Add("Contact No.")
            dtTable.Columns.Add("Blood Type")
            dtTable.Columns.Add("Religion")



            If clearDGVCol = True Then

                clearDGVCol = False
            End If
            '-------------------------------------------

            Dim SRRecord = client.Get("ResidentDB/")

            Dim myJsonTool As New JavaScriptSerializer
            Dim myDeserializedItems = myJsonTool.Deserialize(Of Dictionary(Of String, ResidentData))(SRRecord.Body)

            For Each dictItem As KeyValuePair(Of String, ResidentData) In myDeserializedItems
                dtTable.Rows.Add(dictItem.Value.bFullname, dictItem.Value.bID, dictItem.Value.bLname, dictItem.Value.bFname, dictItem.Value.bMname, dictItem.Value.bSuffix, dictItem.Value.bBdate, dictItem.Value.bAge, dictItem.Value.bGender, dictItem.Value.bAddress, dictItem.Value.bBplace, dictItem.Value.bProvince, dictItem.Value.bContact, dictItem.Value.bBtype, dictItem.Value.bReligion)
            Next


            dtTableGrd = dtTable


        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ElseIf ex.Message = "Object reference not set to an instance of an object." Then
                Dim dtTable As New DataTable
                dtTable.Columns.Add("Fullname")
                dtTable.Columns.Add("ID")
                dtTable.Columns.Add("LastName")
                dtTable.Columns.Add("First Name")
                dtTable.Columns.Add("Middle Name")
                dtTable.Columns.Add("Suffix")
                dtTable.Columns.Add("Birth Date")
                dtTable.Columns.Add("Age")
                dtTable.Columns.Add("Gender")
                dtTable.Columns.Add("Address")
                dtTable.Columns.Add("Birth Place")
                dtTable.Columns.Add("Province")
                dtTable.Columns.Add("Contact No.")
                dtTable.Columns.Add("Blood Type")
                dtTable.Columns.Add("Religion")
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If




        End Try

    End Sub

    Private Sub RTB_Click(sender As Object, e As EventArgs) Handles RTB.Click
        Dim res As FirebaseResponse = client.Get("ResidentDB")
        Dim data As Dictionary(Of String, ResidentData) = JsonConvert.DeserializeObject(Of Dictionary(Of String, ResidentData))(res.Body.ToString())
        UpdateRTB(data)
    End Sub

    Sub UpdateRTB(ByVal records As Dictionary(Of String, ResidentData))
        List.Clear()

        For Each item In records
            List.Text += "-------------------------------------------------------------" & vbCrLf + " Full Name    :  " + item.Value.bFullname.ToString() + vbLf
            List.Text += " ID no.           :  " + item.Key + vbLf
            List.Text += " Birth Date    :  " + item.Value.bBdate.ToString() + vbLf
            List.Text += " Age               :  " + item.Value.bAge.ToString() + vbLf
            List.Text += " Gender         :  " + item.Value.bGender.ToString() + vbLf
            List.Text += " Address        :  " + item.Value.bAddress.ToString() + vbLf
            List.Text += " Birth Place   :  " + item.Value.bBplace.ToString() + vbLf
            List.Text += " Province       :  " + item.Value.bProvince.ToString() + vbLf
            List.Text += " Contact #      :  " + item.Value.bContact.ToString() + vbLf
            List.Text += " Blood Type   :  " + item.Value.bBtype.ToString() + vbLf
            List.Text += " Religion        :  " + item.Value.bReligion.ToString() + vbLf

        Next
    End Sub


    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        ShowRecord()


    End Sub

    Private Sub resList_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            client = New FireSharp.FirebaseClient(fcon)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        ShowRecord()

        Dim clearDGVCol As Boolean = True

        RTB_Click(sender, e)
        btnRefresh_Click(sender, e)
    End Sub

    Private Sub List_TextChanged(sender As Object, e As EventArgs) Handles List.TextChanged

    End Sub

    Private Sub resList_MouseMove(sender As Object, e As MouseEventArgs) Handles Me.MouseMove

    End Sub
End Class