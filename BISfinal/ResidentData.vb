﻿Public Class ResidentData
    Public Property bFullname() As String = ""
    Public Property bID() As String = ""
    Public Property bLname() As String = ""
    Public Property bFname() As String = ""
    Public Property bMname() As String = ""
    Public Property bSuffix() As String = ""
    Public Property bBdate() As String = ""
    Public Property bAge() As String = ""
    Public Property bGender() As String = ""
    Public Property bAddress() As String = ""
    Public Property bBplace() As String = ""
    Public Property bOccupation() As String = ""
    Public Property bVoter() As String = ""
    Public Property bProvince() As String = ""
    Public Property bContact() As String = ""
    Public Property bBtype() As String = ""
    Public Property bStatus() As String = ""
    Public Property bCitizenship() As String = ""
    Public Property bReligion() As String = ""
    Public Property Image() As String = ""



End Class
