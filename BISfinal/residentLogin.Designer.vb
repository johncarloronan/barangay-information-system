﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class residentLogin
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.loading = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.CBPurpose = New System.Windows.Forms.ComboBox()
        Me.TBName = New System.Windows.Forms.TextBox()
        Me.BtnLogin = New System.Windows.Forms.Button()
        Me.LTime = New System.Windows.Forms.Label()
        Me.LDate = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.TBlogid = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'loading
        '
        Me.loading.AutoSize = True
        Me.loading.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.loading.Location = New System.Drawing.Point(100, 348)
        Me.loading.Name = "loading"
        Me.loading.Size = New System.Drawing.Size(181, 21)
        Me.loading.TabIndex = 29
        Me.loading.Text = "Loading.... Please Wait"
        Me.loading.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.White
        Me.Label4.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Gray
        Me.Label4.Location = New System.Drawing.Point(107, 260)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(90, 27)
        Me.Label4.TabIndex = 28
        Me.Label4.Text = "Purpose"
        '
        'CBPurpose
        '
        Me.CBPurpose.BackColor = System.Drawing.Color.White
        Me.CBPurpose.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.CBPurpose.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBPurpose.FormattingEnabled = True
        Me.CBPurpose.Items.AddRange(New Object() {"Registration", "Request Document", "Permit", "Blotter", "Updates", "Visit Only"})
        Me.CBPurpose.Location = New System.Drawing.Point(100, 256)
        Me.CBPurpose.Name = "CBPurpose"
        Me.CBPurpose.Size = New System.Drawing.Size(332, 35)
        Me.CBPurpose.TabIndex = 27
        '
        'TBName
        '
        Me.TBName.BackColor = System.Drawing.Color.White
        Me.TBName.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBName.Location = New System.Drawing.Point(100, 215)
        Me.TBName.Name = "TBName"
        Me.TBName.Size = New System.Drawing.Size(332, 35)
        Me.TBName.TabIndex = 26
        '
        'BtnLogin
        '
        Me.BtnLogin.BackColor = System.Drawing.Color.DodgerBlue
        Me.BtnLogin.FlatAppearance.BorderSize = 0
        Me.BtnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnLogin.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnLogin.ForeColor = System.Drawing.Color.White
        Me.BtnLogin.Location = New System.Drawing.Point(100, 297)
        Me.BtnLogin.Name = "BtnLogin"
        Me.BtnLogin.Size = New System.Drawing.Size(332, 46)
        Me.BtnLogin.TabIndex = 23
        Me.BtnLogin.Text = "Log In"
        Me.BtnLogin.UseVisualStyleBackColor = False
        '
        'LTime
        '
        Me.LTime.AutoSize = True
        Me.LTime.BackColor = System.Drawing.Color.Transparent
        Me.LTime.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LTime.ForeColor = System.Drawing.Color.Black
        Me.LTime.Location = New System.Drawing.Point(423, 90)
        Me.LTime.Name = "LTime"
        Me.LTime.Size = New System.Drawing.Size(38, 15)
        Me.LTime.TabIndex = 31
        Me.LTime.Text = "TIME"
        Me.LTime.Visible = False
        '
        'LDate
        '
        Me.LDate.AutoSize = True
        Me.LDate.BackColor = System.Drawing.Color.Transparent
        Me.LDate.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LDate.ForeColor = System.Drawing.Color.Black
        Me.LDate.Location = New System.Drawing.Point(423, 72)
        Me.LDate.Name = "LDate"
        Me.LDate.Size = New System.Drawing.Size(40, 15)
        Me.LDate.TabIndex = 30
        Me.LDate.Text = "DATE"
        Me.LDate.Visible = False
        '
        'Timer1
        '
        '
        'TBlogid
        '
        Me.TBlogid.AutoSize = True
        Me.TBlogid.Location = New System.Drawing.Point(424, 168)
        Me.TBlogid.Name = "TBlogid"
        Me.TBlogid.Size = New System.Drawing.Size(39, 13)
        Me.TBlogid.TabIndex = 32
        Me.TBlogid.Text = "Label7"
        Me.TBlogid.Visible = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.BISfinal.My.Resources.Resources._196574417_318685429726734_7497525660324242603_n
        Me.PictureBox1.Location = New System.Drawing.Point(81, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(330, 178)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 22
        Me.PictureBox1.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox5.Image = Global.BISfinal.My.Resources.Resources.Daco_1676019
        Me.PictureBox5.Location = New System.Drawing.Point(47, 256)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(47, 35)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox5.TabIndex = 25
        Me.PictureBox5.TabStop = False
        '
        'PictureBox6
        '
        Me.PictureBox6.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox6.Image = Global.BISfinal.My.Resources.Resources.Daco_4214581__1_
        Me.PictureBox6.Location = New System.Drawing.Point(47, 215)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(47, 35)
        Me.PictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox6.TabIndex = 24
        Me.PictureBox6.TabStop = False
        '
        'residentLogin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(502, 392)
        Me.Controls.Add(Me.TBlogid)
        Me.Controls.Add(Me.LTime)
        Me.Controls.Add(Me.LDate)
        Me.Controls.Add(Me.loading)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.CBPurpose)
        Me.Controls.Add(Me.TBName)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.PictureBox5)
        Me.Controls.Add(Me.BtnLogin)
        Me.Controls.Add(Me.PictureBox6)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "residentLogin"
        Me.Text = "residentLogin"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents loading As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents CBPurpose As ComboBox
    Friend WithEvents TBName As TextBox
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents PictureBox5 As PictureBox
    Friend WithEvents BtnLogin As Button
    Friend WithEvents PictureBox6 As PictureBox
    Friend WithEvents LTime As Label
    Friend WithEvents LDate As Label
    Friend WithEvents Timer1 As Timer
    Friend WithEvents TBlogid As Label
End Class
