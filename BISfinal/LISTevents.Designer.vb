﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LISTevents
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.List = New System.Windows.Forms.RichTextBox()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.RTB = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'List
        '
        Me.List.BackColor = System.Drawing.Color.White
        Me.List.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.List.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.List.ForeColor = System.Drawing.Color.Black
        Me.List.Location = New System.Drawing.Point(0, 0)
        Me.List.Name = "List"
        Me.List.ReadOnly = True
        Me.List.Size = New System.Drawing.Size(873, 655)
        Me.List.TabIndex = 0
        Me.List.Text = ""
        '
        'btnRefresh
        '
        Me.btnRefresh.Location = New System.Drawing.Point(541, 188)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(41, 23)
        Me.btnRefresh.TabIndex = 79
        Me.btnRefresh.Text = "R"
        Me.btnRefresh.UseVisualStyleBackColor = True
        Me.btnRefresh.Visible = False
        '
        'RTB
        '
        Me.RTB.Location = New System.Drawing.Point(543, 217)
        Me.RTB.Name = "RTB"
        Me.RTB.Size = New System.Drawing.Size(39, 23)
        Me.RTB.TabIndex = 78
        Me.RTB.Text = "RTB"
        Me.RTB.UseVisualStyleBackColor = True
        Me.RTB.Visible = False
        '
        'LISTevents
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(861, 655)
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.RTB)
        Me.Controls.Add(Me.List)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "LISTevents"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "LISTevents"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents List As RichTextBox
    Friend WithEvents btnRefresh As Button
    Friend WithEvents RTB As Button
End Class
