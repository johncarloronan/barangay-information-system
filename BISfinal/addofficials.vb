﻿Imports FireSharp.Config
Imports FireSharp.Response
Imports FireSharp.Interfaces
'-------------------------------------------

Imports System.Web.Script.Serialization '(Importing System.Web.Script.Serialization) This is used to read and convert JSON strings.
Imports System.ComponentModel '(Importing System.ComponentModel) This is used to sort data on DataGridView.
Imports System.IO

Public Class addOfficials

    Public Property UPDATEoffname As String
    Public Property UPDATEoffposition As String
    Public Property UPDATEoffnum As String
    Dim bmp As Bitmap
    Dim IMG_FileNameInput As String
    Dim clearDGVCol As Boolean = True

    Private dtTableGrd As DataTable


    Private fcon As New FirebaseConfig() With
        {
        .AuthSecret = "8kPwBHDkDdZWGHts9sb98dsJu45thBvgDTUrgqJj",
        .BasePath = "https://bisrealtime-default-rtdb.firebaseio.com/"
        }

    Private client As IFirebaseClient

    Sub ShowRecord()
        Try
            Dim dtTable As New DataTable
            dtTable.Columns.Add("Name")
            dtTable.Columns.Add("Position")
            dtTable.Columns.Add("Num")



            If clearDGVCol = True Then

                clearDGVCol = False
            End If


            Dim SRRecord = client.Get("OfficialsDB/")


            Dim myJsonTool As New JavaScriptSerializer
            Dim myDeserializedItems = myJsonTool.Deserialize(Of Dictionary(Of String, OfficialsDATA))(SRRecord.Body)


            For Each dictItem As KeyValuePair(Of String, OfficialsDATA) In myDeserializedItems
                dtTable.Rows.Add(dictItem.Value.iName, dictItem.Value.iPosition, dictItem.Value.iNum)
            Next


            dtTableGrd = dtTable



        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ElseIf ex.Message = "Object reference not set to an instance of an object." Then
                Dim dtTable As New DataTable
                dtTable.Columns.Add("Name")
                dtTable.Columns.Add("Position")
                dtTable.Columns.Add("Num")
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If



        End Try

    End Sub
    Private Sub addOfficials_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            client = New FireSharp.FirebaseClient(fcon)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        tbid.Text = UPDATEoffnum
        tbName.Text = UPDATEoffname
        CBposition.Text = UPDATEoffposition
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try

            Dim WA As New OfficialsDATA() With
                {
            .iName = tbName.Text,
            .iPosition = CBposition.Text,
            .iNum = tbid.Text
                }
            Dim save = client.Set("OfficialsDB/" + tbid.Text, WA)
            MsgBox("Added Successfully")
            tbName.Text = ""
            CBposition.Text = ""
            Me.Close()




        Catch ex As Exception

        End Try
    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click

    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Try

            Dim ZZ As New OfficialsDATA() With
                {
                .iName = tbName.Text,
                .iPosition = CBposition.Text,
                .iNum = tbid.Text
                }

            Dim update = client.Update("OfficialsDB/" + tbid.Text, ZZ)

            MessageBox.Show("Data updated successfully.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            tbName.Clear()
            CBposition.Text = ""
            Me.Close()






        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If


        End Try
    End Sub
End Class