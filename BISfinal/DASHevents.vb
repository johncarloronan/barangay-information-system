﻿Imports FireSharp.Config
Imports FireSharp.Response
Imports FireSharp.Interfaces

Imports Newtonsoft.Json

Imports System.Web.Script.Serialization '(Importing System.Web.Script.Serialization) This is used to read and convert JSON strings.
Imports System.ComponentModel '(Importing System.ComponentModel) This is used to sort data on DataGridView.
Imports System.IO

Public Class DASHevents


    Dim bmp As Bitmap

    Dim clearDGVCol As Boolean = True

    Private dtTableGrd As DataTable


    Private fcon As New FirebaseConfig() With
        {
        .AuthSecret = "8kPwBHDkDdZWGHts9sb98dsJu45thBvgDTUrgqJj",
        .BasePath = "https://bisrealtime-default-rtdb.firebaseio.com/"
        }

    Private client As IFirebaseClient



    Sub ShowRecord()
        Try
            Dim dtTable As New DataTable
            dtTable.Columns.Add("Theme")
            dtTable.Columns.Add("What")
            dtTable.Columns.Add("Why")
            dtTable.Columns.Add("When")
            dtTable.Columns.Add("Who")
            dtTable.Columns.Add("Where")



            If clearDGVCol = True Then

                clearDGVCol = False
            End If


            Dim SRRecord = client.Get("newAnnouncementDB/")


            Dim myJsonTool As New JavaScriptSerializer
            Dim myDeserializedItems = myJsonTool.Deserialize(Of Dictionary(Of String, AnnouncementDATA))(SRRecord.Body)


            For Each dictItem As KeyValuePair(Of String, AnnouncementDATA) In myDeserializedItems
                dtTable.Rows.Add(dictItem.Value.hTheme, dictItem.Value.hWhat, dictItem.Value.hWhy, dictItem.Value.hWhen, dictItem.Value.hWho, dictItem.Value.hWhere)
            Next



            DGVUserData.DataSource = dtTable
            dtTableGrd = dtTable
            '-------------------------------------------
            DGVUserData.Sort(DGVUserData.Columns(0), ListSortDirection.Ascending)

            lblTotal.Text = "" & (DGVUserData.RowCount)

            DGVUserData.ClearSelection()

        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ElseIf ex.Message = "Object reference not set to an instance of an object." Then
                Dim dtTable As New DataTable
                dtTable.Columns.Add("Theme")
                dtTable.Columns.Add("What")
                dtTable.Columns.Add("Why")
                dtTable.Columns.Add("When")
                dtTable.Columns.Add("Who")
                dtTable.Columns.Add("Where")
                DGVUserData.DataSource = dtTable
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If



        End Try
    End Sub
    Private Sub DASHevents_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            client = New FireSharp.FirebaseClient(fcon)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        ShowRecord()
        Dim clearDGVCol As Boolean = True

        Timer1.Interval = 30000
        Timer1.Enabled = True


    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        btnRefresh_Click(sender, e)
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        ShowRecord()

    End Sub
End Class