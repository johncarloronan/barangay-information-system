﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RECORDblotter
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lbltotal = New System.Windows.Forms.Label()
        Me.DGVUserData = New System.Windows.Forms.DataGridView()
        Me.CBsb = New System.Windows.Forms.ComboBox()
        Me.tbS = New System.Windows.Forms.TextBox()
        Me.Taddress = New System.Windows.Forms.TextBox()
        Me.Tdate = New System.Windows.Forms.Label()
        Me.Ttime = New System.Windows.Forms.Label()
        Me.Tnum = New System.Windows.Forms.Label()
        Me.Tdetails = New System.Windows.Forms.TextBox()
        Me.Cremarks = New System.Windows.Forms.ComboBox()
        Me.Tactiontaken = New System.Windows.Forms.TextBox()
        Me.Tincident = New System.Windows.Forms.TextBox()
        Me.Tcomplainant = New System.Windows.Forms.TextBox()
        Me.PassRecord = New System.Windows.Forms.Button()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.btnDelete = New System.Windows.Forms.Button()
        CType(Me.DGVUserData, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lbltotal
        '
        Me.lbltotal.AutoSize = True
        Me.lbltotal.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltotal.Location = New System.Drawing.Point(508, 19)
        Me.lbltotal.Name = "lbltotal"
        Me.lbltotal.Size = New System.Drawing.Size(20, 22)
        Me.lbltotal.TabIndex = 126
        Me.lbltotal.Text = "0"
        '
        'DGVUserData
        '
        Me.DGVUserData.AllowUserToAddRows = False
        Me.DGVUserData.AllowUserToDeleteRows = False
        Me.DGVUserData.AllowUserToResizeColumns = False
        Me.DGVUserData.AllowUserToResizeRows = False
        Me.DGVUserData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DGVUserData.BackgroundColor = System.Drawing.Color.White
        Me.DGVUserData.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DGVUserData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.DGVUserData.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.Teal
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.SteelBlue
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVUserData.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.DGVUserData.ColumnHeadersHeight = 40
        Me.DGVUserData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.DGVUserData.Cursor = System.Windows.Forms.Cursors.Hand
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.MediumAquamarine
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGVUserData.DefaultCellStyle = DataGridViewCellStyle5
        Me.DGVUserData.EnableHeadersVisualStyles = False
        Me.DGVUserData.GridColor = System.Drawing.Color.DimGray
        Me.DGVUserData.Location = New System.Drawing.Point(0, 56)
        Me.DGVUserData.MultiSelect = False
        Me.DGVUserData.Name = "DGVUserData"
        Me.DGVUserData.ReadOnly = True
        Me.DGVUserData.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(CType(CType(52, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(52, Byte), Integer))
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVUserData.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.DGVUserData.RowHeadersVisible = False
        Me.DGVUserData.RowHeadersWidth = 50
        Me.DGVUserData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGVUserData.Size = New System.Drawing.Size(861, 598)
        Me.DGVUserData.TabIndex = 127
        '
        'CBsb
        '
        Me.CBsb.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBsb.FormattingEnabled = True
        Me.CBsb.Items.AddRange(New Object() {"Case no.", "Date", "Complainant", "Address", "Incident", "Remarks"})
        Me.CBsb.Location = New System.Drawing.Point(6, 12)
        Me.CBsb.Name = "CBsb"
        Me.CBsb.Size = New System.Drawing.Size(117, 31)
        Me.CBsb.TabIndex = 129
        Me.CBsb.Text = "Search by:"
        '
        'tbS
        '
        Me.tbS.BackColor = System.Drawing.Color.White
        Me.tbS.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbS.Location = New System.Drawing.Point(132, 12)
        Me.tbS.Name = "tbS"
        Me.tbS.Size = New System.Drawing.Size(296, 32)
        Me.tbS.TabIndex = 128
        Me.tbS.Visible = False
        '
        'Taddress
        '
        Me.Taddress.BackColor = System.Drawing.Color.AliceBlue
        Me.Taddress.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Taddress.ForeColor = System.Drawing.Color.Black
        Me.Taddress.Location = New System.Drawing.Point(252, 200)
        Me.Taddress.Name = "Taddress"
        Me.Taddress.Size = New System.Drawing.Size(152, 22)
        Me.Taddress.TabIndex = 142
        Me.Taddress.Visible = False
        '
        'Tdate
        '
        Me.Tdate.AutoSize = True
        Me.Tdate.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tdate.ForeColor = System.Drawing.Color.Black
        Me.Tdate.Location = New System.Drawing.Point(254, 131)
        Me.Tdate.Name = "Tdate"
        Me.Tdate.Size = New System.Drawing.Size(55, 15)
        Me.Tdate.TabIndex = 141
        Me.Tdate.Text = "00000000"
        Me.Tdate.Visible = False
        '
        'Ttime
        '
        Me.Ttime.AutoSize = True
        Me.Ttime.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ttime.ForeColor = System.Drawing.Color.Black
        Me.Ttime.Location = New System.Drawing.Point(254, 151)
        Me.Ttime.Name = "Ttime"
        Me.Ttime.Size = New System.Drawing.Size(55, 15)
        Me.Ttime.TabIndex = 140
        Me.Ttime.Text = "00000000"
        Me.Ttime.Visible = False
        '
        'Tnum
        '
        Me.Tnum.AutoSize = True
        Me.Tnum.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tnum.ForeColor = System.Drawing.Color.Black
        Me.Tnum.Location = New System.Drawing.Point(254, 111)
        Me.Tnum.Name = "Tnum"
        Me.Tnum.Size = New System.Drawing.Size(63, 15)
        Me.Tnum.TabIndex = 139
        Me.Tnum.Text = "00000000"
        Me.Tnum.Visible = False
        '
        'Tdetails
        '
        Me.Tdetails.BackColor = System.Drawing.Color.AliceBlue
        Me.Tdetails.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tdetails.ForeColor = System.Drawing.Color.Black
        Me.Tdetails.Location = New System.Drawing.Point(250, 283)
        Me.Tdetails.Multiline = True
        Me.Tdetails.Name = "Tdetails"
        Me.Tdetails.Size = New System.Drawing.Size(209, 58)
        Me.Tdetails.TabIndex = 138
        Me.Tdetails.Visible = False
        '
        'Cremarks
        '
        Me.Cremarks.BackColor = System.Drawing.Color.AliceBlue
        Me.Cremarks.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cremarks.ForeColor = System.Drawing.Color.Black
        Me.Cremarks.FormattingEnabled = True
        Me.Cremarks.Items.AddRange(New Object() {"Settled", "Unsettled"})
        Me.Cremarks.Location = New System.Drawing.Point(250, 411)
        Me.Cremarks.Name = "Cremarks"
        Me.Cremarks.Size = New System.Drawing.Size(121, 23)
        Me.Cremarks.TabIndex = 137
        Me.Cremarks.Visible = False
        '
        'Tactiontaken
        '
        Me.Tactiontaken.BackColor = System.Drawing.Color.AliceBlue
        Me.Tactiontaken.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tactiontaken.ForeColor = System.Drawing.Color.Black
        Me.Tactiontaken.Location = New System.Drawing.Point(250, 347)
        Me.Tactiontaken.Multiline = True
        Me.Tactiontaken.Name = "Tactiontaken"
        Me.Tactiontaken.Size = New System.Drawing.Size(209, 58)
        Me.Tactiontaken.TabIndex = 136
        Me.Tactiontaken.Visible = False
        '
        'Tincident
        '
        Me.Tincident.BackColor = System.Drawing.Color.AliceBlue
        Me.Tincident.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tincident.ForeColor = System.Drawing.Color.Black
        Me.Tincident.Location = New System.Drawing.Point(251, 228)
        Me.Tincident.Multiline = True
        Me.Tincident.Name = "Tincident"
        Me.Tincident.Size = New System.Drawing.Size(153, 49)
        Me.Tincident.TabIndex = 135
        Me.Tincident.Visible = False
        '
        'Tcomplainant
        '
        Me.Tcomplainant.BackColor = System.Drawing.Color.AliceBlue
        Me.Tcomplainant.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tcomplainant.ForeColor = System.Drawing.Color.Black
        Me.Tcomplainant.Location = New System.Drawing.Point(252, 172)
        Me.Tcomplainant.Name = "Tcomplainant"
        Me.Tcomplainant.Size = New System.Drawing.Size(152, 22)
        Me.Tcomplainant.TabIndex = 134
        Me.Tcomplainant.Visible = False
        '
        'PassRecord
        '
        Me.PassRecord.Location = New System.Drawing.Point(572, 217)
        Me.PassRecord.Name = "PassRecord"
        Me.PassRecord.Size = New System.Drawing.Size(75, 23)
        Me.PassRecord.TabIndex = 143
        Me.PassRecord.Text = "PASS"
        Me.PassRecord.UseVisualStyleBackColor = True
        Me.PassRecord.Visible = False
        '
        'btnRefresh
        '
        Me.btnRefresh.BackgroundImage = Global.BISfinal.My.Resources.Resources.refresh__1_
        Me.btnRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnRefresh.FlatAppearance.BorderSize = 0
        Me.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRefresh.Location = New System.Drawing.Point(814, 14)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(37, 29)
        Me.btnRefresh.TabIndex = 133
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = Global.BISfinal.My.Resources.Resources.pngaaa_com_25560
        Me.PictureBox1.Location = New System.Drawing.Point(401, 16)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(24, 24)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 132
        Me.PictureBox1.TabStop = False
        Me.PictureBox1.Visible = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = Global.BISfinal.My.Resources.Resources.pngfind_com_notepad_png_986914
        Me.PictureBox5.Location = New System.Drawing.Point(465, 14)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(37, 29)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox5.TabIndex = 131
        Me.PictureBox5.TabStop = False
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.Transparent
        Me.btnDelete.BackgroundImage = Global.BISfinal.My.Resources.Resources.trash
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.White
        Me.btnDelete.Location = New System.Drawing.Point(771, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(37, 29)
        Me.btnDelete.TabIndex = 130
        Me.btnDelete.UseVisualStyleBackColor = False
        '
        'RECORDblotter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(861, 655)
        Me.Controls.Add(Me.PassRecord)
        Me.Controls.Add(Me.Taddress)
        Me.Controls.Add(Me.Tdate)
        Me.Controls.Add(Me.Ttime)
        Me.Controls.Add(Me.Tnum)
        Me.Controls.Add(Me.Tdetails)
        Me.Controls.Add(Me.Cremarks)
        Me.Controls.Add(Me.Tactiontaken)
        Me.Controls.Add(Me.Tincident)
        Me.Controls.Add(Me.Tcomplainant)
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.PictureBox5)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.CBsb)
        Me.Controls.Add(Me.tbS)
        Me.Controls.Add(Me.DGVUserData)
        Me.Controls.Add(Me.lbltotal)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "RECORDblotter"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.DGVUserData, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lbltotal As Label
    Friend WithEvents DGVUserData As DataGridView
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents PictureBox5 As PictureBox
    Friend WithEvents btnDelete As Button
    Friend WithEvents CBsb As ComboBox
    Friend WithEvents tbS As TextBox
    Friend WithEvents btnRefresh As Button
    Friend WithEvents Taddress As TextBox
    Friend WithEvents Tdate As Label
    Friend WithEvents Ttime As Label
    Friend WithEvents Tnum As Label
    Friend WithEvents Tdetails As TextBox
    Friend WithEvents Cremarks As ComboBox
    Friend WithEvents Tactiontaken As TextBox
    Friend WithEvents Tincident As TextBox
    Friend WithEvents Tcomplainant As TextBox
    Friend WithEvents PassRecord As Button
End Class
