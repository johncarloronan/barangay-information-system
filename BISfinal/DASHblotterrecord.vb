﻿Imports FireSharp.Config
Imports FireSharp.Response
Imports FireSharp.Interfaces
'-------------------------------------------

Imports System.Web.Script.Serialization '(Importing System.Web.Script.Serialization) This is used to read and convert JSON strings.
Imports System.ComponentModel '(Importing System.ComponentModel) This is used to sort data on DataGridView.
Imports System.IO

Public Class DASHblotterrecord

    Dim clearDGVCol As Boolean = True

    Private dtTableGrd As DataTable 'This variable is used to search data on DataGridView.

    '-------------------------------------------Configure FireSharp
    Private fcon As New FirebaseConfig() With
        {
        .AuthSecret = "8kPwBHDkDdZWGHts9sb98dsJu45thBvgDTUrgqJj",
        .BasePath = "https://bisrealtime-default-rtdb.firebaseio.com/"
        }

    Private client As IFirebaseClient

    Public Function ImageToBase64(image As Image) As String
        Using ms As New MemoryStream()
            ' Convert Image to byte[]  
            Dim Format As System.Drawing.Imaging.ImageFormat = Imaging.ImageFormat.Jpeg
            image.Save(ms, Format)
            Dim imageBytes As Byte() = ms.ToArray()

            ' Convert byte[] to Base64 String  
            Dim base64String As String = Convert.ToBase64String(imageBytes)
            Return base64String
        End Using
    End Function

    Public Function Base64ToImage(base64String As String) As Image
        ' Convert Base64 String to byte[]  
        Dim imageBytes As Byte() = Convert.FromBase64String(base64String)
        Dim ms As New MemoryStream(imageBytes, 0, imageBytes.Length)

        ' Convert byte[] to Image  
        ms.Write(imageBytes, 0, imageBytes.Length)
        Dim image__1 As Image = System.Drawing.Image.FromStream(ms, True)
        Return image__1
    End Function

    Sub DisplayRecLoad(Stat As Boolean)
        DGVUserData.Enabled = Stat
        btnRefresh.Enabled = Stat

    End Sub



    Sub ShowRecord()
        Try
            Dim dtTable As New DataTable
            dtTable.Columns.Add("Case no.")
            dtTable.Columns.Add("Date")
            dtTable.Columns.Add("Time")
            dtTable.Columns.Add("Complainant")
            dtTable.Columns.Add("Address")
            dtTable.Columns.Add("Incident")
            dtTable.Columns.Add("Details")
            dtTable.Columns.Add("Action")
            dtTable.Columns.Add("Remarks")
            dtTable.Columns.Add("Encoder")



            If clearDGVCol = True Then
                DGVUserData.Columns.Clear()
                clearDGVCol = False
            End If


            Dim SRRecord = client.Get("BlotterDB/")


            Dim myJsonTool As New JavaScriptSerializer
            Dim myDeserializedItems = myJsonTool.Deserialize(Of Dictionary(Of String, ComplainData))(SRRecord.Body)


            For Each dictItem As KeyValuePair(Of String, ComplainData) In myDeserializedItems
                dtTable.Rows.Add(dictItem.Value.eCasenumber, dictItem.Value.eDate, dictItem.Value.eTime, dictItem.Value.eComplainant, dictItem.Value.eAddress, dictItem.Value.eIncident, dictItem.Value.eDetails, dictItem.Value.eAction, dictItem.Value.eRemarks, dictItem.Value.eEncoder)
            Next

            DGVUserData.DataSource = dtTable
            dtTableGrd = dtTable

            DGVUserData.Sort(DGVUserData.Columns(0), ListSortDirection.Ascending)

            lblTotal.Text = "" & (DGVUserData.RowCount)

            DisplayRecLoad(True)

            DGVUserData.ClearSelection()


        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ElseIf ex.Message = "Object reference not set to an instance of an object." Then
                Dim dtTable As New DataTable
                dtTable.Columns.Add("Case no.")
                dtTable.Columns.Add("Date")
                dtTable.Columns.Add("Time")
                dtTable.Columns.Add("Complainant")
                dtTable.Columns.Add("Address")
                dtTable.Columns.Add("Incident")
                dtTable.Columns.Add("Details")
                dtTable.Columns.Add("Action")
                dtTable.Columns.Add("Remarks")
                dtTable.Columns.Add("Encoder")
                DGVUserData.DataSource = dtTable
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

            DisplayRecLoad(True)

        End Try

    End Sub

    Private Sub DASHblotterrecord_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            client = New FireSharp.FirebaseClient(fcon)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        ShowRecord()
        Dim clearDGVCol As Boolean = True

        Timer1.Interval = 30000
        Timer1.Enabled = True
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        ShowRecord()

    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        btnRefresh_Click(sender, e)
    End Sub
End Class