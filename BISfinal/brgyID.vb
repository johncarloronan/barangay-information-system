﻿Imports FireSharp.Config
Imports FireSharp.Response
Imports FireSharp.Interfaces
'-------------------------------------------

Imports System.Web.Script.Serialization '(Importing System.Web.Script.Serialization) This is used to read and convert JSON strings.
Imports System.ComponentModel '(Importing System.ComponentModel) This is used to sort data on DataGridView.
Imports System.IO

Imports AForge
Imports AForge.Video
Imports AForge.Video.DirectShow


Public Class brgyID
    Dim CAMERA As VideoCaptureDevice
    Dim bmp As Bitmap

    Dim IMG_FileNameInput As String
    Dim clearDGVCol As Boolean = True

    Private dtTableGrd As DataTable


    Private fcon As New FirebaseConfig() With
        {
        .AuthSecret = "8kPwBHDkDdZWGHts9sb98dsJu45thBvgDTUrgqJj",
        .BasePath = "https://bisrealtime-default-rtdb.firebaseio.com/"
        }

    Private client As IFirebaseClient

    Public Function ImageToBase64(image As Image) As String
        Using ms As New MemoryStream()
            ' Convert Image to byte[]  
            Dim Format As System.Drawing.Imaging.ImageFormat = Imaging.ImageFormat.Jpeg
            image.Save(ms, Format)
            Dim imageBytes As Byte() = ms.ToArray()

            ' Convert byte[] to Base64 String  
            Dim base64String As String = Convert.ToBase64String(imageBytes)
            Return base64String
        End Using
    End Function

    Public Function Base64ToImage(base64String As String) As Image
        ' Convert Base64 String to byte[]  
        Dim imageBytes As Byte() = Convert.FromBase64String(base64String)
        Dim ms As New MemoryStream(imageBytes, 0, imageBytes.Length)

        ' Convert byte[] to Image  
        ms.Write(imageBytes, 0, imageBytes.Length)
        Dim image__1 As Image = System.Drawing.Image.FromStream(ms, True)
        Return image__1
    End Function

    Sub DisplayRegSave(Stat As Boolean)
        TBFullname.Enabled = Stat
        dtBday.Enabled = Stat
        CBGender.Enabled = Stat
        TBAddress.Enabled = Stat
        TBBplace.Enabled = Stat
        TBProvince.Enabled = Stat
        TBContact.Enabled = Stat
        CBStatus.Enabled = Stat
        CBBtype.Enabled = Stat
        TBReligion.Enabled = Stat



    End Sub

    Sub DisplayRecLoad(Stat As Boolean)
        DGVUserData.Enabled = Stat

    End Sub

    Sub ShowRecord()
        Try
            Dim dtTable As New DataTable
            dtTable.Columns.Add("Fullname")
            dtTable.Columns.Add("ID")
            dtTable.Columns.Add("LastName")
            dtTable.Columns.Add("First Name")
            dtTable.Columns.Add("Middle Name")
            dtTable.Columns.Add("Suffix")
            dtTable.Columns.Add("Birth Date")
            dtTable.Columns.Add("Age")
            dtTable.Columns.Add("Gender")
            dtTable.Columns.Add("Address")
            dtTable.Columns.Add("Birth Place")
            dtTable.Columns.Add("Province")
            dtTable.Columns.Add("Contact No.")
            dtTable.Columns.Add("Status")
            dtTable.Columns.Add("Blood Type")
            dtTable.Columns.Add("Religion")
            dtTable.Columns.Add("Image", GetType(Image))




            If clearDGVCol = True Then
                DGVUserData.Columns.Clear()
                clearDGVCol = False
            End If


            Dim SRRecord = client.Get("ResidentDB/")


            Dim myJsonTool As New JavaScriptSerializer
            Dim myDeserializedItems = myJsonTool.Deserialize(Of Dictionary(Of String, ResidentData))(SRRecord.Body)


            For Each dictItem As KeyValuePair(Of String, ResidentData) In myDeserializedItems
                dtTable.Rows.Add(dictItem.Value.bFullname, dictItem.Value.bID, dictItem.Value.bLname, dictItem.Value.bFname, dictItem.Value.bMname, dictItem.Value.bSuffix, dictItem.Value.bBdate, dictItem.Value.bAge, dictItem.Value.bGender, dictItem.Value.bAddress, dictItem.Value.bBplace, dictItem.Value.bProvince, dictItem.Value.bContact, dictItem.Value.bStatus, dictItem.Value.bBtype, dictItem.Value.bReligion, Base64ToImage(dictItem.Value.Image))
            Next



            DGVUserData.DataSource = dtTable
            dtTableGrd = dtTable

            Dim imageColumn = DirectCast(DGVUserData.Columns("Image"), DataGridViewImageColumn)
            imageColumn.ImageLayout = DataGridViewImageCellLayout.Zoom

            DGVUserData.Sort(DGVUserData.Columns(0), ListSortDirection.Descending)



            DisplayRecLoad(True)

            DGVUserData.ClearSelection()


        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ElseIf ex.Message = "Object reference not set to an instance of an object." Then
                Dim dtTable As New DataTable
                dtTable.Columns.Add("Fullname")
                dtTable.Columns.Add("ID")
                DGVUserData.DataSource = dtTable

                MessageBox.Show("Database not found or Database is empty.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

            DisplayRecLoad(True)

        End Try
    End Sub







    Private Sub Label23_Click(sender As Object, e As EventArgs) Handles CBStatus.Click

    End Sub

    Private Sub brgyID_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            client = New FireSharp.FirebaseClient(fcon)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        ShowRecord()

        Dim clearDGVCol As Boolean = True
        Timer1.Enabled = True
        tbSearch.Text = " Search Surname..."
        tbSearch.ForeColor = Color.Gray

    End Sub

    Private Sub DGVUserData_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVUserData.CellContentClick

    End Sub

    Private Sub DGVUserData_Click(sender As Object, e As EventArgs) Handles DGVUserData.Click
        TBFullname.Text = DGVUserData.SelectedRows(0).Cells("Fullname").Value
        tbID.Text = DGVUserData.SelectedRows(0).Cells("ID").Value
        dtBday.Text = DGVUserData.SelectedRows(0).Cells("Birth Date").Value
        CBGender.Text = DGVUserData.SelectedRows(0).Cells("Gender").Value
        TBAddress.Text = DGVUserData.SelectedRows(0).Cells("Address").Value
        TBBplace.Text = DGVUserData.SelectedRows(0).Cells("Birth Place").Value
        TBProvince.Text = DGVUserData.SelectedRows(0).Cells("Province").Value
        TBContact.Text = DGVUserData.SelectedRows(0).Cells("Contact No.").Value
        CBStatus.Text = DGVUserData.SelectedRows(0).Cells("Status").Value
        CBBtype.Text = DGVUserData.SelectedRows(0).Cells("Blood Type").Value
        TBReligion.Text = DGVUserData.SelectedRows(0).Cells("Religion").Value
        PictureBoxUserReg.Image = DGVUserData.Rows(DGVUserData.SelectedRows(0).Index).Cells("Image").Value


        DGVUserData.Visible = False





    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        dtTableGrd.DefaultView.RowFilter = CBsb.Text & " Like '" & tbSearch.Text & "%'" 'To search for data on the DataGridView
        DGVUserData.ClearSelection()

        DGVUserData.Visible = True


    End Sub

    Private Sub brgyID_Click(sender As Object, e As EventArgs) Handles Me.Click
        DGVUserData.Visible = False



    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Ldate.Text = Date.Now.ToString("MM-dd-yyyy")
    End Sub

    Private Sub tbSearch_TextChanged(sender As Object, e As EventArgs) Handles tbSearch.TextChanged
        tbSearch.ForeColor = Color.Black
    End Sub

    Private Sub tbSearch_Click(sender As Object, e As EventArgs) Handles tbSearch.Click
        tbSearch.Text = ""
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        Dim cameras As VideoCaptureDeviceForm = New VideoCaptureDeviceForm
        If cameras.ShowDialog = DialogResult.OK Then
            CAMERA = cameras.VideoDevice
            AddHandler CAMERA.NewFrame, New NewFrameEventHandler(AddressOf Captured)
            CAMERA.Start()

            Button2.Visible = True



        End If
    End Sub
    Private Sub Captured(sender As Object, eventArgs As NewFrameEventArgs)
        bmp = DirectCast(eventArgs.Frame.Clone(), Bitmap)
        PictureBox1.Image = DirectCast(eventArgs.Frame.Clone(), Bitmap)

    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs)

    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        PictureBoxUserReg.Image = PictureBox1.Image

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Label18.Text = TextBox1.Text
        Label23.Text = TextBox2.Text
        Label25.Text = TextBox3.Text
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrintf.Click

        If TBFullname.Text = "(none)" Then
            MessageBox.Show("INVALID" & vbCrLf & "Please add your name to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TextBox1.Text = Nothing Then
            MessageBox.Show("NAME field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TextBox2.Text = Nothing Then
            MessageBox.Show("RELATIONSHIP field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If TextBox3.Text = Nothing Then
            MessageBox.Show("CONTACT NO. field is empty." & vbCrLf & "Please fill in the name field to continue.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If


        Me.BackColor = Color.White
        tbSearch.Visible = False
        btnSearch.Visible = False
        DGVUserData.Visible = False
        Panel2.Visible = False
        Panel3.Visible = False
        PictureBox1.Visible = False
        Button2.Visible = False
        Label33.Visible = False
        Label34.Visible = False
        Label35.Visible = False
        Label36.Visible = False
        TextBox1.Visible = False
        TextBox2.Visible = False
        TextBox3.Visible = False
        Button3.Visible = False
        Label16.Visible = False
        Label17.Visible = False
        btnPrintf.Visible = False


        PrintForm1.PrintAction = Printing.PrintAction.PrintToPreview
        PrintForm1.Print()

        Panel1.Visible = False
        Panel2.Visible = True
        btnPrintf.Visible = False
        printBack.Visible = True





    End Sub

    Private Sub printBack_Click(sender As Object, e As EventArgs) Handles printBack.Click
        printBack.Visible = False

        PrintForm1.PrintAction = Printing.PrintAction.PrintToPreview
        PrintForm1.Print()

        Panel2.Visible = False
        Label37.Visible = True
        Label38.Visible = True
        Me.BackColor = Color.WhiteSmoke


    End Sub
End Class