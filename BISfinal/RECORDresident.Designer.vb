﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class RECORDresident
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.tbS = New System.Windows.Forms.TextBox()
        Me.CBsb = New System.Windows.Forms.ComboBox()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.btnList = New System.Windows.Forms.Button()
        Me.DGVUserData = New System.Windows.Forms.DataGridView()
        Me.bFullname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bLname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bFname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bMname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bSuffix = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bBdate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bAge = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bGender = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bAddress = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bBplace = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bProvince = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bContact = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bStatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bBtype = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bCitizenship = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bReligion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Image = New System.Windows.Forms.DataGridViewImageColumn()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TBFullname = New System.Windows.Forms.TextBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.tbID = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CBGender = New System.Windows.Forms.ComboBox()
        Me.TBAge = New System.Windows.Forms.TextBox()
        Me.dtBday = New System.Windows.Forms.DateTimePicker()
        Me.TBSuffix = New System.Windows.Forms.TextBox()
        Me.TBMname = New System.Windows.Forms.TextBox()
        Me.TBFname = New System.Windows.Forms.TextBox()
        Me.TBLname = New System.Windows.Forms.TextBox()
        Me.CBStatus = New System.Windows.Forms.ComboBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TBContact = New System.Windows.Forms.TextBox()
        Me.TBProvince = New System.Windows.Forms.TextBox()
        Me.TBBplace = New System.Windows.Forms.TextBox()
        Me.TBAddress = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TBCitizenship = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.CBBtype = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TBReligion = New System.Windows.Forms.TextBox()
        Me.btnSend = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.TBoccupation = New System.Windows.Forms.TextBox()
        Me.CBvoter = New System.Windows.Forms.ComboBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxUserReg = New System.Windows.Forms.PictureBox()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        CType(Me.DGVUserData, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxUserReg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.BackColor = System.Drawing.Color.Transparent
        Me.lblTotal.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblTotal.Location = New System.Drawing.Point(511, 21)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(14, 19)
        Me.lblTotal.TabIndex = 2
        Me.lblTotal.Text = ":"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkSlateGray
        Me.Button2.FlatAppearance.BorderSize = 0
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(479, 156)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(69, 27)
        Me.Button2.TabIndex = 68
        Me.Button2.Text = "Update"
        Me.Button2.UseVisualStyleBackColor = False
        Me.Button2.Visible = False
        '
        'tbS
        '
        Me.tbS.BackColor = System.Drawing.Color.White
        Me.tbS.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbS.Location = New System.Drawing.Point(132, 14)
        Me.tbS.Name = "tbS"
        Me.tbS.Size = New System.Drawing.Size(296, 32)
        Me.tbS.TabIndex = 70
        Me.tbS.Visible = False
        '
        'CBsb
        '
        Me.CBsb.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBsb.FormattingEnabled = True
        Me.CBsb.Items.AddRange(New Object() {"Fullname", "ID", "Lastname", "Age", "Gender", "Address", "Province", "Status", "Citizenship", "Religion"})
        Me.CBsb.Location = New System.Drawing.Point(9, 14)
        Me.CBsb.Name = "CBsb"
        Me.CBsb.Size = New System.Drawing.Size(117, 31)
        Me.CBsb.TabIndex = 71
        Me.CBsb.Text = "Search by:"
        '
        'btnRefresh
        '
        Me.btnRefresh.BackColor = System.Drawing.Color.Transparent
        Me.btnRefresh.BackgroundImage = Global.BISfinal.My.Resources.Resources.refresh__1_
        Me.btnRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnRefresh.FlatAppearance.BorderSize = 0
        Me.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRefresh.Location = New System.Drawing.Point(825, 15)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(30, 33)
        Me.btnRefresh.TabIndex = 75
        Me.btnRefresh.UseVisualStyleBackColor = False
        '
        'btnList
        '
        Me.btnList.BackColor = System.Drawing.Color.DarkSlateGray
        Me.btnList.FlatAppearance.BorderSize = 0
        Me.btnList.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnList.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnList.ForeColor = System.Drawing.Color.White
        Me.btnList.Location = New System.Drawing.Point(565, 204)
        Me.btnList.Name = "btnList"
        Me.btnList.Size = New System.Drawing.Size(69, 27)
        Me.btnList.TabIndex = 76
        Me.btnList.Text = "List"
        Me.btnList.UseVisualStyleBackColor = False
        Me.btnList.Visible = False
        '
        'DGVUserData
        '
        Me.DGVUserData.AllowUserToAddRows = False
        Me.DGVUserData.AllowUserToDeleteRows = False
        Me.DGVUserData.AllowUserToResizeColumns = False
        Me.DGVUserData.AllowUserToResizeRows = False
        Me.DGVUserData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DGVUserData.BackgroundColor = System.Drawing.Color.White
        Me.DGVUserData.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DGVUserData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.DGVUserData.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Teal
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.SteelBlue
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVUserData.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DGVUserData.ColumnHeadersHeight = 40
        Me.DGVUserData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.DGVUserData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.bFullname, Me.bID, Me.bLname, Me.bFname, Me.bMname, Me.bSuffix, Me.bBdate, Me.bAge, Me.bGender, Me.bAddress, Me.bBplace, Me.bProvince, Me.bContact, Me.bStatus, Me.bBtype, Me.bCitizenship, Me.bReligion, Me.Image})
        Me.DGVUserData.Cursor = System.Windows.Forms.Cursors.Hand
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.MediumAquamarine
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGVUserData.DefaultCellStyle = DataGridViewCellStyle2
        Me.DGVUserData.EnableHeadersVisualStyles = False
        Me.DGVUserData.GridColor = System.Drawing.Color.DimGray
        Me.DGVUserData.Location = New System.Drawing.Point(-1, 57)
        Me.DGVUserData.MultiSelect = False
        Me.DGVUserData.Name = "DGVUserData"
        Me.DGVUserData.ReadOnly = True
        Me.DGVUserData.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(52, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(52, Byte), Integer))
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVUserData.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DGVUserData.RowHeadersVisible = False
        Me.DGVUserData.RowHeadersWidth = 50
        Me.DGVUserData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGVUserData.Size = New System.Drawing.Size(861, 598)
        Me.DGVUserData.TabIndex = 1
        '
        'bFullname
        '
        Me.bFullname.HeaderText = "Fullname"
        Me.bFullname.Name = "bFullname"
        Me.bFullname.ReadOnly = True
        Me.bFullname.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.bFullname.Width = 81
        '
        'bID
        '
        Me.bID.HeaderText = "ID"
        Me.bID.Name = "bID"
        Me.bID.ReadOnly = True
        Me.bID.Width = 43
        '
        'bLname
        '
        Me.bLname.HeaderText = "Lastname"
        Me.bLname.Name = "bLname"
        Me.bLname.ReadOnly = True
        Me.bLname.Width = 83
        '
        'bFname
        '
        Me.bFname.HeaderText = "First Name"
        Me.bFname.Name = "bFname"
        Me.bFname.ReadOnly = True
        Me.bFname.Width = 84
        '
        'bMname
        '
        Me.bMname.HeaderText = "Middle Name"
        Me.bMname.Name = "bMname"
        Me.bMname.ReadOnly = True
        Me.bMname.Width = 93
        '
        'bSuffix
        '
        Me.bSuffix.HeaderText = "Suffix"
        Me.bSuffix.Name = "bSuffix"
        Me.bSuffix.ReadOnly = True
        Me.bSuffix.Width = 63
        '
        'bBdate
        '
        Me.bBdate.HeaderText = "Birth Date"
        Me.bBdate.Name = "bBdate"
        Me.bBdate.ReadOnly = True
        Me.bBdate.Width = 81
        '
        'bAge
        '
        Me.bAge.HeaderText = "Age"
        Me.bAge.Name = "bAge"
        Me.bAge.ReadOnly = True
        Me.bAge.Width = 52
        '
        'bGender
        '
        Me.bGender.HeaderText = "Gender"
        Me.bGender.Name = "bGender"
        Me.bGender.ReadOnly = True
        Me.bGender.Width = 70
        '
        'bAddress
        '
        Me.bAddress.HeaderText = "Address"
        Me.bAddress.Name = "bAddress"
        Me.bAddress.ReadOnly = True
        Me.bAddress.Width = 75
        '
        'bBplace
        '
        Me.bBplace.HeaderText = "Birth Place"
        Me.bBplace.Name = "bBplace"
        Me.bBplace.ReadOnly = True
        Me.bBplace.Width = 85
        '
        'bProvince
        '
        Me.bProvince.HeaderText = "Province"
        Me.bProvince.Name = "bProvince"
        Me.bProvince.ReadOnly = True
        Me.bProvince.Width = 78
        '
        'bContact
        '
        Me.bContact.HeaderText = "Contact No."
        Me.bContact.Name = "bContact"
        Me.bContact.ReadOnly = True
        Me.bContact.Width = 85
        '
        'bStatus
        '
        Me.bStatus.HeaderText = "Status"
        Me.bStatus.Name = "bStatus"
        Me.bStatus.ReadOnly = True
        Me.bStatus.Width = 65
        '
        'bBtype
        '
        Me.bBtype.HeaderText = "Blood Type"
        Me.bBtype.Name = "bBtype"
        Me.bBtype.ReadOnly = True
        Me.bBtype.Width = 82
        '
        'bCitizenship
        '
        Me.bCitizenship.HeaderText = "Citizenship"
        Me.bCitizenship.Name = "bCitizenship"
        Me.bCitizenship.ReadOnly = True
        Me.bCitizenship.Width = 93
        '
        'bReligion
        '
        Me.bReligion.HeaderText = "Religion"
        Me.bReligion.Name = "bReligion"
        Me.bReligion.ReadOnly = True
        Me.bReligion.Width = 77
        '
        'Image
        '
        Me.Image.HeaderText = "Image"
        Me.Image.Name = "Image"
        Me.Image.ReadOnly = True
        Me.Image.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Image.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Image.Width = 63
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.Black
        Me.Label17.Location = New System.Drawing.Point(10, 459)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(67, 15)
        Me.Label17.TabIndex = 100
        Me.Label17.Text = "Full Name:"
        Me.Label17.Visible = False
        '
        'TBFullname
        '
        Me.TBFullname.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TBFullname.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBFullname.ForeColor = System.Drawing.Color.White
        Me.TBFullname.Location = New System.Drawing.Point(83, 456)
        Me.TBFullname.Name = "TBFullname"
        Me.TBFullname.Size = New System.Drawing.Size(134, 22)
        Me.TBFullname.TabIndex = 99
        Me.TBFullname.Visible = False
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {"LastName"})
        Me.ComboBox1.Location = New System.Drawing.Point(423, 433)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(46, 21)
        Me.ComboBox1.TabIndex = 98
        Me.ComboBox1.Text = "LastName"
        Me.ComboBox1.Visible = False
        '
        'tbID
        '
        Me.tbID.AutoSize = True
        Me.tbID.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbID.ForeColor = System.Drawing.Color.Black
        Me.tbID.Location = New System.Drawing.Point(83, 482)
        Me.tbID.Name = "tbID"
        Me.tbID.Size = New System.Drawing.Size(57, 19)
        Me.tbID.TabIndex = 97
        Me.tbID.Text = "000000"
        Me.tbID.Visible = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Black
        Me.Label16.Location = New System.Drawing.Point(53, 484)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(24, 15)
        Me.Label16.TabIndex = 96
        Me.Label16.Text = "ID:"
        Me.Label16.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(266, 488)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(51, 15)
        Me.Label7.TabIndex = 95
        Me.Label7.Text = "Gender:"
        Me.Label7.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(284, 460)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(33, 15)
        Me.Label6.TabIndex = 94
        Me.Label6.Text = "Age:"
        Me.Label6.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(248, 435)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(69, 15)
        Me.Label5.TabIndex = 93
        Me.Label5.Text = "Birth Date:"
        Me.Label5.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(273, 404)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(44, 15)
        Me.Label4.TabIndex = 92
        Me.Label4.Text = "Suffix:"
        Me.Label4.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(235, 376)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(82, 15)
        Me.Label3.TabIndex = 91
        Me.Label3.Text = "Middle Name:"
        Me.Label3.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(245, 348)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 15)
        Me.Label2.TabIndex = 90
        Me.Label2.Text = "First Name:"
        Me.Label2.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(248, 320)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 15)
        Me.Label1.TabIndex = 89
        Me.Label1.Text = "Last Name:"
        Me.Label1.Visible = False
        '
        'CBGender
        '
        Me.CBGender.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.CBGender.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBGender.ForeColor = System.Drawing.Color.White
        Me.CBGender.FormattingEnabled = True
        Me.CBGender.Items.AddRange(New Object() {"Male", "Female"})
        Me.CBGender.Location = New System.Drawing.Point(323, 485)
        Me.CBGender.Name = "CBGender"
        Me.CBGender.Size = New System.Drawing.Size(76, 23)
        Me.CBGender.TabIndex = 88
        Me.CBGender.Visible = False
        '
        'TBAge
        '
        Me.TBAge.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TBAge.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBAge.ForeColor = System.Drawing.Color.White
        Me.TBAge.Location = New System.Drawing.Point(323, 457)
        Me.TBAge.Name = "TBAge"
        Me.TBAge.Size = New System.Drawing.Size(76, 22)
        Me.TBAge.TabIndex = 87
        Me.TBAge.Visible = False
        '
        'dtBday
        '
        Me.dtBday.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtBday.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtBday.Location = New System.Drawing.Point(323, 429)
        Me.dtBday.Name = "dtBday"
        Me.dtBday.Size = New System.Drawing.Size(76, 22)
        Me.dtBday.TabIndex = 86
        Me.dtBday.Visible = False
        '
        'TBSuffix
        '
        Me.TBSuffix.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TBSuffix.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBSuffix.ForeColor = System.Drawing.Color.White
        Me.TBSuffix.Location = New System.Drawing.Point(323, 401)
        Me.TBSuffix.Name = "TBSuffix"
        Me.TBSuffix.Size = New System.Drawing.Size(76, 22)
        Me.TBSuffix.TabIndex = 85
        Me.TBSuffix.Visible = False
        '
        'TBMname
        '
        Me.TBMname.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TBMname.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBMname.ForeColor = System.Drawing.Color.White
        Me.TBMname.Location = New System.Drawing.Point(323, 373)
        Me.TBMname.Name = "TBMname"
        Me.TBMname.Size = New System.Drawing.Size(134, 22)
        Me.TBMname.TabIndex = 84
        Me.TBMname.Visible = False
        '
        'TBFname
        '
        Me.TBFname.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TBFname.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBFname.ForeColor = System.Drawing.Color.White
        Me.TBFname.Location = New System.Drawing.Point(323, 345)
        Me.TBFname.Name = "TBFname"
        Me.TBFname.Size = New System.Drawing.Size(134, 22)
        Me.TBFname.TabIndex = 83
        Me.TBFname.Visible = False
        '
        'TBLname
        '
        Me.TBLname.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TBLname.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBLname.ForeColor = System.Drawing.Color.White
        Me.TBLname.Location = New System.Drawing.Point(323, 317)
        Me.TBLname.Name = "TBLname"
        Me.TBLname.Size = New System.Drawing.Size(134, 22)
        Me.TBLname.TabIndex = 82
        Me.TBLname.Visible = False
        '
        'CBStatus
        '
        Me.CBStatus.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.CBStatus.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBStatus.ForeColor = System.Drawing.Color.White
        Me.CBStatus.FormattingEnabled = True
        Me.CBStatus.Items.AddRange(New Object() {"Single", "Married", "Divorse"})
        Me.CBStatus.Location = New System.Drawing.Point(541, 429)
        Me.CBStatus.Name = "CBStatus"
        Me.CBStatus.Size = New System.Drawing.Size(77, 23)
        Me.CBStatus.TabIndex = 111
        Me.CBStatus.Visible = False
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Black
        Me.Label15.Location = New System.Drawing.Point(489, 433)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(46, 15)
        Me.Label15.TabIndex = 110
        Me.Label15.Text = "Status:"
        Me.Label15.Visible = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Black
        Me.Label11.Location = New System.Drawing.Point(461, 404)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(74, 15)
        Me.Label11.TabIndex = 109
        Me.Label11.Text = "Contact No.:"
        Me.Label11.Visible = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Black
        Me.Label10.Location = New System.Drawing.Point(476, 376)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(59, 15)
        Me.Label10.TabIndex = 108
        Me.Label10.Text = "Province:"
        Me.Label10.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Black
        Me.Label9.Location = New System.Drawing.Point(461, 348)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(74, 15)
        Me.Label9.TabIndex = 107
        Me.Label9.Text = "Birth Place:"
        Me.Label9.Visible = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(479, 320)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(56, 15)
        Me.Label8.TabIndex = 106
        Me.Label8.Text = "Address:"
        Me.Label8.Visible = False
        '
        'TBContact
        '
        Me.TBContact.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TBContact.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBContact.ForeColor = System.Drawing.Color.White
        Me.TBContact.Location = New System.Drawing.Point(541, 401)
        Me.TBContact.Name = "TBContact"
        Me.TBContact.Size = New System.Drawing.Size(134, 22)
        Me.TBContact.TabIndex = 105
        Me.TBContact.Visible = False
        '
        'TBProvince
        '
        Me.TBProvince.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TBProvince.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBProvince.ForeColor = System.Drawing.Color.White
        Me.TBProvince.Location = New System.Drawing.Point(541, 373)
        Me.TBProvince.Name = "TBProvince"
        Me.TBProvince.Size = New System.Drawing.Size(134, 22)
        Me.TBProvince.TabIndex = 104
        Me.TBProvince.Visible = False
        '
        'TBBplace
        '
        Me.TBBplace.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TBBplace.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBBplace.ForeColor = System.Drawing.Color.White
        Me.TBBplace.Location = New System.Drawing.Point(541, 345)
        Me.TBBplace.Name = "TBBplace"
        Me.TBBplace.Size = New System.Drawing.Size(134, 22)
        Me.TBBplace.TabIndex = 103
        Me.TBBplace.Visible = False
        '
        'TBAddress
        '
        Me.TBAddress.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TBAddress.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBAddress.ForeColor = System.Drawing.Color.White
        Me.TBAddress.Location = New System.Drawing.Point(541, 317)
        Me.TBAddress.Name = "TBAddress"
        Me.TBAddress.Size = New System.Drawing.Size(134, 22)
        Me.TBAddress.TabIndex = 102
        Me.TBAddress.Visible = False
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.Black
        Me.Label18.Location = New System.Drawing.Point(461, 488)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(74, 15)
        Me.Label18.TabIndex = 115
        Me.Label18.Text = "Citizenship:"
        Me.Label18.Visible = False
        '
        'TBCitizenship
        '
        Me.TBCitizenship.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TBCitizenship.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBCitizenship.ForeColor = System.Drawing.Color.White
        Me.TBCitizenship.Location = New System.Drawing.Point(541, 485)
        Me.TBCitizenship.Name = "TBCitizenship"
        Me.TBCitizenship.Size = New System.Drawing.Size(134, 22)
        Me.TBCitizenship.TabIndex = 114
        Me.TBCitizenship.Visible = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Black
        Me.Label12.Location = New System.Drawing.Point(465, 459)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(70, 15)
        Me.Label12.TabIndex = 113
        Me.Label12.Text = "Blood Type:"
        Me.Label12.Visible = False
        '
        'CBBtype
        '
        Me.CBBtype.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.CBBtype.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBBtype.ForeColor = System.Drawing.Color.White
        Me.CBBtype.FormattingEnabled = True
        Me.CBBtype.Items.AddRange(New Object() {"A", "B", "O", "A+", "B+", "O+"})
        Me.CBBtype.Location = New System.Drawing.Point(541, 456)
        Me.CBBtype.Name = "CBBtype"
        Me.CBBtype.Size = New System.Drawing.Size(76, 23)
        Me.CBBtype.TabIndex = 112
        Me.CBBtype.Visible = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Black
        Me.Label14.Location = New System.Drawing.Point(651, 436)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(58, 15)
        Me.Label14.TabIndex = 117
        Me.Label14.Text = "Religion:"
        Me.Label14.Visible = False
        '
        'TBReligion
        '
        Me.TBReligion.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TBReligion.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBReligion.ForeColor = System.Drawing.Color.White
        Me.TBReligion.Location = New System.Drawing.Point(715, 433)
        Me.TBReligion.Name = "TBReligion"
        Me.TBReligion.Size = New System.Drawing.Size(134, 22)
        Me.TBReligion.TabIndex = 116
        Me.TBReligion.Visible = False
        '
        'btnSend
        '
        Me.btnSend.BackColor = System.Drawing.Color.DarkSlateGray
        Me.btnSend.FlatAppearance.BorderSize = 0
        Me.btnSend.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSend.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSend.ForeColor = System.Drawing.Color.White
        Me.btnSend.Location = New System.Drawing.Point(715, 369)
        Me.btnSend.Name = "btnSend"
        Me.btnSend.Size = New System.Drawing.Size(124, 27)
        Me.btnSend.TabIndex = 118
        Me.btnSend.Text = "pass"
        Me.btnSend.UseVisualStyleBackColor = False
        Me.btnSend.Visible = False
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(654, 121)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 121
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(343, 560)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(41, 15)
        Me.Label13.TabIndex = 129
        Me.Label13.Text = "Voter:"
        Me.Label13.Visible = False
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Black
        Me.Label19.Location = New System.Drawing.Point(346, 532)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(73, 15)
        Me.Label19.TabIndex = 128
        Me.Label19.Text = "Occupation:"
        Me.Label19.Visible = False
        '
        'TBoccupation
        '
        Me.TBoccupation.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TBoccupation.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBoccupation.ForeColor = System.Drawing.Color.White
        Me.TBoccupation.Location = New System.Drawing.Point(423, 529)
        Me.TBoccupation.Name = "TBoccupation"
        Me.TBoccupation.Size = New System.Drawing.Size(134, 22)
        Me.TBoccupation.TabIndex = 126
        Me.TBoccupation.Visible = False
        '
        'CBvoter
        '
        Me.CBvoter.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.CBvoter.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBvoter.ForeColor = System.Drawing.Color.White
        Me.CBvoter.FormattingEnabled = True
        Me.CBvoter.Items.AddRange(New Object() {"A", "B", "O", "A+", "B+", "O+"})
        Me.CBvoter.Location = New System.Drawing.Point(423, 557)
        Me.CBvoter.Name = "CBvoter"
        Me.CBvoter.Size = New System.Drawing.Size(76, 23)
        Me.CBvoter.TabIndex = 130
        Me.CBvoter.Visible = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = Global.BISfinal.My.Resources.Resources.pngaaa_com_25560
        Me.PictureBox1.Location = New System.Drawing.Point(400, 18)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(24, 24)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 120
        Me.PictureBox1.TabStop = False
        Me.PictureBox1.Visible = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = Global.BISfinal.My.Resources.Resources.pngegg__2_
        Me.PictureBox5.Location = New System.Drawing.Point(464, 14)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(37, 31)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox5.TabIndex = 119
        Me.PictureBox5.TabStop = False
        '
        'PictureBoxUserReg
        '
        Me.PictureBoxUserReg.BackColor = System.Drawing.Color.Transparent
        Me.PictureBoxUserReg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxUserReg.Location = New System.Drawing.Point(12, 317)
        Me.PictureBoxUserReg.Name = "PictureBoxUserReg"
        Me.PictureBoxUserReg.Size = New System.Drawing.Size(190, 127)
        Me.PictureBoxUserReg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBoxUserReg.TabIndex = 101
        Me.PictureBoxUserReg.TabStop = False
        Me.PictureBoxUserReg.Visible = False
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.Transparent
        Me.btnDelete.BackgroundImage = Global.BISfinal.My.Resources.Resources.trash
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.White
        Me.btnDelete.Location = New System.Drawing.Point(785, 15)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(30, 33)
        Me.btnDelete.TabIndex = 77
        Me.btnDelete.UseVisualStyleBackColor = False
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.Transparent
        Me.btnAdd.BackgroundImage = Global.BISfinal.My.Resources.Resources.jing_fm_asterisk_clipart_679204
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.White
        Me.btnAdd.Location = New System.Drawing.Point(744, 16)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(30, 33)
        Me.btnAdd.TabIndex = 0
        Me.btnAdd.UseVisualStyleBackColor = False
        '
        'RECORDresident
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(861, 655)
        Me.Controls.Add(Me.CBvoter)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.TBoccupation)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.PictureBox5)
        Me.Controls.Add(Me.btnSend)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.TBReligion)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.TBCitizenship)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.CBBtype)
        Me.Controls.Add(Me.CBStatus)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.TBContact)
        Me.Controls.Add(Me.TBProvince)
        Me.Controls.Add(Me.TBBplace)
        Me.Controls.Add(Me.TBAddress)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.TBFullname)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.tbID)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CBGender)
        Me.Controls.Add(Me.TBAge)
        Me.Controls.Add(Me.dtBday)
        Me.Controls.Add(Me.TBSuffix)
        Me.Controls.Add(Me.TBMname)
        Me.Controls.Add(Me.TBFname)
        Me.Controls.Add(Me.TBLname)
        Me.Controls.Add(Me.PictureBoxUserReg)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnList)
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.CBsb)
        Me.Controls.Add(Me.tbS)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.DGVUserData)
        Me.Controls.Add(Me.btnAdd)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "RECORDresident"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "resView"
        CType(Me.DGVUserData, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxUserReg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnAdd As Button
    Friend WithEvents lblTotal As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents tbS As TextBox
    Friend WithEvents CBsb As ComboBox
    Friend WithEvents ButtonTimer As Timer
    Friend WithEvents btnRefresh As Button
    Friend WithEvents btnList As Button
    Friend WithEvents btnDelete As Button
    Friend WithEvents Label17 As Label
    Friend WithEvents TBFullname As TextBox
    Friend WithEvents ComboBox1 As ComboBox
    Friend WithEvents tbID As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents CBGender As ComboBox
    Friend WithEvents TBAge As TextBox
    Friend WithEvents dtBday As DateTimePicker
    Friend WithEvents TBSuffix As TextBox
    Friend WithEvents TBMname As TextBox
    Friend WithEvents TBFname As TextBox
    Friend WithEvents TBLname As TextBox
    Friend WithEvents PictureBoxUserReg As PictureBox
    Friend WithEvents CBStatus As ComboBox
    Friend WithEvents Label15 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents TBContact As TextBox
    Friend WithEvents TBProvince As TextBox
    Friend WithEvents TBBplace As TextBox
    Friend WithEvents TBAddress As TextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents TBCitizenship As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents CBBtype As ComboBox
    Friend WithEvents Label14 As Label
    Friend WithEvents TBReligion As TextBox
    Friend WithEvents btnSend As Button
    Friend WithEvents bFullname As DataGridViewTextBoxColumn
    Friend WithEvents bID As DataGridViewTextBoxColumn
    Friend WithEvents bLname As DataGridViewTextBoxColumn
    Friend WithEvents bFname As DataGridViewTextBoxColumn
    Friend WithEvents bMname As DataGridViewTextBoxColumn
    Friend WithEvents bSuffix As DataGridViewTextBoxColumn
    Friend WithEvents bBdate As DataGridViewTextBoxColumn
    Friend WithEvents bAge As DataGridViewTextBoxColumn
    Friend WithEvents bGender As DataGridViewTextBoxColumn
    Friend WithEvents bAddress As DataGridViewTextBoxColumn
    Friend WithEvents bBplace As DataGridViewTextBoxColumn
    Friend WithEvents bProvince As DataGridViewTextBoxColumn
    Friend WithEvents bContact As DataGridViewTextBoxColumn
    Friend WithEvents bStatus As DataGridViewTextBoxColumn
    Friend WithEvents bBtype As DataGridViewTextBoxColumn
    Friend WithEvents bCitizenship As DataGridViewTextBoxColumn
    Friend WithEvents bReligion As DataGridViewTextBoxColumn
    Friend WithEvents Image As DataGridViewImageColumn
    Friend WithEvents DGVUserData As DataGridView
    Friend WithEvents PictureBox5 As PictureBox
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents Button1 As Button
    Friend WithEvents Label13 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents TBoccupation As TextBox
    Friend WithEvents CBvoter As ComboBox
End Class
