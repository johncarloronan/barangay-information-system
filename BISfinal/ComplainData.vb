﻿Public Class ComplainData
    Public Property eCasenumber() As String = ""
    Public Property eDate() As String = ""
    Public Property eTime() As String = ""
    Public Property eComplainant() As String = ""
    Public Property eAddress() As String = ""
    Public Property eIncident() As String = ""
    Public Property eDetails() As String = ""
    Public Property eAction() As String = ""
    Public Property eRemarks() As String = ""
    Public Property eEncoder() As String = ""

End Class
