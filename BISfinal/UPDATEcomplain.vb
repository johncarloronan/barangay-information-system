﻿Imports FireSharp.Config
Imports FireSharp.Response
Imports FireSharp.Interfaces
'-------------------------------------------

Imports System.Web.Script.Serialization '(Importing System.Web.Script.Serialization) This is used to read and convert JSON strings.
Imports System.ComponentModel '(Importing System.ComponentModel) This is used to sort data on DataGridView.
Imports System.IO

Public Class UPDATEcomplain

    Public Property UPDATEBLOTTERnum As String
    Public Property UPDATEBLOTTERdate As String
    Public Property UPDATEBLOTTERtime As String
    Public Property UPDATEBLOTTERcomplainant As String
    Public Property UPDATEBLOTTERaddress As String
    Public Property UPDATEBLOTTERincident As String
    Public Property UPDATEBLOTTERdetails As String
    Public Property UPDATEBLOTTERaction As String
    Public Property UPDATEBLOTTERremarks As String

    Dim clearDGVCol As Boolean = True

    Private dtTableGrd As DataTable


    Private fcon As New FirebaseConfig() With
        {
        .AuthSecret = "8kPwBHDkDdZWGHts9sb98dsJu45thBvgDTUrgqJj",
        .BasePath = "https://bisrealtime-default-rtdb.firebaseio.com/"
        }

    Private client As IFirebaseClient


    Sub ShowRecord()
        Try
            Dim dtTable As New DataTable
            dtTable.Columns.Add("Case no.")
            dtTable.Columns.Add("Date")
            dtTable.Columns.Add("Time")
            dtTable.Columns.Add("Complainant")
            dtTable.Columns.Add("Address")
            dtTable.Columns.Add("Incident")
            dtTable.Columns.Add("Details")
            dtTable.Columns.Add("Action")
            dtTable.Columns.Add("Remarks")



            If clearDGVCol = True Then

                clearDGVCol = False
            End If


            Dim SRRecord = client.Get("BlotterDB/")


            Dim myJsonTool As New JavaScriptSerializer
            Dim myDeserializedItems = myJsonTool.Deserialize(Of Dictionary(Of String, ComplainData))(SRRecord.Body)


            For Each dictItem As KeyValuePair(Of String, ComplainData) In myDeserializedItems
                dtTable.Rows.Add(dictItem.Value.eCasenumber, dictItem.Value.eDate, dictItem.Value.eTime, dictItem.Value.eComplainant, dictItem.Value.eAddress, dictItem.Value.eDetails, dictItem.Value.eAction, dictItem.Value.eRemarks, dictItem.Value.eEncoder)
            Next


            dtTableGrd = dtTable



        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ElseIf ex.Message = "Object reference not set to an instance of an object." Then
                Dim dtTable As New DataTable
                dtTable.Columns.Add("Case no.")
                dtTable.Columns.Add("Date")
                dtTable.Columns.Add("Time")
                dtTable.Columns.Add("Complainant")
                dtTable.Columns.Add("Address")
                dtTable.Columns.Add("Incident")
                dtTable.Columns.Add("Details")
                dtTable.Columns.Add("Action")
                dtTable.Columns.Add("Remarks")
                dtTable.Columns.Add("Encoder")
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If



        End Try

    End Sub


    Private Sub UPDATEcomplain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            client = New FireSharp.FirebaseClient(fcon)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try



        caseNum.Text = UPDATEBLOTTERnum
        caseDate.Text = UPDATEBLOTTERdate
        caseTime.Text = UPDATEBLOTTERtime
        TBComplainant.Text = UPDATEBLOTTERcomplainant
        TBaddress.Text = UPDATEBLOTTERaddress
        TBincident.Text = UPDATEBLOTTERincident
        TBdetails.Text = UPDATEBLOTTERdetails
        TBactiontaken.Text = UPDATEBLOTTERaction
        CBremarks.Text = UPDATEBLOTTERremarks

    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub btnUpdate_Click_1(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Try
            Dim CD As New ComplainData() With
                {
                .eCasenumber = caseNum.Text,
                .eDate = caseDate.Text,
                .eTime = caseTime.Text,
                .eComplainant = TBComplainant.Text,
                .eAddress = TBaddress.Text,
                .eIncident = TBincident.Text,
                .eDetails = TBdetails.Text,
                .eAction = TBactiontaken.Text,
                .eRemarks = CBremarks.Text,
                .eEncoder = Eadmin.Text
                }

            Dim update = client.Update("BlotterDB/" + caseNum.Text, CD) 'To update data to Firebase Database.

            MessageBox.Show("Updated successfully.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            caseNum.Text = ""
            caseDate.Text = ""
            caseTime.Text = ""
            TBComplainant.Text = ""
            TBaddress.Text = ""
            TBincident.Text = ""
            TBdetails.Text = ""
            TBactiontaken.Text = ""
            CBremarks.Text = ""
            Me.Close()

        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If


        End Try

    End Sub
End Class