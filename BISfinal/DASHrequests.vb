﻿Imports FireSharp.Config
Imports FireSharp.Response
Imports FireSharp.Interfaces
'-------------------------------------------

Imports System.Web.Script.Serialization '(Importing System.Web.Script.Serialization) This is used to read and convert JSON strings.
Imports System.ComponentModel '(Importing System.ComponentModel) This is used to sort data on DataGridView.
Imports System.IO

Public Class DASHrequests
    Dim clearDGVCol As Boolean = True

    Private dtTableGrd As DataTable


    Private fcon As New FirebaseConfig() With
        {
        .AuthSecret = "8kPwBHDkDdZWGHts9sb98dsJu45thBvgDTUrgqJj",
        .BasePath = "https://bisrealtime-default-rtdb.firebaseio.com/"
        }

    Private client As IFirebaseClient


    Sub ShowRecord()
        Try
            Dim dtTable As New DataTable
            dtTable.Columns.Add("Time")
            dtTable.Columns.Add("Requested")
            dtTable.Columns.Add("Name")
            dtTable.Columns.Add("Number")



            If clearDGVCol = True Then
                DGVUserData.Columns.Clear()
                clearDGVCol = False
            End If


            Dim SRRecord = client.Get("RequestsDB/")


            Dim myJsonTool As New JavaScriptSerializer
            Dim myDeserializedItems = myJsonTool.Deserialize(Of Dictionary(Of String, DATArequests))(SRRecord.Body)


            For Each dictItem As KeyValuePair(Of String, DATArequests) In myDeserializedItems
                dtTable.Rows.Add(dictItem.Value.gTime, dictItem.Value.gRequested, dictItem.Value.gName, dictItem.Value.gNum)
            Next

            DGVUserData.DataSource = dtTable
            dtTableGrd = dtTable

            DGVUserData.Sort(DGVUserData.Columns(0), ListSortDirection.Descending)

            lbltotal.Text = "" & (DGVUserData.RowCount)

            DGVUserData.ClearSelection()

            noReq.Visible = False
            PictureBox2.Image = My.Resources.notification


        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ElseIf ex.Message = "Object reference not set to an instance of an object." Then
                Dim dtTable As New DataTable
                dtTable.Columns.Add("Time")
                dtTable.Columns.Add("Requested")
                dtTable.Columns.Add("Name")
                dtTable.Columns.Add("Number")
                DGVUserData.DataSource = dtTable
                lbltotal.Text = "" & (DGVUserData.RowCount)
                noReq.Visible = True
                PictureBox2.Image = My.Resources.nonotif
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If



        End Try
    End Sub
    Private Sub RECORDrequests_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            client = New FireSharp.FirebaseClient(fcon)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Dim clearDGVCol As Boolean = True
        Button1_Click(sender, e)



        Timer1.Interval = 30000
        Timer1.Enabled = True
    End Sub

    Private Sub DGVUserData_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVUserData.CellContentClick
        lblPurpose.Text = DGVUserData.SelectedRows(0).Cells("Requested").Value

        If lblPurpose.Text = "Clearance" Then
            TBname.Text = DGVUserData.SelectedRows(0).Cells("Name").Value
            PassClearance_Click(sender, e)
        ElseIf lblPurpose.Text = "Cedula" Then
            TBname.Text = DGVUserData.SelectedRows(0).Cells("Name").Value
            PassCedula_Click(sender, e)
        ElseIf lblPurpose.Text = "Blotter" Then
            TBname.Text = DGVUserData.SelectedRows(0).Cells("Name").Value
            PassBlotter_Click(sender, e)
        End If

        For Each row As DataGridViewRow In DGVUserData.SelectedRows
            If row.Selected = True Then
                Dim delete = client.Delete("RequestsDB/" & row.DataBoundItem(3).ToString)
                Button1_Click(sender, e)
            End If
        Next

    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ShowRecord()

    End Sub

    Private Sub DGVUserData_DoubleClick(sender As Object, e As EventArgs) Handles DGVUserData.DoubleClick




    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Button1_Click(sender, e)
    End Sub

    Private Sub PassClearance_Click(sender As Object, e As EventArgs) Handles PassClearance.Click
        Dim aaclearance As New clearance

        aaclearance.CLEARANCEname = TBname.Text
        aaclearance.Show()
    End Sub

    Private Sub DGVUserData_Click(sender As Object, e As EventArgs) Handles DGVUserData.Click

    End Sub
    Private Sub PassCedula_Click(sender As Object, e As EventArgs) Handles PassCedula.Click
        Dim aacedula As New PERMITcedula

        aacedula.CEDULAname = TBname.Text
        aacedula.Show()
    End Sub

    Private Sub PassBlotter_Click(sender As Object, e As EventArgs) Handles PassBlotter.Click
        Dim aablotter As New Complain

        aablotter.BLOTTERname = TBname.Text
        aablotter.Show()
    End Sub

    Private Sub DGVUserData_ColumnContextMenuStripChanged(sender As Object, e As DataGridViewColumnEventArgs) Handles DGVUserData.ColumnContextMenuStripChanged

    End Sub

    Private Sub lbltotal_Click(sender As Object, e As EventArgs) Handles lbltotal.Click

    End Sub
End Class