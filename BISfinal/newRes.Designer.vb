﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class newRes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.TBLname = New System.Windows.Forms.TextBox()
        Me.TBFname = New System.Windows.Forms.TextBox()
        Me.TBMname = New System.Windows.Forms.TextBox()
        Me.TBSuffix = New System.Windows.Forms.TextBox()
        Me.dtBday = New System.Windows.Forms.DateTimePicker()
        Me.TBAge = New System.Windows.Forms.TextBox()
        Me.CBGender = New System.Windows.Forms.ComboBox()
        Me.TBAddress = New System.Windows.Forms.TextBox()
        Me.TBBplace = New System.Windows.Forms.TextBox()
        Me.TBProvince = New System.Windows.Forms.TextBox()
        Me.TBContact = New System.Windows.Forms.TextBox()
        Me.CBBtype = New System.Windows.Forms.ComboBox()
        Me.TBReligion = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TBid = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TBFullname = New System.Windows.Forms.TextBox()
        Me.OpenFileDialogUserImage = New System.Windows.Forms.OpenFileDialog()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.CBStatus = New System.Windows.Forms.ComboBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TBCitizenship = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxUserReg = New System.Windows.Forms.PictureBox()
        Me.BtnSave = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.CBvoter = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.TBoccupation = New System.Windows.Forms.TextBox()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxUserReg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TBLname
        '
        Me.TBLname.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBLname.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBLname.ForeColor = System.Drawing.Color.Black
        Me.TBLname.Location = New System.Drawing.Point(109, 199)
        Me.TBLname.Name = "TBLname"
        Me.TBLname.Size = New System.Drawing.Size(134, 22)
        Me.TBLname.TabIndex = 0
        '
        'TBFname
        '
        Me.TBFname.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBFname.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBFname.ForeColor = System.Drawing.Color.Black
        Me.TBFname.Location = New System.Drawing.Point(109, 227)
        Me.TBFname.Name = "TBFname"
        Me.TBFname.Size = New System.Drawing.Size(134, 22)
        Me.TBFname.TabIndex = 1
        '
        'TBMname
        '
        Me.TBMname.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBMname.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBMname.ForeColor = System.Drawing.Color.Black
        Me.TBMname.Location = New System.Drawing.Point(109, 255)
        Me.TBMname.Name = "TBMname"
        Me.TBMname.Size = New System.Drawing.Size(134, 22)
        Me.TBMname.TabIndex = 2
        '
        'TBSuffix
        '
        Me.TBSuffix.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBSuffix.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBSuffix.ForeColor = System.Drawing.Color.Black
        Me.TBSuffix.Location = New System.Drawing.Point(109, 283)
        Me.TBSuffix.Name = "TBSuffix"
        Me.TBSuffix.Size = New System.Drawing.Size(134, 22)
        Me.TBSuffix.TabIndex = 3
        '
        'dtBday
        '
        Me.dtBday.CalendarForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.dtBday.CalendarMonthBackground = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dtBday.CalendarTitleBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dtBday.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtBday.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtBday.Location = New System.Drawing.Point(109, 311)
        Me.dtBday.Name = "dtBday"
        Me.dtBday.Size = New System.Drawing.Size(134, 22)
        Me.dtBday.TabIndex = 4
        '
        'TBAge
        '
        Me.TBAge.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBAge.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBAge.ForeColor = System.Drawing.Color.Black
        Me.TBAge.Location = New System.Drawing.Point(109, 339)
        Me.TBAge.Name = "TBAge"
        Me.TBAge.Size = New System.Drawing.Size(134, 22)
        Me.TBAge.TabIndex = 5
        '
        'CBGender
        '
        Me.CBGender.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CBGender.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBGender.ForeColor = System.Drawing.Color.Black
        Me.CBGender.FormattingEnabled = True
        Me.CBGender.Items.AddRange(New Object() {"Male", "Female"})
        Me.CBGender.Location = New System.Drawing.Point(109, 367)
        Me.CBGender.Name = "CBGender"
        Me.CBGender.Size = New System.Drawing.Size(134, 23)
        Me.CBGender.TabIndex = 6
        '
        'TBAddress
        '
        Me.TBAddress.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBAddress.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBAddress.ForeColor = System.Drawing.Color.Black
        Me.TBAddress.Location = New System.Drawing.Point(109, 396)
        Me.TBAddress.Name = "TBAddress"
        Me.TBAddress.Size = New System.Drawing.Size(134, 22)
        Me.TBAddress.TabIndex = 7
        '
        'TBBplace
        '
        Me.TBBplace.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBBplace.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBBplace.ForeColor = System.Drawing.Color.Black
        Me.TBBplace.Location = New System.Drawing.Point(109, 424)
        Me.TBBplace.Name = "TBBplace"
        Me.TBBplace.Size = New System.Drawing.Size(134, 22)
        Me.TBBplace.TabIndex = 8
        '
        'TBProvince
        '
        Me.TBProvince.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBProvince.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBProvince.ForeColor = System.Drawing.Color.Black
        Me.TBProvince.Location = New System.Drawing.Point(377, 244)
        Me.TBProvince.Name = "TBProvince"
        Me.TBProvince.Size = New System.Drawing.Size(134, 22)
        Me.TBProvince.TabIndex = 9
        '
        'TBContact
        '
        Me.TBContact.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBContact.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBContact.ForeColor = System.Drawing.Color.Black
        Me.TBContact.Location = New System.Drawing.Point(377, 271)
        Me.TBContact.Name = "TBContact"
        Me.TBContact.Size = New System.Drawing.Size(134, 22)
        Me.TBContact.TabIndex = 10
        '
        'CBBtype
        '
        Me.CBBtype.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CBBtype.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBBtype.ForeColor = System.Drawing.Color.Black
        Me.CBBtype.FormattingEnabled = True
        Me.CBBtype.Items.AddRange(New Object() {"A", "B", "O", "A+", "B+", "O+", "N/A"})
        Me.CBBtype.Location = New System.Drawing.Point(377, 325)
        Me.CBBtype.Name = "CBBtype"
        Me.CBBtype.Size = New System.Drawing.Size(134, 23)
        Me.CBBtype.TabIndex = 11
        '
        'TBReligion
        '
        Me.TBReligion.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBReligion.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBReligion.ForeColor = System.Drawing.Color.Black
        Me.TBReligion.Location = New System.Drawing.Point(377, 378)
        Me.TBReligion.Name = "TBReligion"
        Me.TBReligion.Size = New System.Drawing.Size(134, 22)
        Me.TBReligion.TabIndex = 12
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(13, 202)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 15)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Last Name:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(13, 230)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 15)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "First Name:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(13, 258)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(82, 15)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Middle Name:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(13, 286)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(44, 15)
        Me.Label4.TabIndex = 17
        Me.Label4.Text = "Suffix:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(13, 317)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(69, 15)
        Me.Label5.TabIndex = 18
        Me.Label5.Text = "Birth Date:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(13, 342)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(33, 15)
        Me.Label6.TabIndex = 19
        Me.Label6.Text = "Age:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(13, 370)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(51, 15)
        Me.Label7.TabIndex = 20
        Me.Label7.Text = "Gender:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(13, 399)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(92, 15)
        Me.Label8.TabIndex = 21
        Me.Label8.Text = "Street Address:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Black
        Me.Label9.Location = New System.Drawing.Point(13, 427)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(74, 15)
        Me.Label9.TabIndex = 22
        Me.Label9.Text = "Birth Place:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Black
        Me.Label10.Location = New System.Drawing.Point(267, 247)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(59, 15)
        Me.Label10.TabIndex = 23
        Me.Label10.Text = "Province:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Black
        Me.Label11.Location = New System.Drawing.Point(267, 274)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(74, 15)
        Me.Label11.TabIndex = 24
        Me.Label11.Text = "Contact No.:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Black
        Me.Label12.Location = New System.Drawing.Point(267, 328)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(70, 15)
        Me.Label12.TabIndex = 25
        Me.Label12.Text = "Blood Type:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Black
        Me.Label14.Location = New System.Drawing.Point(267, 381)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(58, 15)
        Me.Label14.TabIndex = 27
        Me.Label14.Text = "Religion:"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Black
        Me.Label16.Location = New System.Drawing.Point(13, 177)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(24, 15)
        Me.Label16.TabIndex = 33
        Me.Label16.Text = "ID:"
        '
        'TBid
        '
        Me.TBid.AutoSize = True
        Me.TBid.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBid.ForeColor = System.Drawing.Color.Black
        Me.TBid.Location = New System.Drawing.Point(109, 174)
        Me.TBid.Name = "TBid"
        Me.TBid.Size = New System.Drawing.Size(49, 19)
        Me.TBid.TabIndex = 35
        Me.TBid.Text = "00000"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.Black
        Me.Label17.Location = New System.Drawing.Point(13, 152)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(67, 15)
        Me.Label17.TabIndex = 37
        Me.Label17.Text = "Full Name:"
        '
        'TBFullname
        '
        Me.TBFullname.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBFullname.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBFullname.ForeColor = System.Drawing.Color.Black
        Me.TBFullname.Location = New System.Drawing.Point(109, 149)
        Me.TBFullname.Name = "TBFullname"
        Me.TBFullname.Size = New System.Drawing.Size(134, 22)
        Me.TBFullname.TabIndex = 36
        '
        'OpenFileDialogUserImage
        '
        Me.OpenFileDialogUserImage.FileName = "OpenFileDialog1"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Black
        Me.Label15.Location = New System.Drawing.Point(267, 302)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(46, 15)
        Me.Label15.TabIndex = 41
        Me.Label15.Text = "Status:"
        '
        'CBStatus
        '
        Me.CBStatus.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CBStatus.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBStatus.ForeColor = System.Drawing.Color.Black
        Me.CBStatus.FormattingEnabled = True
        Me.CBStatus.Items.AddRange(New Object() {"Single", "Married", "Divorse"})
        Me.CBStatus.Location = New System.Drawing.Point(377, 299)
        Me.CBStatus.Name = "CBStatus"
        Me.CBStatus.Size = New System.Drawing.Size(134, 23)
        Me.CBStatus.TabIndex = 42
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.Black
        Me.Label18.Location = New System.Drawing.Point(267, 355)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(74, 15)
        Me.Label18.TabIndex = 44
        Me.Label18.Text = "Citizenship:"
        '
        'TBCitizenship
        '
        Me.TBCitizenship.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBCitizenship.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBCitizenship.ForeColor = System.Drawing.Color.Black
        Me.TBCitizenship.Location = New System.Drawing.Point(377, 352)
        Me.TBCitizenship.Name = "TBCitizenship"
        Me.TBCitizenship.Size = New System.Drawing.Size(134, 22)
        Me.TBCitizenship.TabIndex = 43
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.Label19.Location = New System.Drawing.Point(143, 33)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(110, 22)
        Me.Label19.TabIndex = 89
        Me.Label19.Text = "REGISTER"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.Label20.Location = New System.Drawing.Point(157, 55)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(109, 22)
        Me.Label20.TabIndex = 90
        Me.Label20.Text = "RESIDENT"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Transparent
        Me.Button1.BackgroundImage = Global.BISfinal.My.Resources.Resources.add
        Me.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(303, 502)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(35, 27)
        Me.Button1.TabIndex = 45
        Me.Button1.UseVisualStyleBackColor = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox2.Image = Global.BISfinal.My.Resources.Resources.pngwing_com__1_
        Me.PictureBox2.Location = New System.Drawing.Point(-3, 11)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(161, 106)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox2.TabIndex = 88
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox1.Location = New System.Drawing.Point(365, 468)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(121, 88)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 87
        Me.PictureBox1.TabStop = False
        '
        'PictureBoxUserReg
        '
        Me.PictureBoxUserReg.BackColor = System.Drawing.Color.Transparent
        Me.PictureBoxUserReg.Image = Global.BISfinal.My.Resources.Resources.add_user
        Me.PictureBoxUserReg.Location = New System.Drawing.Point(321, 12)
        Me.PictureBoxUserReg.Name = "PictureBoxUserReg"
        Me.PictureBoxUserReg.Size = New System.Drawing.Size(176, 125)
        Me.PictureBoxUserReg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBoxUserReg.TabIndex = 38
        Me.PictureBoxUserReg.TabStop = False
        '
        'BtnSave
        '
        Me.BtnSave.BackColor = System.Drawing.Color.Transparent
        Me.BtnSave.BackgroundImage = Global.BISfinal.My.Resources.Resources.register_button_png_23372
        Me.BtnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtnSave.FlatAppearance.BorderSize = 0
        Me.BtnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnSave.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnSave.ForeColor = System.Drawing.Color.White
        Me.BtnSave.Location = New System.Drawing.Point(274, 404)
        Me.BtnSave.Name = "BtnSave"
        Me.BtnSave.Size = New System.Drawing.Size(238, 55)
        Me.BtnSave.TabIndex = 30
        Me.BtnSave.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Transparent
        Me.Button2.BackgroundImage = Global.BISfinal.My.Resources.Resources._20_512_7240
        Me.Button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Button2.FlatAppearance.BorderSize = 0
        Me.Button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(393, 139)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(32, 25)
        Me.Button2.TabIndex = 86
        Me.Button2.UseVisualStyleBackColor = False
        Me.Button2.Visible = False
        '
        'CBvoter
        '
        Me.CBvoter.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CBvoter.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBvoter.ForeColor = System.Drawing.Color.Black
        Me.CBvoter.FormattingEnabled = True
        Me.CBvoter.Items.AddRange(New Object() {"Yes", "No"})
        Me.CBvoter.Location = New System.Drawing.Point(377, 213)
        Me.CBvoter.Name = "CBvoter"
        Me.CBvoter.Size = New System.Drawing.Size(134, 23)
        Me.CBvoter.TabIndex = 94
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(267, 216)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(104, 15)
        Me.Label13.TabIndex = 93
        Me.Label13.Text = "Registered Voter:"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.Black
        Me.Label21.Location = New System.Drawing.Point(267, 188)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(73, 15)
        Me.Label21.TabIndex = 92
        Me.Label21.Text = "Occupation:"
        '
        'TBoccupation
        '
        Me.TBoccupation.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBoccupation.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBoccupation.ForeColor = System.Drawing.Color.Black
        Me.TBoccupation.Location = New System.Drawing.Point(377, 185)
        Me.TBoccupation.Name = "TBoccupation"
        Me.TBoccupation.Size = New System.Drawing.Size(134, 22)
        Me.TBoccupation.TabIndex = 91
        '
        'newRes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(529, 467)
        Me.Controls.Add(Me.CBvoter)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.TBoccupation)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.TBCitizenship)
        Me.Controls.Add(Me.CBStatus)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.PictureBoxUserReg)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.TBFullname)
        Me.Controls.Add(Me.TBid)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TBReligion)
        Me.Controls.Add(Me.CBBtype)
        Me.Controls.Add(Me.TBContact)
        Me.Controls.Add(Me.TBProvince)
        Me.Controls.Add(Me.TBBplace)
        Me.Controls.Add(Me.TBAddress)
        Me.Controls.Add(Me.CBGender)
        Me.Controls.Add(Me.TBAge)
        Me.Controls.Add(Me.dtBday)
        Me.Controls.Add(Me.TBSuffix)
        Me.Controls.Add(Me.TBMname)
        Me.Controls.Add(Me.TBFname)
        Me.Controls.Add(Me.TBLname)
        Me.Controls.Add(Me.BtnSave)
        Me.Controls.Add(Me.Button2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "newRes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxUserReg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TBLname As TextBox
    Friend WithEvents TBFname As TextBox
    Friend WithEvents TBMname As TextBox
    Friend WithEvents TBSuffix As TextBox
    Friend WithEvents dtBday As DateTimePicker
    Friend WithEvents TBAge As TextBox
    Friend WithEvents CBGender As ComboBox
    Friend WithEvents TBAddress As TextBox
    Friend WithEvents TBBplace As TextBox
    Friend WithEvents TBProvince As TextBox
    Friend WithEvents TBContact As TextBox
    Friend WithEvents CBBtype As ComboBox
    Friend WithEvents TBReligion As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents BtnSave As Button
    Friend WithEvents Label16 As Label
    Friend WithEvents TBid As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents TBFullname As TextBox
    Friend WithEvents PictureBoxUserReg As PictureBox
    Friend WithEvents OpenFileDialogUserImage As OpenFileDialog
    Friend WithEvents Label15 As Label
    Friend WithEvents CBStatus As ComboBox
    Friend WithEvents Label18 As Label
    Friend WithEvents TBCitizenship As TextBox
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents Label19 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents CBvoter As ComboBox
    Friend WithEvents Label13 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents TBoccupation As TextBox
End Class
