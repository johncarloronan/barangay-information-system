﻿Imports FireSharp.Config
Imports FireSharp.Response
Imports FireSharp.Interfaces
'-------------------------------------------

Imports System.Web.Script.Serialization '(Importing System.Web.Script.Serialization) This is used to read and convert JSON strings.
Imports System.ComponentModel '(Importing System.ComponentModel) This is used to sort data on DataGridView.
Imports System.IO

Imports AForge
Imports AForge.Video
Imports AForge.Video.DirectShow

Public Class clearance
    Public Property CLEARANCEname As String

    Dim CAMERA As VideoCaptureDevice
    Dim bmp As Bitmap

    Dim IMG_FileNameInput As String
    Dim clearDGVCol As Boolean = True

    Private dtTableGrd As DataTable


    Private fcon As New FirebaseConfig() With
        {
        .AuthSecret = "8kPwBHDkDdZWGHts9sb98dsJu45thBvgDTUrgqJj",
        .BasePath = "https://bisrealtime-default-rtdb.firebaseio.com/"
        }

    Private client As IFirebaseClient

    Public Function ImageToBase64(image As Image) As String
        Using ms As New MemoryStream()
            ' Convert Image to byte[]  
            Dim Format As System.Drawing.Imaging.ImageFormat = Imaging.ImageFormat.Jpeg
            image.Save(ms, Format)
            Dim imageBytes As Byte() = ms.ToArray()

            ' Convert byte[] to Base64 String  
            Dim base64String As String = Convert.ToBase64String(imageBytes)
            Return base64String
        End Using
    End Function

    Public Function Base64ToImage(base64String As String) As Image
        ' Convert Base64 String to byte[]  
        Dim imageBytes As Byte() = Convert.FromBase64String(base64String)
        Dim ms As New MemoryStream(imageBytes, 0, imageBytes.Length)

        ' Convert byte[] to Image  
        ms.Write(imageBytes, 0, imageBytes.Length)
        Dim image__1 As Image = System.Drawing.Image.FromStream(ms, True)
        Return image__1
    End Function

    Sub DisplayRegSave(Stat As Boolean)
        TBFullname.Enabled = Stat
        TBAddress.Enabled = Stat
        CBstatus.Enabled = Stat
        tbName.Enabled = Stat
        TBAddress.Enabled = Stat
        TBCitizenship.Enabled = Stat



    End Sub

    Sub DisplayRecLoad(Stat As Boolean)
        DGVUserData.Enabled = Stat

    End Sub

    Sub ShowRecord()
        Try
            Dim dtTable As New DataTable
            dtTable.Columns.Add("Fullname")
            dtTable.Columns.Add("ID")
            dtTable.Columns.Add("LastName")
            dtTable.Columns.Add("First Name")
            dtTable.Columns.Add("Middle Name")
            dtTable.Columns.Add("Suffix")
            dtTable.Columns.Add("Birth Date")
            dtTable.Columns.Add("Age")
            dtTable.Columns.Add("Gender")
            dtTable.Columns.Add("Address")
            dtTable.Columns.Add("Birth Place")
            dtTable.Columns.Add("Province")
            dtTable.Columns.Add("Contact No.")
            dtTable.Columns.Add("Status")
            dtTable.Columns.Add("Blood Type")
            dtTable.Columns.Add("Citizenship")
            dtTable.Columns.Add("Religion")
            dtTable.Columns.Add("Image", GetType(Image))


            If clearDGVCol = True Then
                DGVUserData.Columns.Clear()
                clearDGVCol = False
            End If


            Dim SRRecord = client.Get("ResidentDB/")


            Dim myJsonTool As New JavaScriptSerializer
            Dim myDeserializedItems = myJsonTool.Deserialize(Of Dictionary(Of String, ResidentData))(SRRecord.Body)


            For Each dictItem As KeyValuePair(Of String, ResidentData) In myDeserializedItems
                dtTable.Rows.Add(dictItem.Value.bFullname, dictItem.Value.bID, dictItem.Value.bLname, dictItem.Value.bFname, dictItem.Value.bMname, dictItem.Value.bSuffix, dictItem.Value.bBdate, dictItem.Value.bAge, dictItem.Value.bGender, dictItem.Value.bAddress, dictItem.Value.bBplace, dictItem.Value.bProvince, dictItem.Value.bContact, dictItem.Value.bStatus, dictItem.Value.bBtype, dictItem.Value.bCitizenship, dictItem.Value.bReligion, Base64ToImage(dictItem.Value.Image))
            Next



            DGVUserData.DataSource = dtTable
            dtTableGrd = dtTable

            Dim imageColumn = DirectCast(DGVUserData.Columns("Image"), DataGridViewImageColumn)
            imageColumn.ImageLayout = DataGridViewImageCellLayout.Zoom

            DGVUserData.Sort(DGVUserData.Columns(0), ListSortDirection.Descending)



            DisplayRecLoad(True)

            DGVUserData.ClearSelection()


        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ElseIf ex.Message = "Object reference not set to an instance of an object." Then
                Dim dtTable As New DataTable
                dtTable.Columns.Add("Fullname")
                dtTable.Columns.Add("ID")
                dtTable.Columns.Add("LastName")
                dtTable.Columns.Add("First Name")
                dtTable.Columns.Add("Middle Name")
                dtTable.Columns.Add("Suffix")
                dtTable.Columns.Add("Birth Date")
                dtTable.Columns.Add("Age")
                dtTable.Columns.Add("Gender")
                dtTable.Columns.Add("Address")
                dtTable.Columns.Add("Birth Place")
                dtTable.Columns.Add("Province")
                dtTable.Columns.Add("Contact No.")
                dtTable.Columns.Add("Status")
                dtTable.Columns.Add("Blood Type")
                dtTable.Columns.Add("Citizenship")
                dtTable.Columns.Add("Religion")
                dtTable.Columns.Add("Image", GetType(Image))
                DGVUserData.DataSource = dtTable

                MessageBox.Show("Database not found or Database is empty.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

            DisplayRecLoad(True)

        End Try
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        Me.BackColor = Color.White
        tbSearch.Visible = False
        btnSearch.Visible = False
        PictureBox1.Visible = False


        PrintForm1.PrintAction = Printing.PrintAction.PrintToPreview
        PrintForm1.Print()



    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        dtTableGrd.DefaultView.RowFilter = CBsb.Text & " Like '" & tbSearch.Text & "%'" 'To search for data on the DataGridView
        DGVUserData.ClearSelection()

        DGVUserData.Visible = True
        Panel5.Visible = True
    End Sub

    Private Sub clearance_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            client = New FireSharp.FirebaseClient(fcon)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        ShowRecord()

        Dim clearDGVCol As Boolean = True
        Timer1.Enabled = True
        tbSearch.Text = " Search Surname..."
        tbSearch.ForeColor = Color.Gray


        TBFullname.Text = CLEARANCEname
        Panel5.Visible = False
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Ldate.Text = Date.Now.ToString("MM-dd-yyyy")
    End Sub

    Private Sub DGVUserData_CellContentClick(sender As Object, e As DataGridViewCellEventArgs)

    End Sub



    Private Sub Button1_Click(sender As Object, e As EventArgs)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        PictureBoxUserReg.Image = PictureBox1.Image
    End Sub

    Private Sub tbsex_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub tbsex_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub Label22_Click(sender As Object, e As EventArgs) Handles TBs.Click

    End Sub

    Private Sub TBFullname_Click(sender As Object, e As EventArgs) Handles TBFullname.Click

    End Sub

    Private Sub TBFullname_TextChanged(sender As Object, e As EventArgs) Handles TBFullname.TextChanged

    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        TextBox1.ForeColor = Color.Black
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        Dim cameras As VideoCaptureDeviceForm = New VideoCaptureDeviceForm
        If cameras.ShowDialog = DialogResult.OK Then
            CAMERA = cameras.VideoDevice
            AddHandler CAMERA.NewFrame, New NewFrameEventHandler(AddressOf Captured)
            CAMERA.Start()

            Button2.Visible = True


        End If
    End Sub

    Private Sub Captured(sender As Object, eventArgs As NewFrameEventArgs)
        bmp = DirectCast(eventArgs.Frame.Clone(), Bitmap)
        PictureBox1.Image = DirectCast(eventArgs.Frame.Clone(), Bitmap)

    End Sub

    Private Sub DGVUserData_CellContentClick_1(sender As Object, e As DataGridViewCellEventArgs) Handles DGVUserData.CellContentClick
        TBFullname.Text = DGVUserData.SelectedRows(0).Cells("Fullname").Value
        CBstatus.Text = DGVUserData.SelectedRows(0).Cells("Status").Value + ", " + DGVUserData.SelectedRows(0).Cells("Citizenship").Value + ", is a bonafide resident"
        TBAddress.Text = DGVUserData.SelectedRows(0).Cells("Address").Value + " Barangay West Kamias, Quezon City,"
        tbsex.Text = DGVUserData.SelectedRows(0).Cells("Gender").Value
        PictureBoxUserReg.Image = DGVUserData.Rows(DGVUserData.SelectedRows(0).Index).Cells("Image").Value
        tbName.Text = DGVUserData.SelectedRows(0).Cells("Fullname").Value
        TBs.Text = DGVUserData.SelectedRows(0).Cells("Gender").Value

        DGVUserData.Visible = False
        Panel5.Visible = False

        If tbsex.Text = "Male" Then
            tbsex.Text = "He has no derogatory/criminal records filed"
        Else
            tbsex.Text = "She has no derogatory/criminal"
        End If

        If TBs.Text = "Male" Then
            TBs.Text = "His"
        Else
            TBs.Text = "Her"
        End If
    End Sub

End Class