﻿Imports FireSharp.Config
Imports FireSharp.Response
Imports FireSharp.Interfaces
'-------------------------------------------

Imports System.Web.Script.Serialization
Imports System.ComponentModel
Imports System.IO

Public Class Main

    Dim IMG_FileNameInput As String
    Dim clearDGVCol As Boolean = True

    Private dtTableGrd As DataTable


    Private fcon As New FirebaseConfig() With
        {
        .AuthSecret = "8kPwBHDkDdZWGHts9sb98dsJu45thBvgDTUrgqJj",
        .BasePath = "https://bisrealtime-default-rtdb.firebaseio.com/"
        }

    Private client As IFirebaseClient



    Sub ShowRecord()
        Try
            Dim dtTable As New DataTable
            dtTable.Columns.Add("ID")
            dtTable.Columns.Add("Name")
            dtTable.Columns.Add("Purpose")
            dtTable.Columns.Add("Date")
            dtTable.Columns.Add("Time")

            If clearDGVCol = True Then

                clearDGVCol = False
            End If


            Dim SRRecord = client.Get("LogDB/")


            Dim myJsonTool As New JavaScriptSerializer
            Dim myDeserializedItems = myJsonTool.Deserialize(Of Dictionary(Of String, LogData))(SRRecord.Body)


            For Each dictItem As KeyValuePair(Of String, LogData) In myDeserializedItems
                dtTable.Rows.Add(dictItem.Value.aID, dictItem.Value.aName, dictItem.Value.aPurpose)
            Next


            dtTableGrd = dtTable



        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ElseIf ex.Message = "Object reference not set to an instance of an object." Then
                Dim dtTable As New DataTable
                dtTable.Columns.Add("ID")
                dtTable.Columns.Add("Name")
                dtTable.Columns.Add("Purpose")


                MessageBox.Show("Database not found or Database is empty.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

            Me.Text = "VB Net Firebase RealTime Database"

        End Try
    End Sub
    Private Sub Panel1_Paint(sender As Object, e As PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs)


    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TBName.Text = " Name"
        TBName.ForeColor = Color.Gray
        Timer1.Enabled = True

        Try
            client = New FireSharp.FirebaseClient(fcon)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Panel1.Controls.Clear()
        residentLogin.TopLevel = False
        Panel1.Controls.Add(residentLogin)
        residentLogin.Show()

    End Sub

    Private Sub TBName_TextChanged(sender As Object, e As EventArgs) Handles TBName.TextChanged
        TBName.ForeColor = Color.Black
    End Sub

    Private Sub BtnLogin_Click(sender As Object, e As EventArgs) Handles BtnLogin.Click
        If TBName.Text = Nothing Then
            MessageBox.Show("Insert NAME!", "Warning!!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        If CBPurpose.Text = Nothing Then
            MessageBox.Show("Insert PURPOSE!", "Warning!!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        Try
            loading.Visible = True
            Dim LD As New LogData() With
                {
                .aID = TBlogid.Text,
                .aName = TBName.Text,
                .aPurpose = CBPurpose.Text,
                .aDate = LDate.Text,
                .aTime = LTime.Text
                }
            Dim save = client.Set("LogDB/" + TBlogid.Text, LD)
            residentInterface.Show()
            loading.Visible = False
            Me.Hide()
            TBlogid.Text = ""
            TBName.Text = "Name"
            TBName.ForeColor = Color.Gray
            CBPurpose.Text = ""



        Catch ex As Exception

        End Try
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        LDate.Text = Date.Now.ToString("MMM dd yyyy")
        LTime.Text = Date.Now.ToString("hh:mm:ss tt")
    End Sub

    Private Sub TBName_Click(sender As Object, e As EventArgs) Handles TBName.Click
        TBName.Text = ""

        Try

            Dim r As Random = New Random
            Dim num As Integer
            num = (r.Next(1, 999999))
            Dim IDresults As String = Strings.Right("0000000" & num.ToString(), 6)

            Dim Check_ID = client.Get("LogDB/" + IDresults)

            '-------------------------------------------Conditions to check whether the ID has been used.
            If Check_ID.Body <> "null" Then
                MessageBox.Show("The same ID is found, create another ID by pressing the Create ID button.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                TBlogid.Text = IDresults
            End If
            '-------------------------------------------
        Catch ex As Exception
            If ex.Message = "One or more errors occurred." Then
                TBlogid.Text = ""
                MessageBox.Show("Cannot connect to firebase, check your network !", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                TBlogid.Text = ""
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End Try
    End Sub

    Private Sub CBPurpose_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CBPurpose.SelectedIndexChanged

    End Sub

    Private Sub CBPurpose_Click(sender As Object, e As EventArgs) Handles CBPurpose.Click
        Label4.Visible = False



    End Sub

    Private Sub Label6_Click(sender As Object, e As EventArgs)


    End Sub

    Private Sub TBlogid_Click(sender As Object, e As EventArgs) Handles TBlogid.Click

    End Sub

    Private Sub PictureBox2_Click(sender As Object, e As EventArgs) Handles PictureBox2.Click
        Panel1.Controls.Clear()
        AdminLogIn.TopLevel = False
        Panel1.Controls.Add(AdminLogIn)
        AdminLogIn.Show()
    End Sub

    Private Sub PictureBox3_Click(sender As Object, e As EventArgs) Handles PictureBox3.Click
        Panel1.Controls.Clear()
        residentLogin.TopLevel = False
        Panel1.Controls.Add(residentLogin)
        residentLogin.Show()
    End Sub

End Class
